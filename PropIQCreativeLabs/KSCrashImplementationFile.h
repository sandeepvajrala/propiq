//
//  KSCrashImplementationFile.h
//  miDriver
//
//  Created by AcmeMinds on 07/11/17.
//  Copyright © 2017 Acmeminds. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSCrash.h"
#import "KSCrashInstallationEmail.h"
#import "KSCrashInstallation+Alert.h"

@interface KSCrashImplementationFile : UIViewController

@property (strong, nonatomic) UIWindow* window;
@property (strong, nonatomic) KSCrashInstallation* crashInstallation;

- (void) installCrashHandler;
- (KSCrashInstallation*) makeEmailInstallation;


@end


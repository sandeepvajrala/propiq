//
//  MaintenanceJobsInnerCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 03/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class MaintenanceJobsInnerCell: UITableViewCell {

    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var jobDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        circleLabel.layer.cornerRadius = 0.5 * circleLabel.layer.bounds.width
        circleLabel.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

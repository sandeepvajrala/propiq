//
//  MaintainanceJobsVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 03/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class MaintainanceJobsVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var firstPriorityLabel: UILabel!
    @IBOutlet weak var secondPriorityLabel: UILabel!
    @IBOutlet weak var thirdPriorityLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var jobsLabel: UILabel!
    @IBOutlet weak var noJobsFoundLabel: UILabel!
    
    var isIndexPathSelected = 0
    var selectedCellIndexPath: IndexPath?
    var selectedCellHeight: CGFloat = 240.0
    let unselectedCellHeight: CGFloat = 140.0
    var jobsArray:JobsData?
    let dimmingView = UIView()
    var params = [String : Any]()
    var currentJobsArray = [Job]()
    var priority = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setup()
    }

    override func viewWillAppear(_ animated: Bool) {
        selectedCellIndexPath = nil
        apiCallForAllJobs(params: params)
        tableView.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Methods
    func setup() {
        params = ["job_status":"0","job_priority":"1","user_id":UserDetails.sharedInstance.User_ID,"department_id":"3"]
        
    }
    func apiCallForAllJobs(params: [String: Any]) {
        _ = addActivityIndicator()
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.JobListing, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            do {
                let jobDetails = try JSONDecoder().decode(JobsData.self, from: response as! Data)
                self.jobsArray = jobDetails
                self.currentJobsArray = jobDetails.jobs
                if self.currentJobsArray.count == 0 {
                    self.noJobsFoundLabel.isHidden = false
                } else {
                    self.noJobsFoundLabel.isHidden = true
                }
                self.tableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
//MARK: - Actions
    @IBAction func firstPriorityButtonTapped(_ sender: UIButton) {
        selectedCellIndexPath = nil
        selectedButtonLabel(selectedLabel: firstPriorityLabel)
        unSelectedButtonLAbel(unSelectedLabel1: secondPriorityLabel, unSelectedLabel2: thirdPriorityLabel)
        params.updateValue("1", forKey: "job_priority")
        priority = 1
        apiCallForAllJobs(params: params)
    }
    
    @IBAction func secondPriorityButtonTapped(_ sender: UIButton) {
        selectedCellIndexPath = nil
        selectedButtonLabel(selectedLabel: secondPriorityLabel)
        unSelectedButtonLAbel(unSelectedLabel1: firstPriorityLabel, unSelectedLabel2: thirdPriorityLabel)
        params.updateValue("2", forKey: "job_priority")
        priority = 2
        apiCallForAllJobs(params: params)

        
    }
    
    @IBAction func thirdPriorityButtonTapped(_ sender: Any) {
        selectedCellIndexPath = nil
        selectedButtonLabel(selectedLabel: thirdPriorityLabel)
        unSelectedButtonLAbel(unSelectedLabel1: secondPriorityLabel, unSelectedLabel2: firstPriorityLabel)
        params.updateValue("3", forKey: "job_priority")
        priority = 3
        apiCallForAllJobs(params: params)
        
    }
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        guard let acceptVC = UIStoryboard(name: "JobsDetail", bundle: nil).instantiateViewController(withIdentifier: "AcceptVC") as? AcceptVC else { return }
        
        guard let index = selectedCellIndexPath?.row else { return }
        acceptVC.jobId = currentJobsArray[index].jobID
        acceptVC.jobDetails = currentJobsArray[index].detail
        acceptVC.status = currentJobsArray[index].status
//        self.add(acceptVC)
        self.present(acceptVC, animated:true, completion: nil)
    }
    
    @IBAction func rejectBtnPressed(_ sender: Any) {
        let rejectionVC = UIStoryboard(name: "JobsDetail", bundle: nil).instantiateViewController(withIdentifier: "RejectionVC") as! RejectionVC
        
        guard let index = selectedCellIndexPath?.row else { return }
        rejectionVC.jobId = currentJobsArray[index].jobID
//        self.add(rejectionVC)
        self.present(rejectionVC, animated:true, completion: nil)
    }
    
    func selectedButtonLabel(selectedLabel: UILabel) {
        selectedLabel.backgroundColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
    }
    func unSelectedButtonLAbel(unSelectedLabel1: UILabel, unSelectedLabel2: UILabel ){
        unSelectedLabel1.backgroundColor = UIColor.white
        unSelectedLabel2.backgroundColor = UIColor.white
    }
    
    
}

//MARK: - Tableview Methods

extension MaintainanceJobsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return currentJobsArray.count > 0 ? currentJobsArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MaintenanceJobsCell") as? MaintenanceJobsCell else { return UITableViewCell() }
        cell.stackView.isHidden = false
        if priority == 1 {
            cell.stackView.isHidden = false
        } else if priority == 2{
            if let jobs = jobsArray {
                if jobs.priority1 != 0 {
                    cell.stackView.isHidden = true
                }
            }
        } else if priority == 3{
            if let jobs = jobsArray {
                if jobs.priority2 != 0 && jobs.priority3 != 0 {
                    cell.stackView.isHidden = true
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let jobsCell = cell as? MaintenanceJobsCell {
            if currentJobsArray.count > 0 {
                jobsCell.configureCell(jobsArr: currentJobsArray[indexPath.row])
                if selectedCellIndexPath == nil {
                    cell.setSelected(false, animated: true)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let count = currentJobsArray[indexPath.row].detail.count
        selectedCellHeight = CGFloat((count * 30) + 140)
        
        let cell = tableView.cellForRow(at: indexPath) as! MaintenanceJobsCell
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            
            selectedCellIndexPath = nil
//            cell.isSelection = false
            cell.setSelected(false, animated: true)
            
        } else {
            selectedCellIndexPath = indexPath
//            cell.isSelection = true
            cell.setSelected(true, animated: false)
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? MaintenanceJobsCell else { return }
        cell.isSelection = false
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCellIndexPath == indexPath {
            return selectedCellHeight
        }
        return unselectedCellHeight
    }
}

//
//  ApiFile.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct ApiFile
{
//    static let baseApi: String = "http://b4d6ebc4.ngrok.io/PropIQ/"
    static let baseApi: String = "http://34.215.99.251:8080/"

    //"https://d4937718.ngrok.io/PropIQ/"  // testing api at local end
    
    
    /***********************************************************************/
    
    // values iof the api that needs to be concatinated
    
    static let loginApi: String = baseApi + "User_Login"
    static let FirstLogin: String = baseApi + "User_FirstLogin"
    static let ChangePassword: String = baseApi + "User_ChangePassword"
    static let JobListing: String = baseApi + "Job_AllJobs"
    static let jobImages = baseApi + "Job_setJobImages"
    static let jobRejectDetailsList = baseApi + "Job_RejectDetailsList"
    static let rejectJobs = baseApi + "Job_RejectJobs"
    static let allUsers = baseApi + "User_AllUsers"
    static let countries = baseApi + "User_Countries"
    static let addUser = baseApi + "User_AddUser"
    static let jobDetailsList = baseApi + "Job_DetailsList"
    static let roomsList = baseApi + "Room_List"
    static let addJobs = baseApi + "Job_AddJobs"
    static let addjobDetailList = baseApi + "Job_AddJobDetailsList"
    static let updateJobs = baseApi + "Job_UpdateJobs"
    static let updateProfile  = baseApi + "User_UpdateProfile"
    static let filterJobs = baseApi + "Job_FilterJobs"
    static let getExpenses = baseApi + "Expenses_GetExpenses"
    static let addUpdateExpense = baseApi + "Expenses_AddUpdateExpenses"
    static let getImages = baseApi + "Job_GetImages"
    static let approveOrRejectJob = baseApi + "Job_ApproveRejectJob"
    static let dahsboard = baseApi + "User_Dashboard"
    static let userAttendance = baseApi + "User_Attendance"
    static let userAttendanceFilter = baseApi + "User_AttendanceFilter"
    static let userPerformanceStats = baseApi + "User_PerformanceStats"
    static let userCheckInList = baseApi + "User_CheckInList"
    static let availableRoomList = baseApi + "Room_List"
    static let roomTypes = baseApi + "Room_RoomType"
    static let roomCheckIn = baseApi + "Room_CheckIn"
    static let roomCheckOut = baseApi + "Room_CheckOut"

    
}
struct Constants {
    static let notificationName = Notification.Name("myNotificationName")
    static let PMLeftMenuNotificationName = Notification.Name("PMLeftMenuNotificationName")
}

//
//  DataService.swift
//  breakpoint
//
//  Created by Caleb Stultz on 7/22/17.
//  Copyright © 2017 Caleb Stultz. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

let DB_BASE = Database.database().reference()

class DataService {
    static let instance = DataService()
    
    private var _REF_BASE = DB_BASE
    private var _REF_MESSAGES = DB_BASE.child("messages")
    
    var REF_BASE: DatabaseReference {
        return _REF_BASE
    }
    
    var REF_MESSAGES: DatabaseReference {
        return _REF_MESSAGES
    }
    
    
    func uploadMessages(withMessage message: String, forUID uid: String, withGroupKey groupKey: String, receiverName: String, receiverPhoto: String, receiverId: String,senderName: String, senderId: String, senderPhoto: String, sendComplete: @escaping (_ status: Bool) -> ()) {
 
            REF_MESSAGES.child(groupKey).childByAutoId().updateChildValues(["receiverid": receiverId, "receivername": receiverName,"receiverphoto": receiverPhoto, "senderid": senderId, "sendername": senderName, "senderphoto": senderPhoto , "text": message, "timestamp": "\(Date().millisecondsSince1970)"])
            sendComplete(true)
    }
    
//    func uploadImages(groupKey: String, message: String, pictureLink: String, senderId: String, senderName: String, date: Date, receiverName: String, receiverPhoto: String, receiverId: String, senderPhoto:String , status: String, type: String) {
//        REF_MESSAGES.child(groupKey).childByAutoId().updateChildValues(["receiverid": receiverId, "receivername": receiverName,"receiverphoto": receiverPhoto, "senderid": senderId, "sendername": senderName, "senderphoto": senderPhoto , "text": message, "timestamp": "\(Date().millisecondsSince1970)"])
//    }
    
    func getAllFeedMessages(forUID uid: String, handler: @escaping (_ messages: [Message]) -> ()) {
        var messageArray = [Message]()
        let child = REF_MESSAGES.child("6 - " + uid)
        child.observeSingleEvent(of: .value) { (feedMessageSnapshot) in
            guard let feedMessageSnapshot = feedMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for message in feedMessageSnapshot {
                let msg = message.childSnapshot(forPath: "text").value as? String
                let receiverName = message.childSnapshot(forPath: "receivername").value as? String
                let myMessage = Message()
                
                if let messsage = msg, let receive = receiverName {
                    myMessage.messageBody = messsage
                    myMessage.sender = receive
                    messageArray.append(myMessage)
                }
            }
            
            handler(messageArray)
        }
    }
    
    func getRecentMessages(handler: @escaping(_ messages: [Messages]?, _ isSuccess: Bool ) -> ()) {
        let messageDB = DB_BASE.child("messages")
    
        messageDB.observe(.value) { (snapshot) in
            
            var messages = [Messages]()
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                
                let child = item.key
                
                let childMessageDB = messageDB.child(child)
                
                childMessageDB.queryLimited(toLast: 1).observe(.value, with: { (snapshot) in
                    
                    let snapshotValue = snapshot.value
                    guard let message = self.createMessage(messageDictionary: snapshotValue as! NSDictionary ) else { return }
                    let receiverId = message.receiverid
                    if receiverId == myId && message.senderid == myId {
                        return
                    } else if receiverId == myId {
                        messages.append(message)
                        messages = messages.sorted(by: {$0.timestamp > $1.timestamp})
                        
                        
                    } else if message.senderid == myId {
                        messages.append(message)
                        messages = messages.sorted(by: {$0.timestamp > $1.timestamp})
                        
                    }
                    handler(messages, true)
                })
                handler(nil,false)
            }
        }
        handler(nil,false)

    }
    
    func createMessage(messageDictionary: NSDictionary) -> Messages? {
        
        for (_,value) in messageDictionary {
            guard let myDictionary = value as? NSDictionary else { return nil}
            let receiverId = myDictionary["receiverid"] as? String ?? ""
            let senderId = myDictionary["senderid"] as? String ?? ""
            let receiverName = myDictionary["receivername"] as? String ?? ""
            let text = myDictionary["text"] as? String ?? ""
            let senderName = myDictionary["sendername"] as? String ?? ""
            let timeStamp = myDictionary["timestamp"] as? String
            let receiverphoto = myDictionary["receiverphoto"] as? String ?? ""
            let senderphoto = myDictionary["senderphoto"] as? String ?? ""
            var myDate = Date()
            if let dat = timeStamp {
                if let val = Int(dat) {
                    myDate = Date(milliseconds: val) ?? Date()
                }
                
            }
            let message = Messages(receiverid: receiverId  , receivername: receiverName , receiverphoto: receiverphoto, senderid: senderId, sendername: senderName, senderphoto: senderphoto, text: text, timestamp: myDate )
            return message
        }
        
        return nil
    }
    
    

}










//    func createDBUser(uid: String, userData: Dictionary<String, Any>) {
//        REF_USERS.child(uid).updateChildValues(userData)
//    }

    
//    func getUsername(forUID uid: String, handler: @escaping (_ username: String) -> ()) {
//        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
//            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for user in userSnapshot {
//                if user.key == uid {
//                    handler(user.childSnapshot(forPath: "email").value as! String)
//                }
//            }
//        }
//    }

//    func uploadPost(withMessage message: String, forUID uid: String, withGroupKey groupKey: String?, sendComplete: @escaping (_ status: Bool) -> ()) {
//        if groupKey != nil {
//            REF_GROUPS.child(groupKey!).child("messages").childByAutoId().updateChildValues(["content": message, "senderId": uid])
//            sendComplete(true)
//        } else {
//            REF_FEED.childByAutoId().updateChildValues(["content": message, "senderId": uid])
//            sendComplete(true)
//        }
//    }

//    func getAllFeedMessages(handler: @escaping (_ messages: [Message]) -> ()) {
//        var messageArray = [Message]()
//        REF_FEED.observeSingleEvent(of: .value) { (feedMessageSnapshot) in
//            guard let feedMessageSnapshot = feedMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
//
//            for message in feedMessageSnapshot {
//                let content = message.childSnapshot(forPath: "content").value as! String
//                let senderId = message.childSnapshot(forPath: "senderId").value as! String
//                let message = Message(content: content, senderId: senderId)
//                messageArray.append(message)
//            }
//
//            handler(messageArray)
//        }
//    }
//
//    func getAllMessagesFor(desiredGroup: Group, handler: @escaping (_ messagesArray: [Message]) -> ()) {
//        var groupMessageArray = [Message]()
//        REF_GROUPS.child(desiredGroup.key).child("messages").observeSingleEvent(of: .value) { (groupMessageSnapshot) in
//            guard let groupMessageSnapshot = groupMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for groupMessage in groupMessageSnapshot {
//                let content = groupMessage.childSnapshot(forPath: "content").value as! String
//                let senderId = groupMessage.childSnapshot(forPath: "senderId").value as! String
//                let groupMessage = Message(content: content, senderId: senderId)
//                groupMessageArray.append(groupMessage)
//            }
//            handler(groupMessageArray)
//        }
//    }
//
//    func getEmail(forSearchQuery query: String, handler: @escaping (_ emailArray: [String]) -> ()) {
//        var emailArray = [String]()
//        REF_USERS.observe(.value) { (userSnapshot) in
//            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for user in userSnapshot {
//                let email = user.childSnapshot(forPath: "email").value as! String
//
//                if email.contains(query) == true && email != Auth.auth().currentUser?.email {
//                    emailArray.append(email)
//                }
//            }
//            handler(emailArray)
//        }
//    }
//
//    func getIds(forUsernames usernames: [String], handler: @escaping (_ uidArray: [String]) -> ()) {
//        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
//            var idArray = [String]()
//            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for user in userSnapshot {
//                let email = user.childSnapshot(forPath: "email").value as! String
//
//                if usernames.contains(email) {
//                    idArray.append(user.key)
//                }
//            }
//            handler(idArray)
//        }
//    }
//
//    func getEmailsFor(group: Group, handler: @escaping (_ emailArray: [String]) -> ()) {
//        var emailArray = [String]()
//        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
//            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for user in userSnapshot {
//                if group.members.contains(user.key) {
//                    let email = user.childSnapshot(forPath: "email").value as! String
//                    emailArray.append(email)
//                }
//            }
//            handler(emailArray)
//        }
//    }
//
//    func createGroup(withTitle title: String, andDescription description: String, forUserIds ids: [String], handler: @escaping (_ groupCreated: Bool) -> ()) {
//        REF_GROUPS.childByAutoId().updateChildValues(["title": title, "description": description, "members": ids])
//        handler(true)
//    }
//
//    func getAllGroups(handler: @escaping (_ groupsArray: [Group]) -> ()) {
//        var groupsArray = [Group]()
//        REF_GROUPS.observeSingleEvent(of: .value) { (groupSnapshot) in
//            guard let groupSnapshot = groupSnapshot.children.allObjects as? [DataSnapshot] else { return }
//            for group in groupSnapshot {
//                let memberArray = group.childSnapshot(forPath: "members").value as! [String]
//                if memberArray.contains((Auth.auth().currentUser?.uid)!) {
//                    let title = group.childSnapshot(forPath: "title").value as! String
//                    let description = group.childSnapshot(forPath: "description").value as! String
//                    let group = Group(title: title, description: description, key: group.key, members: memberArray, memberCount: memberArray.count)
//                    groupsArray.append(group)
//                }
//            }
//            handler(groupsArray)
//        }
//    }
//}

















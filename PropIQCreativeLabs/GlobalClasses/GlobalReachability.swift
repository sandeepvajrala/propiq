//
//  GlobalReachability.swift
//  propIQ app
//
//  Created by Tangerine creative labs on 04/10/16.
//
// Developer name : - Shashank K. Atray
//  Copyright © 2017 Tangerine Creative labs. All rights reserved.
//

import UIKit

class GlobalReachability: NSObject
{
    //instance class to maintain the value of the login type 
    static let sharedInstance: GlobalReachability = {
        let instance = GlobalReachability()
        // setup code
        return instance
    }()
    
    var reachability = Reachability()!
}

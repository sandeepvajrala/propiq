//
//  CommonCategories.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD


private var progressHUDKey = 0

extension NSObject{
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        
        if let data = (text as AnyObject).data(using: String.Encoding.utf8.rawValue){
            do {
                
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                
                return json
                
            } catch {
                return nil
            }
        }
        return nil
    }
    func convertUrlToImage(urlString: String?) -> UIImage? {
        guard let urlstring = urlString else { return UIImage() }
        let url = URL(string: urlstring)
                guard  let myurl = url else { return UIImage() }
        do {
            let data = try Data(contentsOf: myurl)
            let image = UIImage(data: data) ?? UIImage()
            return image
            
        } catch {
            print(error)
        }
        
        return UIImage()
        
    }
    
    
    
}


extension Data
{
    func appendToURL(fileURL: NSURL) throws
    {
        if let fileHandle = try? FileHandle(forWritingTo: fileURL as URL)
        {
            defer
            {
                fileHandle.closeFile()
            }
            
            fileHandle.seekToEndOfFile()
            
            fileHandle.write(self as Data)
        }
        else
        {
            try write(to: fileURL as URL, options: .atomic)
        }
    }
}


extension UIColor
{
    class func appThemeColor() -> UIColor
    {
        return UIColor(red: 125.0/255.0, green: 2.0/255.0, blue: 29.0/255.0, alpha: 1.0)
    }
    
    class func appTextColor() -> UIColor
    {
        return UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0)
    }
}

// extension for textfield
extension UITextField
{
    // MARK :- Methods for providing paddign to textfield
    func textFieldPadding()
    {
        self.leftViewMode = UITextFieldViewMode.always
        self.layer.borderWidth = 1.0
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
    }
    
    func LatentBorderColor()
    {
        self.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
    }
    func ActiveBorderColor(){
        self.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
    }
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
 
}

// value or states of string

extension String
{
    func trimmedString() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    
    
    func isValidEmail() -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
    }
    
    
    // atleast 8 character with 1 caps and alpha numeric
    func isValidPassword() -> Bool
    {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$"
        //"^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$"
        
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        
        return passwordTest.evaluate(with: self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
    }
    
    
    func appendLineToURL(fileURL: NSURL) throws {
        try self.appending("\n").appendToURL(fileURL: fileURL)
        
    }
    
    
    func appendToURL(fileURL: NSURL) throws {
        let data = self.data(using: String.Encoding.utf8)
        try data?.appendToURL(fileURL: fileURL)
        
    }
    
    
    func isEmptyString() -> Bool
    {
        if self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).characters.count == 0
        {
            return true
        }
            
        else
        {
            return false
        }
    }
    
    
}


extension UIButton // set states of all the button
{
    // primary button states
    func setButtonEnableForPrimaryButton(_ enable: Bool)
    {
        if enable // set state if button is enabled
        {
            self.backgroundColor = UIColor.appThemeColor()
            self.setTitleColor(UIColor.white, for: UIControlState.normal)
            self.isEnabled = true
            self.layer.borderWidth = 0
        }
            
        else // set state if button is disable
        {
            self.isEnabled = false
            self.backgroundColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0)
            self.setTitleColor(UIColor(red: 153.0/255.0, green: 153.0/255.0, blue: 153.0/255.0, alpha: 1.0), for: UIControlState.normal) // 999999 font color
            self.layer.borderWidth = 0
        }
    }
    
    func setButtonEnabledForSecondyButton(_ enable: Bool)
    {
        
        // secondry button states and color
        if enable
        {
            self.backgroundColor = UIColor.white
            self.setTitleColor(UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0), for: UIControlState.normal)
            self.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
            self.isEnabled = true
            self.layer.borderWidth = 1
            
        }
            
        else
        {
            self.isEnabled = false
            self.backgroundColor = UIColor.white
            self.layer.borderWidth = 1
            self.layer.borderColor = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0).cgColor
            self.setTitleColor(UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 1.0), for: UIControlState.normal)
        }
    }
    
    // button clicked action performed color change
    func buttonClicked()
    {
        self.layer.borderWidth = 0
        
        self.backgroundColor = UIColor(red: 252.0/255.0, green: 50.0/255.0, blue: 64.0/255.0, alpha: 1.0)
        self.setTitleColor(UIColor.white, for: UIControlState.highlighted)
        // self.setTitleColor(.white, for: .highlighted)
        //self.setTitleColor(UIColor.blue, for: .normal)
        
        
        
    }
}

extension AppDelegate
{
    func removeDefaultErrorAlert()
    {
        if let vNetworkBar = self.window?.viewWithTag(89)
        {
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                
                var frameNetworkBar = vNetworkBar.frame
                frameNetworkBar.origin.y = -frameNetworkBar.size.height
                vNetworkBar.frame = frameNetworkBar
                
            }, completion: { (finished) in
                
                vNetworkBar.removeFromSuperview()
            })
        }
    }
    
}
extension UIView {
  
  
    /// Circularises the View
    /// - Parameter enable: Circular or normal view toggled with enable
    func border(enable: Bool = true, withWidth width: CGFloat = 1.0, andColor color: UIColor = #colorLiteral(red: 0.3747478724, green: 0.3747574091, blue: 0.3747522831, alpha: 1)) {
        layer.borderWidth = enable ? width : 0
        layer.borderColor = enable ? color.cgColor : UIColor.clear.cgColor
    }
    
    //Make Circle of Image
    func makeCircle() {
        self.layer.cornerRadius = self.bounds.size.width / 2
        self.layer.masksToBounds = true
    }
    
    // Make Circle with Color
    func makeCircleWithBorderColor(color: UIColor, width: CGFloat, cornerRadius: CGFloat) {
        self.layer.cornerRadius = cornerRadius;
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
        
    }
    func circleWithBorderColor(enable: Bool = true, color: UIColor, width: CGFloat ) {
        self.layoutIfNeeded()
        let cornerRadius = min(bounds.width, bounds.height)/2
        layer.cornerRadius = enable ? cornerRadius : 0
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
        
        
    }
    
    
    // Make Rounded Corner of Any UI Component.
    func roundCornersWithLayerMask(cornerRadii: CGFloat, corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect: bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: cornerRadii, height: cornerRadii))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        layer.mask = maskLayer
    }
    
    func setBottomBorder() {
        self.layer.backgroundColor = UIColor.red.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    /// - Parameters:
    ///   - enable: Smoothening/Sharpening of edges toggled with enable
    ///   - radius: Curve radius for the edges
    func round(enable: Bool = true, withRadius radius: CGFloat? = 5) {
        let cornerRadius = radius ?? bounds.height/2
        layer.cornerRadius = enable ? cornerRadius : 0
        layer.masksToBounds = enable
    }
    
    /// Adds a 50% opaque shadow to the view
    ///
    /// - Parameters:
    ///   - enable: toggles shadow on the
    ///   - color: color of the shadow
    func shadow(enable: Bool = true, colored color: CGColor, withRadius radius: CGFloat = 2.0) {
        layer.masksToBounds = !enable
        layer.shadowOffset = CGSize.zero
        layer.shadowOpacity = enable ? 0.5 : 0.0
        layer.shadowRadius = radius
        layer.shadowColor = enable ? color : UIColor.clear.cgColor
    }
    
    /// Circularises the View
    ///
    /// - Parameter enable: Circular or normal view toggled with enable
    func circle(enable: Bool = true) {
        self.layoutIfNeeded()
        let cornerRadius = min(bounds.width, bounds.height)/2
        layer.cornerRadius = enable ? cornerRadius : 0
        layer.masksToBounds = enable
        
    }
    
    
    var progressHUD : MBProgressHUD? {
        get {
            return objc_getAssociatedObject(self, &progressHUDKey) as? MBProgressHUD
        }
        set(newValue) {
            
            objc_setAssociatedObject(self, &progressHUDKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    func showLoader() {
        if self.progressHUD == nil {
            let loader = MBProgressHUD.showAdded(to: self, animated: true)
            loader.bezelView.color = UIColor.white
            loader.bezelView.style = .solidColor
            loader.backgroundView.color = UIColor.lightGray.withAlphaComponent(0.3)
            loader.removeFromSuperViewOnHide = true
            self.progressHUD = loader
        }
        self.progressHUD?.show(animated: true)
    }
    func hideLoader() {
        self.progressHUD?.hide(animated: true)
        self.progressHUD = nil
    }
    
    
}
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
    
}
extension UIImage {
    
    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
        
        self.init(data: imageData)
    }
    
    
    
}

//For Cache Image
 let imageCache = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    
    func loadImagesUsingCacheWithUrlString(urlString: String) {
        
        self.image = nil
        
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cachedImage
            return
            
        }
        
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async  {
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
        //          At this point if let downloadedImage = UIImage(data: data!) { you have a UIImage. If that's what you want then just use that and remove this stuff:
                    self.image = downloadedImage
                    
                    self.contentMode = .scaleAspectFill
                    self.translatesAutoresizingMaskIntoConstraints = false
                    
                }
                
            }
            
        }).resume()
        
    }
    
    
}
extension Array {
    init(repeating: [Element], count: Int) {
        self.init([[Element]](repeating: repeating, count: count).flatMap{$0})
    }
    
    func repeated(count: Int) -> [Element] {
        return [Element](repeating: self, count: count)
    }
}
extension UIViewController {
    func showAlert(title: String, message: String?) {
        self.view.hideLoader()
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title:"Ok", style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    func add(_ child: UIViewController) {
        addChildViewController(child)
        view.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    
    func remove() {
        guard parent != nil else {
            return
        }
        willMove(toParentViewController: nil)
        removeFromParentViewController()
        view.removeFromSuperview()
    }
    
    func removeChildViewControllers() {
        if self.childViewControllers.count > 0{
            let viewControllers:[UIViewController] = self.childViewControllers
            for viewContoller in viewControllers{
                viewContoller.willMove(toParentViewController: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParentViewController()
            }
        }
    }
    
}
extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
    
    var millisecondsSince1970: Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init?(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    
    
}
extension UISearchBar {
    func changeSearchBarColor(color: UIColor) {
        UIGraphicsBeginImageContext(self.frame.size)
        color.setFill()
        UIBezierPath(rect: self.frame).fill()
        let bgImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        self.setSearchFieldBackgroundImage(bgImage, for: .normal)
    }
}

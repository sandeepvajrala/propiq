//
//  LogFile.swift
//  PropIQ
//
//  Created by Shashank K. Atray on 24/05/18.
//  Copyright @ 2018 Tangerine Creative labs . All rights reserved.
//

import UIKit

class LogFile: NSObject {
    static var logNum = 0
    
    static func addToLog(_ dataToWrite: String, forFunction: String, withUserID: String) {
        
        do {
            let filePath = NSHomeDirectory() + "/Documents/" + "log.txt"
            
            let url = NSURL(fileURLWithPath: filePath)
           
            logNum = logNum + 1
            let stringtoWrite = "\(logNum)). Request: {\(dataToWrite)} DateTime: {\(NSDate())} Function:{\(forFunction)} UserID: {\(withUserID)}"
            try stringtoWrite.appendLineToURL(fileURL: url)
            
            _ = try String(contentsOf: url as URL)
        }
            
        catch {
           // print("Could not write to file")
        }
    }
    
    
    static func removeLogFile() {
        let fileManager = FileManager.default
        let filePath = NSHomeDirectory() + "/Documents/" + "log.txt"
        
        _ = NSURL(fileURLWithPath: filePath)
       // print("urlis\(url)")

        do {
            try fileManager.removeItem(atPath: filePath)
        } catch _ as NSError {
           // print(error.debugDescription)
        }
    }

   }

//
//  ConnectivityHelper.swift
//  miDriver
//
//  Created by Shashank K. Atray --- 11/05/2018.
//  Copyright © 2017 Tangerine Creative labs. All rights reserved.
//

import Foundation
import Alamofire
import MBProgressHUD



// get request value
public class ConnectivityHelper {
    
    @discardableResult class func executePOSTRequest(URLString: String, parameters: [String : AnyObject]?, headers: [String: String]?, success: @escaping(_ responsevalue: Any)->Void, failure: @escaping(_ error: Error?)->Void, notRechable: @escaping()->Void, sessionExpired: @escaping()->Void) -> DataRequest?
    {
       
        if !GlobalReachability.sharedInstance.reachability.isReachable {
            DispatchQueue.main.async {
                
                notRechable()
            }
            
            return nil
        }
        
        let dataRequest = Alamofire.request(URLString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseString { (response) in
            
            DispatchQueue.main.async {

                guard response.result.isSuccess else {
                    
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    
                    if let httpResponse = response.response {
                        if httpResponse.statusCode == 401 {
                            sessionExpired()
                            return
                        }
                        else if httpResponse.statusCode == 410 {
                            success(httpResponse)
                            
                            return
                        }
                    }
                    
                    failure(response.result.error)
                    return
                }
                
                if let jsonResponse = response.result.value {
                    success(jsonResponse)
                }
                else {
                    failure(nil)
                }
            }
        }
        
        return dataRequest
    }
    
    @discardableResult class func callPOSTRequest(URLString: String, parameters: [String : AnyObject]?, headers: [String: String]?, success: @escaping(_ responsevalue: Any)->Void, failure: @escaping(_ error: Error?)->Void, notRechable: @escaping()->Void, sessionExpired: @escaping()->Void) -> DataRequest?
    {
        if !GlobalReachability.sharedInstance.reachability.isReachable {
            DispatchQueue.main.async {
                
                notRechable()
            }
            
            return nil
        }

        
        let dataRequest = Alamofire.request(URLString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            
            
            DispatchQueue.main.async {
                
                guard response.result.isSuccess else {
                    
                    print("Error while fetching remote rooms: \(String(describing: response.result.error))")
                    
                    if let httpResponse = response.response {
                        if httpResponse.statusCode == 401 {
                            sessionExpired()
                            return
                        }
                        else if httpResponse.statusCode == 410 {
                            success(httpResponse)
                            
                            return
                        }
                    }
                    
                    failure(response.result.error)
                    return
                }
                
                if let jsonResponse = response.result.value {
                    success(jsonResponse)
                }
                else {
                    failure(nil)
                }
            }
        }
        
        return dataRequest
       
    }
    
//    @discardableResult class func uploadImages(urlString: String, parameters: [String : AnyObject]?, imagesData:[Data]?, imageParamName: String!, headers: [String: String]?, success: @escaping(_ responsevalue: Any)->Void, failure: @escaping(_ error: Error?)->Void, notRechable: @escaping()->Void, sessionExpired: @escaping()->Void)
//    {
//        if !GlobalReachability.sharedInstance.reachability.isReachable {
//            DispatchQueue.main.async {
//                
//                notRechable()
//            }
//            
//            
//        }
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            if let imagesData = imagesData {
//                for imageData in imagesData {
//                    multipartFormData.append(imageData, withName: "\(imageParamName)[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
//                }
//            }
//            if let params = parameters {
//                for (key, value) in params {
//                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                }
//            }
//            
//        }, to: urlString) { (encodingResult) in
//            switch encodingResult {
//            case .success(let upload, _, _):
//                upload.responseJSON { response in
//                    
//                }
//            case .failure(let error):
//                print(error)
//            }
//        }
//        
//   
//    
//}
    
    // post request value
    @discardableResult class func executeGETRequest(URLString: String, parameters: [String : AnyObject]?, headers: [String: String]?, success: @escaping(_ responsevalue: Any)->Void, failure: @escaping(_ error: Error?)->Void, notRechable: @escaping()->Void, sessionExpired: @escaping()->Void) -> DataRequest?
    {
       
        if !GlobalReachability.sharedInstance.reachability.isReachable {
            DispatchQueue.main.async {
                notRechable()
            }
            return nil
        }
        
        let dataRequest = Alamofire.request(URLString, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseData { (response) in
            
            DispatchQueue.main.async {
                
                guard response.result.isSuccess else {
                   
                    if let httpResponse = response.response {
                        if httpResponse.statusCode == 401 {
                            sessionExpired()
                            return
                        }
                    }
                    failure(response.result.error)
                    return
                }
                
                if let jsonResponse = response.result.value {
                    success(jsonResponse)
                } else{
                    failure(nil)
                }
            }
        }
        
        return dataRequest
    }
}


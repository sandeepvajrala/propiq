//
//  KSCrashImplementationFile.m
//  miDriver
//
//  Created by AcmeMinds on 07/11/17.
//  Copyright © 2017 Acmeminds. All rights reserved.
//

#import "KSCrashImplementationFile.h"
#import <sys/utsname.h>

@interface KSCrashImplementationFile ()

@end

@implementation KSCrashImplementationFile

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// ======================================================================
#pragma mark - Basic Crash Handling -
// ======================================================================

- (void) installCrashHandler
{
    // Create an installation (choose one)
    //    KSCrashInstallation* installation = [self makeStandardInstallation];
  
    //    KSCrashInstallation* installation = [self makeHockeyInstallation];
    //    KSCrashInstallation* installation = [self makeQuincyInstallation];
    //    KSCrashInstallation *installation = [self makeVictoryInstallation];
    
    
    // Install the crash handler. This should be done as early as possible.
    // This will record any crashes that occur, but it doesn't automatically send them.
    
      KSCrashInstallation* installation = [self makeEmailInstallation];
    [installation install];
    //[KSCrash sharedInstance].deleteBehaviorAfterSendAll = KSCDeleteAlways; //KSCDeleteOnSucess; // TODO: Remove this
    
    
    // Send all outstanding reports. You can do this any time; it doesn't need
    // to happen right as the app launches. Advanced-Example shows how to defer
    // displaying the main view controller until crash reporting completes.
    [installation sendAllReportsWithCompletion:^(NSArray* reports, BOOL completed, NSError* error)
     {
         if(completed)
         {
             
             NSLog(@"Sent %d reports", (int)[reports count]);
         }
         else
         {
             NSLog(@"Failed to send reports: %@", error);
         }
     }];
}

- (KSCrashInstallation*) makeEmailInstallation
{
    
    
    NSString* emailAddress = @"dev@acmeminds.com";
    NSString *APPversion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString *OSVersion = [[UIDevice currentDevice] systemVersion];
    //NSString * CurrentDeviceName  = [[UIDevice currentDevice] localizedModel];
  
    NSString * DeviceName = deviceName();
    
    KSCrashInstallationEmail* email = [KSCrashInstallationEmail sharedInstance];
    email.recipients = @[emailAddress];
    email.subject = @"Younique Crash Report";
    email.message = [NSString stringWithFormat:@"App version : - %@,\n Operating system version : - %@,\n Device Model : - %@",APPversion, OSVersion, DeviceName];
    email.filenameFmt = @"crash-report-%d.txt.gz";
    
    [email addConditionalAlertWithTitle:@"Younique has crashed"
                                message:@"An unexpected error occured forcing the application to stop. Want to send the report to the developer?"
                              yesAnswer:@"REPORT"
                               noAnswer:@"CANCEL"];
    
    // Uncomment to send Apple style reports instead of JSON.
    [email setReportStyle:KSCrashEmailReportStyleApple useDefaultFilenameFormat:YES];
    
    return email;
}


NSString* deviceName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

///
//  AppDelegate.swift
//  PropIQCreativeLabs
//
//
//  Created by Tangerine Creative on 11/05/18.
//  Developer name : - Shashank K. Atray 
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var indicator = UIActivityIndicatorView()

    //Crashlytics implementation
    let objCrash = KSCrashImplementationFile()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        // crashlyticas objects implementation
     
        objCrash.installCrashHandler()
        
        //Keyboard Manager
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
//        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        // Reachability and internet connection check
        
        GlobalReachability.sharedInstance.reachability.whenReachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async() {
                if reachability.isReachableViaWiFi {
                    // print("Reachable via WiFi")
                } else {
                    // print("Reachable via Cellular")
                }
                
                self.setNetworkStatusBarConnected(true)
            }
        }
        
        // reachability instance for the value that shows the network connection to be false
        
        GlobalReachability.sharedInstance.reachability.whenUnreachable = { reachability in
            // this is called on a background thread, but UI updates must
            // be on the main thread, like this:
            DispatchQueue.main.async() {
                // print("Not reachable")
                
                self.setNetworkStatusBarConnected(false)
            }
        }
        
        do{
            try GlobalReachability.sharedInstance.reachability.startNotifier()
        } catch {
            // print("Unable to start notifier")
        }
        statusBarStyle()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func statusBarStyle() {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.white
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setNetworkStatusBarConnected(_ connected:Bool)
    {
        self.removeDefaultErrorAlert()
       
        
        
        if !connected
        {
            if let _ = self.window?.viewWithTag(88)
            {
                return
            }
            
            let vNetworkBar = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 80))
            vNetworkBar.tag = 88
            vNetworkBar.backgroundColor = UIColor.black
            
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.tag = 81
            activityIndicator.tintColor = UIColor.appThemeColor()
            activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
            activityIndicator.startAnimating()
   
            
            var spinnerFrame = activityIndicator.frame
            spinnerFrame.origin.x = vNetworkBar.bounds.midX
            spinnerFrame.origin.y = 30
            activityIndicator.frame = spinnerFrame
            
            let lblTitle = UILabel(frame: CGRect(x: 20, y: 20 , width: vNetworkBar.frame.size.width - 40, height: vNetworkBar.frame.size.height - 40 ))
            lblTitle.tag = 82
            lblTitle.textColor = UIColor.white
            lblTitle.textAlignment = .center
           
            lblTitle.minimumScaleFactor = 8/lblTitle.font.pointSize
            lblTitle.numberOfLines = 3
            lblTitle.text = "We can't reach our network right now. Please check your internet connection."
            //            lblTitle.textAlignment = NSTextAlignment.justified
            lblTitle.adjustsFontSizeToFitWidth = true
            vNetworkBar.addSubview(lblTitle)
            
            var frameNetworkBar = vNetworkBar.frame
            frameNetworkBar.origin.y = -frameNetworkBar.size.height
            vNetworkBar.frame = frameNetworkBar
            
            self.window?.addSubview(vNetworkBar)
            
            UIView.animate(withDuration: 1.0, animations: {
                frameNetworkBar = vNetworkBar.frame
                frameNetworkBar.origin.y = 0
                vNetworkBar.frame = frameNetworkBar
            })
        }
        else
        {
            if let vNetworkBar = self.window?.viewWithTag(88)
            {
                vNetworkBar.viewWithTag(81)?.removeFromSuperview()
                
                let lblTitle = vNetworkBar.viewWithTag(82) as! UILabel
                lblTitle.text = "We are now connected to internet."
                //                lblTitle.textColor = UIColor.black
               
                lblTitle.textAlignment = NSTextAlignment.center
                
                var labelFrame = lblTitle.frame
                labelFrame.origin.x = 0
                labelFrame.size.width = vNetworkBar.frame.size.width
                lblTitle.frame = labelFrame
                
                vNetworkBar.backgroundColor = UIColor(red: 0/255.0, green: 100.0/255.0, blue: 0/255.0, alpha: 1.0)
                
                UIView.animate(withDuration: 1.0, delay: 1.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
                    
                    var frameNetworkBar = vNetworkBar.frame
                    frameNetworkBar.origin.y = -frameNetworkBar.size.height
                    vNetworkBar.frame = frameNetworkBar
                    
                }, completion: { (finished) in
                    
                    vNetworkBar.removeFromSuperview()
                })

            }
        }
    }


    func showIndicator(){
        indicator = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.height)) as UIActivityIndicatorView
        indicator.layer.cornerRadius = 3.0
        indicator.clipsToBounds = true
        indicator.hidesWhenStopped = true
        indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray;
        indicator.center = (window?.rootViewController?.view.center)!
        indicator.startAnimating()
        //        UIApplication.shared.beginIgnoringInteractionEvents()
        window?.rootViewController?.view.addSubview(indicator)
    }
    func hideIndicator(){
        indicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
        indicator.removeFromSuperview()
    }
}
func addActivityIndicator(withText text: String = String()) -> UIView {
    let indicatorView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width,
                                             height: UIScreen.main.bounds.height))
    indicatorView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4029751712)
    indicatorView.tag = 1000
    
    let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    activityView.frame = CGRect(x: indicatorView.frame.midX - 25, y: indicatorView.frame.midY - 25,
                                width: 50, height: 50)
    activityView.color = UIColor.white
    activityView.startAnimating()
    
    let textLabel = UILabel(frame: CGRect(x: 0, y: indicatorView.frame.midY + 25,
                                          width: UIScreen.main.bounds.width, height: 40))
    textLabel.textColor = UIColor.white
    textLabel.textAlignment = .center
    textLabel.numberOfLines = 0
    textLabel.text = text
    
    indicatorView.addSubview(activityView)
    indicatorView.addSubview(textLabel)
    
    UIApplication.shared.keyWindow?.addSubview(indicatorView)
    
    return indicatorView
}

/// Removes the activity indicator
func removeIndicator() {
    let keyWindow = UIApplication.shared.keyWindow
    keyWindow?.viewWithTag(1000)?.removeFromSuperview()
}






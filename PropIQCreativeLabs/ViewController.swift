//
//  ViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import PBRevealViewController

class ViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationToNext()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func navigationToNext() {
        if UserDetails.sharedInstance.isUserLogIn() {
            
            let userWorkStaus = UserDetails.sharedInstance.user_workStatus
            switch userWorkStaus {
            case "1" :
                let navVC = UIStoryboard(name: "landingTabbar", bundle: nil).instantiateViewController(withIdentifier: "PropIQNavigationController") as! PropIQNavigationController
                
                self.present(navVC, animated: false, completion: {
                    
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                    
                })
            case "2" :
                let navVC = UIStoryboard(name: "Frontdesk", bundle: nil).instantiateViewController(withIdentifier: "PBRevealViewController") as! PBRevealViewController
                
                self.present(navVC, animated: false, completion: {
                    
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                    
                })
            case "3" :
                let navVC = UIStoryboard(name: "Maintenance", bundle: nil).instantiateViewController(withIdentifier: "MaintenanceNavigationController") as! PropIQNavigationController
                
                self.present(navVC, animated: false, completion: {
                    
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                    
                })
                
            case "4" :
                let navVC = UIStoryboard(name: "PropertyManager", bundle: nil).instantiateViewController(withIdentifier: "PMPBRevealViewController") as! PBRevealViewController
                self.present(navVC, animated: false, completion: {
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                })
                
            default : return
        
            }
            
        } else {
            performSegue(withIdentifier: "MoveToLogin", sender: self)
        }
    }
    
    // api call value 
    func apiCallForTesting()
    {
       
        self.view.endEditing(true)
     
      
        var requestParams = [String : AnyObject]()
        self.view.endEditing(true)
        
        requestParams["user_id"] = "lohani10" as AnyObject
        requestParams["user_pass"] = "Qwerty123" as AnyObject
        requestParams["OSType"] = "iOS" as AnyObject
        requestParams["DeviceModel"] = UIDevice.current.model as AnyObject
        requestParams["OSVersion"] = UIDevice.current.systemVersion as AnyObject
        requestParams["AppVersion"] = String(format: "%@", Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! CVarArg) as AnyObject
        let deviceUUID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        requestParams["DeviceID"] = deviceUUID as AnyObject
  
        print(requestParams)
  
        
        //https://f1e1bcf0.ngrok.io/PropIQ/api    34.215.99.251:8080/api   34.215.99.251:8080/User_Login
        
        _ = ConnectivityHelper.executePOSTRequest(URLString: "https://b68e3a8c.ngrok.io/PropIQ/User_Login", parameters: requestParams, headers: nil, success: { (response) in
            
             print("response: \(response)")

            
        },  failure: { (error) in
           
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }

}


//
//  ExpenceVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 01/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ExpenceVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var expensesArray = [Expense]()
    var params: [String: Any]?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 160
        tableView.rowHeight = UITableViewAutomaticDimension
        params = ["department_id":"","month_id":""]
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterNotification(_:)), name: NSNotification.Name("FilterDetails"), object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let params = params {
            apiCallForAllGetExpences(params: params)

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Methods
    
    @objc func filterNotification(_ notification: NSNotification) {
        let dict = notification.userInfo as? [String: Any]
        if let params = dict {
            apiCallForAllGetExpences(params: params)
        }
    }
    
    func apiCallForAllGetExpences(params: [String: Any]){
        
        self.view.showLoader()
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.getExpenses, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            print(response)
            do {
                let expenses = try JSONDecoder().decode(Expenses.self, from: response as! Data)
                self.expensesArray = expenses.expenses
                self.tableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    
    @IBAction func addExpensesPressed(_ sender: Any) {
        guard let addExpenseVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "AddExpencesVC") as? AddExpencesVC else { return }
        self.present(addExpenseVC, animated: true, completion: nil)
    }
}

//MARK: - Tableview Methods
extension ExpenceVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expensesArray.count > 0 ? expensesArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseTableCell") as? ExpenseTableCell else { return UITableViewCell() }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ExpenseTableCell else { return }
        if expensesArray.count > 0 {
            cell.configureCell(expenses: expensesArray[indexPath.row])
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let addExpenseVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "AddExpencesVC") as? AddExpencesVC {
            addExpenseVC.expenses = expensesArray[indexPath.row]
            self.present(addExpenseVC, animated: true, completion: nil)
        }
    }
    
}
extension ExpenceVC: ExpenseFilterDelegate {
    func updateExpenseFilterDetails(departmentID: String, monthId: String) {
        params = ["department_id":departmentID ,"month_id": monthId]
    }

}






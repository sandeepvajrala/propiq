//
//  ImagesListViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import SDWebImage
protocol UpdateVC: class {
    func removeChilds()
}

class ImagesListViewController: UIViewController {
    
    //Variables
    var imagesArray: [Image]?
    weak var delegate: UpdateVC?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fullScreenImage(image: UIImage) {
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        self.navigationController?.isNavigationBarHidden = true
        sender.view?.removeFromSuperview()
    }
    func hideL()
    {
        self.view.hideLoader()
    }
    
//MARK: - Actions
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        delegate?.removeChilds()
        self.remove()
    }
    
}

extension ImagesListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesArray?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagesCell", for: indexPath) as? ImagesCell else { return UICollectionViewCell()}
        guard let imagesArr = imagesArray else { return UICollectionViewCell() }
        let imageUrl = imagesArr[indexPath.row].imagePath
    
        cell.imagesView.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "loading"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.size.width / 2) - 2 * 5 - 10
        let height = (collectionView.frame.size.height / 2) - 50 - 50
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.view.showLoader()
        DispatchQueue.global(qos: .background).async {
            do {
                guard let imagesArr = self.imagesArray else { return }
                let imageUrl = imagesArr[indexPath.row].imagePath
                let data = try Data(contentsOf: URL(string: imageUrl)!)
                
                DispatchQueue.main.async {
                    self.view.hideLoader()
                    let image = UIImage(data: data) ?? nil
                    if let image = image {
                        self.fullScreenImage(image: image)
                    }
                }
            }
            catch {
                // error
                DispatchQueue.main.async {
                    self.view.hideLoader()
                }
            }
        }
        
    }
    
    
}


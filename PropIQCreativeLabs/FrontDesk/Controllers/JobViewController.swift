//
//  JobViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 18/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController

class JobViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var tblJobListing: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var jobsLabel: UILabel!
    @IBOutlet weak var noJobsFoundLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    
    // Variables
    var isIndexPathSelected = 0
    var selectedCellIndexPath: IndexPath?
    var selectedCellHeight: CGFloat = 240.0
    let unselectedCellHeight: CGFloat = 140.0
    var jobsArray:[Job]?
    let dimmingView = UIView()
    var params = [String : Any]()
    var currentJobsArray = [Job]()
    var expenseFilter = false
    var fromPM: Bool = false
    
// MARK : - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupForSearchBar()
        if !fromPM {              //This check is for property manager
            UserDefaults.standard.removeObject(forKey: "index")
            menuButton.isHidden = false
            menuButton.addTarget(self.revealViewController(), action: #selector(PBRevealViewController.revealLeftView), for: UIControlEvents.touchUpInside)
            
            self.revealViewController()!.delegate = self
            self.revealViewController()!.leftToggleSpringDampingRatio = 1.0
            params = ["job_status":"0","job_priority":"0","user_id":UserDetails.sharedInstance.User_ID,"department_id":"0"]
        } else {
            setupForPM()
            setTitle()
        }

        apiCallForAllJobs(params: params)
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UserDefaults.standard.removeObject(forKey: "jobStatus")
        UserDefaults.standard.removeObject(forKey: "departmentId")
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: Constants.notificationName, object: nil)
        selectedCellIndexPath = nil
        tblJobListing.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK : - Methods
    
    @objc func backTapForPM(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func singleTap(sender: UITapGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    func setupForSearchBar(){
        searchBar.backgroundImage = UIImage()
        searchBar.changeSearchBarColor(color: #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1))
        let textFieldInsideSearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        let textFieldInsideSearchBarLabel = textFieldInsideSearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideSearchBarLabel?.textColor = UIColor.white
        searchBar.setImage(UIImage(), for: .search, state: .normal)
        searchBar.delegate = self
        let singleTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.singleTap(sender:)))
        singleTapGestureRecognizer.numberOfTapsRequired = 1
        singleTapGestureRecognizer.isEnabled = true
        singleTapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(singleTapGestureRecognizer)
    }

    func setupForPM() {
        menuButton.isHidden = true
        let backButonForPM = UIButton(frame: CGRect(x: 10, y: 0, width: 40, height: 40))
        backButonForPM.setImage(#imageLiteral(resourceName: "back"), for: .normal)
        backButonForPM.addTarget(self, action: #selector(self.backTapForPM(sender:)), for: .touchUpInside)
        navigationView.addSubview(backButonForPM)
        self.tabBarController?.tabBar.isHidden = true
        addButton.isHidden = true
    }
    
    @objc func onNotification(notification:Notification) {
        
        let dict = notification.userInfo as? [String: Any]
        setTitle()
        removeChildViewControllers()
        if let dict = dict {
            expenseFilter = false
            guard let _ = dict["job_status"] as? String else { return }
            apiCallForAllJobs(params: dict)
            self.selectedCellIndexPath = nil
            
        } else {
            let index = UserDefaults.standard.value(forKey: "index")
            if let index = index as? Int {
                switch index {
                case 8:
                    self.jobsLabel.text = "Expense Jobs"
                    if let expenseVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "ExpenceVC") as? ExpenceVC {
                        expenseFilter = true
                        self.add(expenseVC)
                        expenseVC.view.frame = CGRect(x: 0, y: 70, width: self.view.frame.width, height: self.view.frame.height - 70)
                        
                    }
                case 9: self.jobsLabel.text = "CheckIn - CheckOut"
                if let checkVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "CheckInCheckOutVC") as? CheckInCheckOutVC {
                    self.add(checkVC)
                    checkVC.view.frame = CGRect(x: 0, y: 70, width: self.view.frame.width, height: self.view.frame.height - 70)
                    }
                default:
                    break
                }
            }
            
        }
    }
    
    func apiCallForAllJobs(params: [String: Any]) {
        self.view.showLoader()
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.JobListing, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let jobDetails = try JSONDecoder().decode(JobsData.self, from: response as! Data)
                self.jobsArray = jobDetails.jobs
                self.currentJobsArray = jobDetails.jobs
                if self.currentJobsArray.count == 0 {
                    self.noJobsFoundLabel.isHidden = false
                } else {
                    self.noJobsFoundLabel.isHidden = true
                }
                self.selectedCellIndexPath = nil
                self.tblJobListing.reloadData()
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
       
    }
    
    func apiCallForAproveOrRecjectJob(params: [String: Any]) {
        self.view.showLoader()
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.approveOrRejectJob, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let jobDetails = try JSONDecoder().decode(ApproveOrReject.self, from: response as! Data)
                let status = jobDetails.status
                if status {
                    self.apiCallForAllJobs(params: ["job_status":"5","job_priority":"0","user_id": UserDetails.sharedInstance.User_ID,"department_id":"0"])
                }else {
                    self.showAlert(title: "Alert", message: "Approve job is not done please try after some time")
                }
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
    }
    func setTitle() {
        let index = UserDefaults.standard.value(forKey: "index")
        if let index = index as? Int {
            switch index {
            case 0: jobsLabel.text = "All Jobs"
            case 1: jobsLabel.text = "Maintanance Jobs"
            case 2: jobsLabel.text = "HousekeepingJobs"
            case 3: jobsLabel.text = "NotApproved Jobs"
            case 4: jobsLabel.text = "Pending Jobs"
            case 5: jobsLabel.text = "Inprogress Jobs"
            case 6: jobsLabel.text = "Completed Jobs"
            case 7: jobsLabel.text = "Rejected Jobs"
            case 8: jobsLabel.text = "Expense Jobs"
            case 9: jobsLabel.text = "CheckIn - CheckOut"
            default: return
            }
        }
    }
   
    //MARK: - Actions
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        searchButton.isHidden = true
        searchBar.becomeFirstResponder()
        menuButton.isHidden = true
        searchBar.isHidden = false
        backButton.isHidden = false
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        searchBar.endEditing(true)
        searchBar.text = ""
        tblJobListing.reloadData()
        searchButton.isHidden = false
        menuButton.isHidden = false
        searchBar.isHidden = true
        backButton.isHidden = true
    }
    
    @IBAction func filterButtonPressed(_ sender: Any) {
        if expenseFilter {
            guard let filterVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "ExpenseFilterVC") as? ExpenseFilterVC else { return }
            self.add(filterVC)
        } else {
            guard let filterVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "FilterVC") as? FilterVC else { return }
            filterVC.delegate = self
            self.add(filterVC)
        }
        
    }
    
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        let status = UserDefaults.standard.value(forKey: "jobStatus") as? String ?? nil
        if status == "5"{
            
            guard let index = selectedCellIndexPath?.row else {return }
            let jobId = currentJobsArray[index].jobID
            let aproveParams = ["job_id":"\(jobId)","job_status":"3"]
            apiCallForAproveOrRecjectJob(params: aproveParams)
        } else {
            guard let acceptVC = self.storyboard!.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController else {return}
            
            guard let index = selectedCellIndexPath?.row else {return }
            acceptVC.delegate = self
            acceptVC.jobsDetails = currentJobsArray[index]
            
            self.add(acceptVC)
        }
    }
    
    @IBAction func rejectBtnPressed(_ sender: Any) {
        let status = UserDefaults.standard.value(forKey: "jobStatus") as? String ?? nil
        if status == "5"{
            
            guard let index = selectedCellIndexPath?.row else {return }
            let jobId = currentJobsArray[index].jobID
            let aproveParams = ["job_id":"\(jobId)","job_status":"1"]
            apiCallForAproveOrRecjectJob(params: aproveParams)
        } else {
            let rejectionVC = UIStoryboard(name: "JobsDetail", bundle: nil).instantiateViewController(withIdentifier: "RejectionVC") as! RejectionVC
            rejectionVC.delegate = self
            if let index = selectedCellIndexPath?.row {
                rejectionVC.jobId = currentJobsArray[index].jobID
                
            }
            self.present(rejectionVC, animated: true, completion: nil)
        }
        
    }
    @IBAction func addButtonTapped(_ sender: Any) {
        guard let addVC = self.storyboard?.instantiateViewController(withIdentifier: "AddViewController") as? AddViewController else { return }
        addVC.delegate = self
        self.add(addVC)
    }
    @IBAction func viewImagesBtnTapped(_ sender: UIButton) {
        if let imagesVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "ImagesPopupVC") as? ImagesPopupVC {
            
            imagesVC.jobID = currentJobsArray[sender.tag].jobID
            self.add(imagesVC)
        
        }
    }
    
    
}

//MARK: - Tableview Methods
extension JobViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return currentJobsArray.count > 0 ? currentJobsArray.count : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableCell") as? JobTableCell else { return UITableViewCell() }
        cell.viewImageBtn.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let jobsCell = cell as? JobTableCell {
            if currentJobsArray.count > 0 {
                if fromPM {
                   jobsCell.fromPM = true
                }
                jobsCell.configureCell(jobsArr: currentJobsArray[indexPath.row])
                if selectedCellIndexPath == nil {
                    jobsCell.setSelected(false, animated: true)
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if currentJobsArray.count > 0 {
            let count = currentJobsArray[indexPath.row].detail.count
            selectedCellHeight = CGFloat(count * 30 + 70 + 120 + 20)
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! JobTableCell
        tableView.beginUpdates()
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            selectedCellIndexPath = nil
            cell.setSelected(false, animated: true)
//            cell.isSelection = false

        } else {
            selectedCellIndexPath = indexPath
            cell.setSelected(true, animated: true)
//            cell.isSelection = true

        }
       
        
        tableView.endUpdates()

    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCellIndexPath == indexPath {
            return selectedCellHeight
        }
        return unselectedCellHeight
        
    }
}

//MARK: ReavealController Delegate Methods

extension JobViewController: PBRevealViewControllerDelegate {
    func revealController(_ revealController: PBRevealViewController, willShowLeft viewController: UIViewController) {
        dimmingView.frame = self.view.bounds
        self.view.addSubview(self.dimmingView)
        self.dimmingView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        revealController.mainViewController?.view.isUserInteractionEnabled = false
        revealController.leftViewRevealWidth = 300
    }
    func revealController(_ revealController: PBRevealViewController, willHideLeft viewController: UIViewController) {
        self.dimmingView.removeFromSuperview()
        revealController.mainViewController?.view.isUserInteractionEnabled = true
    }
    
}
extension JobViewController: FilterDataDelegate {
    func fileredArray(details: [Job]) {
        self.currentJobsArray = details
        self.tblJobListing.reloadData()
    }
}

//MARK: - SearchBar Delegate Methods

extension JobViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let jobsArr = jobsArray else { return }
        
        guard !searchText.isEmpty else {
            currentJobsArray = jobsArr
            tblJobListing.reloadData()
            return
        }
        
        currentJobsArray = jobsArr.filter({ (item) -> Bool in
            if item.roomNo.lowercased().contains(searchText.lowercased()) {
                return true
            } else if item.workerDetails.department.lowercased().contains(searchText.lowercased()){
                return true
            } else if item.workerDetails.workerFirstName.lowercased().contains(searchText.lowercased()){
                return true
            } else if item.workerDetails.workerLastName.lowercased().contains(searchText.lowercased()){
                return true
            } else {
                return false
            }
        })

        tblJobListing.reloadData()
    }
    
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
}
extension JobViewController: UploadedJobDelegate, RejectionDelegate {
    func updateJobsVC() {
        apiCallForAllJobs(params: params)
    }
    
    func updaloadJob() {
        apiCallForAllJobs(params: params)
    }
    
    
}


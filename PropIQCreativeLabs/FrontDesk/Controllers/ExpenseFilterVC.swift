//
//  ExpenseFilterVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 08/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

protocol ExpenseFilterDelegate: class {
    func updateExpenseFilterDetails(departmentID: String, monthId: String)
}

class ExpenseFilterVC: UIViewController {
    
    @IBOutlet weak var expenseFilterView: UIView!
    @IBOutlet weak var expenseSelectDeopartment: UIButton!
    @IBOutlet weak var selectMonth: UIButton!
    
    var selection: Selected?
    var departmentsArray = ["Select Department","House Keeping","Maintainance"]
    var monthsArray = ["Select Month","January","February","March","April","May","June","July","August","September","October","November","December"]
    var selectedDepartment = ""
    var selectedMonthId = ""
    var selectedUserId = ""
    let myView = UIView()
    let monthView = UIView()
    var expensesArray: [Expense]?
    var selectedMonth = false
    weak var delegate: ExpenseFilterDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Expense Actions
    
    @IBAction func expenseSubmitBtnPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("FilterDetails"), object: nil, userInfo: ["department_id":selectedDepartment ,"month_id": selectedMonthId] )
        self.remove()
//        delegate?.updateExpenseFilterDetails(departmentID: selectedDepartment, monthId: selectedMonthId)
    }
    
    @IBAction func expenseResetPressed(_ sender: UIButton) {
        expenseSelectDeopartment.setTitle("Select Department", for: .normal)
        selectedDepartment = ""
        selectMonth.setTitle("Select month", for: .normal)
        selectedMonthId = ""
    }
    
    @IBAction func expenseDepartmentPressed(_ sender: UIButton) {
        myView.frame = CGRect(x: 0, y: 30, width: expenseFilterView.frame.width , height: 110)
        expenseFilterView.addSubview(myView)
        myView.isHidden = false
        monthView.isHidden = true
        let departmentTable = UITableView()
        departmentTable.frame = CGRect(x: myView.frame.origin.x, y: myView.frame.origin.y, width: myView.frame.width, height: 110)
        departmentTable.separatorStyle = .none
        departmentTable.delegate = self
        departmentTable.dataSource  = self
        myView.addSubview(departmentTable)
        
    }
    @IBAction func expenseMonthPressed(_ sender: UIButton) {
        selectedMonth = true
        monthView.isHidden = false
        myView.isHidden = true
        monthView.frame = CGRect(x: expenseFilterView.frame.origin.x + 20 , y: expenseFilterView.frame.origin.y, width: 120 , height: self.view.frame.height - expenseFilterView.frame.origin.y-60-20)
        self.view.addSubview(monthView)
       
        let departmentTable = UITableView()
        departmentTable.frame = CGRect(x: myView.frame.origin.x, y: myView.frame.origin.y, width: 200 , height: monthView.frame.height)
        departmentTable.separatorStyle = .none
        departmentTable.delegate = self
        departmentTable.dataSource  = self
        monthView.addSubview(departmentTable)
    }
    @IBAction func coverButtonPressed(_ sender: UIButton) {
        self.remove()
    }
    
}

extension ExpenseFilterVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedMonth ? monthsArray.count : departmentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if selectedMonth {
            cell.textLabel?.text = monthsArray[indexPath.row]
        } else{
            cell.textLabel?.text = departmentsArray[indexPath.row]
        }
        cell.selectionStyle = .none
        cell.textLabel?.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedMonth {
            
            selectedMonth = false
            self.monthView.isHidden = true
            selectedMonthId = "\(indexPath.row)"
            selectMonth.setTitle(monthsArray[indexPath.row], for: .normal)
            
        } else {
            
            self.myView.isHidden = true
            switch indexPath.row {
            case 0:
                selectedDepartment = ""
            case 1:
                selectedDepartment = "1"
            case 2:
                selectedDepartment = "3"
            default: break
                
            }
            expenseSelectDeopartment.setTitle(departmentsArray[indexPath.row], for: .normal)
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return selectedMonth ? 30 : 25
        
    }
}

//
//  ImagesPopupVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

struct ImagesList: Codable {
    let images: [Image]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct Image: Codable {
    let imagePath: String
    let jobID, id, imageStatus: Int
    
    enum CodingKeys: String, CodingKey {
        case imagePath = "image_path"
        case jobID = "job_id"
        case id
        case imageStatus = "image_status"
    }
}

class ImagesPopupVC: UIViewController {
    
    var jobID: Int?
    var imageStaus = String()
    var imagesArray = [Image]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Methods
    func apiCallForGetImages() {
        
        _ = addActivityIndicator()
        guard let jobId = jobID else {return}
        let params = ["job_id":"\(jobId)","image_status":"\(imageStaus)"]

       
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.getImages, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            print(response)
            do {
                let imagesList = try JSONDecoder().decode(ImagesList.self, from: response as! Data)
                self.imagesArray = imagesList.images
                
                if let imagesVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "ImagesListViewController") as? ImagesListViewController {
                    imagesVC.delegate = self
                    imagesVC.imagesArray = imagesList.images
                    self.add(imagesVC)
//                    self.present(imagesVC, animated: true, completion: nil)
//
                }
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    
//MARK:- ACtions
    @IBAction func coverButtonTapped(_ sender: Any) {
        self.remove()
    }
    
    @IBAction func beforeImageBtnTapped(_ sender: Any) {
        imageStaus = "0"
        
        apiCallForGetImages()
       
    
    }
    @IBAction func afterImageBtnTapped(_ sender: Any) {
        imageStaus = "1"
        apiCallForGetImages()
        
    }
    
}
extension ImagesPopupVC: UpdateVC {
    func removeChilds() {
        self.remove()
    }
    
    
}

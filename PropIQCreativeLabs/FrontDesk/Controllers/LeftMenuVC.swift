//
//  LeftMenuVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 19/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController


struct MenuItems {
    var title: String?
    var image: UIImage?
    
    func data(title: String,image: UIImage) -> MenuItems {
        let item = MenuItems(title: title, image: image)
        return item
    }
    
}

class LeftMenuVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    var items = MenuItems()
    var iconsWithImages = [MenuItems]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iconsWithImages.append(items.data(title: "All jobs", image: #imageLiteral(resourceName: "All")))
        iconsWithImages.append(items.data(title: "Maintenance Job", image: #imageLiteral(resourceName: "maintenance")))
        iconsWithImages.append(items.data(title: "HouseKeeping Jobs", image: #imageLiteral(resourceName: "cleaning")))
        iconsWithImages.append(items.data(title: "Not Approved Jobs", image: #imageLiteral(resourceName: "notAprroved")))
        iconsWithImages.append(items.data(title: "Pending Jobs", image: #imageLiteral(resourceName: "wait")))
        iconsWithImages.append(items.data(title: "InProgress Jobs", image: #imageLiteral(resourceName: "progress")))
        iconsWithImages.append(items.data(title: "Completed Jobs", image: #imageLiteral(resourceName: "Check")))
        iconsWithImages.append(items.data(title: "Rejected Jobs", image: #imageLiteral(resourceName: "reject")))
        iconsWithImages.append(items.data(title: "Expense", image: #imageLiteral(resourceName: "money")))
        iconsWithImages.append(items.data(title: "Check-In Check-Out", image: #imageLiteral(resourceName: "Check in")))
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension LeftMenuVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return iconsWithImages.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as? MenuTableCell else { return UITableViewCell() }
        cell.nameLabel.text = iconsWithImages[indexPath.row].title
        cell.iconImageView.image = iconsWithImages[indexPath.row].image
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let id = UserDetails.sharedInstance.User_ID
        let statusKey = "jobStatus"
        let departmentKey = "departmentId"
        switch indexPath.row {
           
        case 0:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"0","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("0", forKey: departmentKey)
            UserDefaults.standard.removeObject(forKey: statusKey)
            revealViewController()?.hideLeftView(animated: true)
        case 1:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"0","job_priority":"0","user_id": id,"department_id":"3"])
            UserDefaults.standard.set("3", forKey: departmentKey)
            UserDefaults.standard.removeObject(forKey: statusKey)
            revealViewController()?.hideLeftView(animated: true)
        case 2:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"0","job_priority":"0","user_id": id,"department_id":"1"])
            UserDefaults.standard.set("1", forKey: departmentKey)
            UserDefaults.standard.removeObject(forKey: statusKey)
            revealViewController()?.hideLeftView(animated: true)
        case 3:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"5","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("5", forKey: statusKey)
            UserDefaults.standard.removeObject(forKey: departmentKey)
            revealViewController()?.hideLeftView(animated: true)
        case 4:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"1","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("1", forKey: statusKey)
            UserDefaults.standard.removeObject(forKey: departmentKey)
            revealViewController()?.hideLeftView(animated: true)
    
        case 5:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"2","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("2", forKey: statusKey)
            UserDefaults.standard.removeObject(forKey: departmentKey)
            revealViewController()?.hideLeftView(animated: true)
        case 6:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"3","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("3", forKey: statusKey)
            UserDefaults.standard.removeObject(forKey: departmentKey)
            revealViewController()?.hideLeftView(animated: true)
        case 7:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo:["job_status":"4","job_priority":"0","user_id": id,"department_id":"0"])
            UserDefaults.standard.set("4", forKey: statusKey)
            UserDefaults.standard.removeObject(forKey: departmentKey)
            revealViewController()?.hideLeftView(animated: true)
        case 8:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo: nil)
            revealViewController()?.hideLeftView(animated: true)
        case 9:
            UserDefaults.standard.set(indexPath.row, forKey: "index")
            NotificationCenter.default.post(name: Constants.notificationName, object: nil, userInfo: nil)
            revealViewController()?.hideLeftView(animated: true)
            
        default:
            return
        }
    }
}

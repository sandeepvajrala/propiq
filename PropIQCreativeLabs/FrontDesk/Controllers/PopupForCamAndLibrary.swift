//
//  PopupForCamAndLibrary.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 02/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

protocol UpdateImageDetails: class {
    func imageDetails(name: String, image: UIImage)
}

class PopupForCamAndLibrary: UIViewController {

    @IBOutlet weak var mainView: UIView!
    
    var imagePicker = UIImagePickerController()
    
    weak var delegate: UpdateImageDetails?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setup() {
        mainView.round(enable: true, withRadius: 20)
        mainView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
    }
    
//MARK: - Actions
    @IBAction func coverButtonPressed(_ sender: Any) {
        self.remove()
    }
    
    @IBAction func takePhotoButtonTapped(_ sender: UIButton) {
        pickFromCamera()
    }
    
    @IBAction func chooseFromLibraryTapped(_ sender: UIButton) {
        pickImage()
    }
    
    @IBAction func cancelBtnTapped(_ sender: UIButton) {
        self.remove()
    }
    
    fileprivate func pickImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    fileprivate func pickFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showAlert(title: "Alert", message: "Something wrong")
        }
    }
}

//MARK: - ImagePicker Delegate Methods
extension PopupForCamAndLibrary: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true) {
        }
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.remove()
            delegate?.imageDetails(name: "\(Date().timeIntervalSince1970).jpg", image: pickedImage)
            
        }
    }
    
}



//
//  AddExpencesVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 01/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Alamofire
import DropDown

class AddExpencesVC: UIViewController {

    //Outlets
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var coverButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    
    //Variables
    var titlesArray = ["Select Department", "Amount", "Month", "Description","Image" ]
    var departmentsArray = ["Select Department", "House Keeping", "Maintainance"]
    var monthsArray = ["Select Month","January","February","March","April","May","June","July","August","September","October","November","December"]
    var selectedIndex = 0
    var image: UIImage?
    var selectedMonthIndex: Int?
    var expenses: Expense?
    var departmentsDropDown = DropDown()
    var monthsDropDown = DropDown()
    
    var amount: String?
    var descriptionString: String?
    
    //Life Cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let expense = expenses {
            submitButton.setTitle("Update", for: .normal)
            titlesArray = ["\(expense.department)","\(expense.amount)","\(monthsArray[expense.monthID])","\(expense.description)","image"]
        }
        setupForDepartmentsDropDown()
        setupForMonthsDropDown()
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


//MARK: - Methods

    func setupForDepartmentsDropDown() {
        departmentsDropDown.dismissMode = .automatic
        departmentsDropDown.cellHeight = 40.0
        departmentsDropDown.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
        departmentsDropDown.width = 200
        departmentsDropDown.dataSource = departmentsArray
    }
    
    func setupForMonthsDropDown() {
        monthsDropDown.dismissMode = .automatic
        monthsDropDown.cellHeight = 40.0
        monthsDropDown.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
        monthsDropDown.width = 200
        monthsDropDown.dataSource = monthsArray
    }
    
    func getParams() -> [String: Any]? {
        
        var params = [String :Any]()
        let workerId = UserDetails.sharedInstance.User_ID
        if let expense = expenses {
            let amount = self.amount ?? "\(expense.amount)"
            let description = descriptionString ?? expense.description
            params = ["expense_id": "\(expense.id)","department_id": "\(expense.departmentID)", "amount": amount, "month_id": "\(expense.monthID)", "description": description, "worker_id": workerId]
        } else {
            var departmentId = selectedIndex
            switch departmentId {
            case 0: self.showAlert(title: "Alert", message: "Department is mandatory"); return nil
            case 1: departmentId = 1
            case 2: departmentId = 3
            default: break
            }
            guard let amount = amount else { self.showAlert(title: "Alert", message: "Amount is mandatory"); return nil}
            guard let month = selectedMonthIndex else { self.showAlert(title: "Alert", message: "month is mandatory"); return nil }
            guard let discription = descriptionString else { self.showAlert(title: "Alert", message: "Description is mandatory"); return nil }
            
            params = ["expense_id": "","department_id": "\(departmentId)", "amount": amount, "month_id": "\(month)", "description": discription, "worker_id": workerId]
        }
        
        return params
        
    }
    
    func apiCallForAddAndUpdateExpense() {
        
        let params = self.getParams()
        if params == nil { return }
        self.view.showLoader()
        var imageData = Data()
        if let image = self.image {
            imageData = UIImageJPEGRepresentation(image, 0.2)!
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(imageData, withName: "Expense Images", fileName: "\(Date().timeIntervalSince1970).jpg", mimeType: "image/jpeg")
            for (key, value) in params! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: ApiFile.addUpdateExpense) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    self.view.hideLoader()
                    self.dismiss(animated: true, completion: nil)
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                self.view.hideLoader()
            }
        }
        
    }
    
    //MARK: - Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitButtonTapped(_ sender: Any) {
        apiCallForAddAndUpdateExpense()
    }
    @IBAction func coverButtonTapped(_ sender: Any) {
        coverButton.isHidden = true
    }
    
}

//MARK: - Tableview Methods
extension AddExpencesVC: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        
        switch row {
            case 0,2,4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentAndIamgeCell") as? DepartmentAndIamgeCell else { return UITableViewCell()}
                cell.isUserInteractionEnabled = true
                cell.titleLAbel.text = titlesArray[indexPath.row]
                guard let _ = expenses else { return cell}
                if row == 0 { cell.isUserInteractionEnabled = false }
                else if row == 2 { cell.isUserInteractionEnabled = false }
                
                return cell
            
            case 1:
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AmountAndMonthCell") as? AmountAndMonthCell else { return UITableViewCell()}
                cell.amountAndMonthTF.placeholder = titlesArray[row]
                cell.amountAndMonthTF.delegate = self
                return cell
            
            case 3:
               
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as? DescriptionCell else { return UITableViewCell() }
                cell.descriptionTextView.text = titlesArray[row]
                cell.descriptionTextView.delegate = self
                return cell
            
            default: break
        }
        return UITableViewCell()
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return indexPath.row == 3 ? 120 : 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? DepartmentAndIamgeCell else { return }
        
        switch indexPath.row {
        case 0:
            
            
            departmentsDropDown.anchorView = cell.titleLAbel
            departmentsDropDown.show()
            tableView.beginUpdates()
            departmentsDropDown.selectionAction = { (index: Int, item: String) in
                self.selectedIndex = index
                cell.titleLAbel.text = item
            }
            tableView.endUpdates()
            
        case 2:
            
            monthsDropDown.anchorView = cell.titleLAbel
            monthsDropDown.show()
            tableView.beginUpdates()
            monthsDropDown.selectionAction = { (index: Int, item: String) in
                self.selectedMonthIndex = index
                cell.titleLAbel.text = item
            }
            tableView.endUpdates()
            
        case 4:
            
            if let camPopupVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "PopupForCamAndLibrary") as? PopupForCamAndLibrary {
                camPopupVC.delegate = self
                self.add(camPopupVC)
            }
        default:
            break
        }
    }
    
}

//MARK: Updating Image
extension AddExpencesVC: UpdateImageDetails {
    
    func imageDetails(name: String, image: UIImage) {
        self.image = image
        mainTableView.beginUpdates()
        let cell = mainTableView.cellForRow(at: IndexPath(row: 4, section: 0)) as? DepartmentAndIamgeCell
        cell?.titleLAbel.text = name
        mainTableView.endUpdates()
    }
    
}

//MARK: Textfield & TextView Delegate Mehtods
extension AddExpencesVC: UITextFieldDelegate, UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        amount = textField.text
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == titlesArray[3]) {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            textView.text = titlesArray[3]
            textView.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
        } else {
            descriptionString = textView.text
        }
        textView.resignFirstResponder()
        
    }
}







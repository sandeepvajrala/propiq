//
//  FilterVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 30/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

protocol FilterDataDelegate: class {
    func fileredArray(details: [Job])
}

enum Selected {
    case yesterday
    case today
    case tomorrow
}

class FilterVC: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var yesterdayBtn: UIButton!
    @IBOutlet weak var tomorowBtn: UIButton!
    @IBOutlet weak var todayBtn: UIButton!
    @IBOutlet weak var dropDownTableView: UITableView!
    @IBOutlet weak var selectDepartment: UIButton!
    @IBOutlet weak var fromDateButton: UIButton!
    @IBOutlet weak var toDateButton: UIButton!
    @IBOutlet weak var userButton: UIButton!
    
    var selection: Selected?
    var departmentsArray = ["Select Department","House Keeping","Maintainance"]
    weak var delegate: FilterDataDelegate?
    var selectedDepartment = ""
    var fromDate = ""
    var toDate = ""
    var jobDate = ""
    var selectedUserId = ""
    var jobstatus = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectDepartment.isUserInteractionEnabled = true
        if let index = UserDefaults.standard.value(forKey: "index") as? Int {
            switch index {
            case 0: selectedDepartment = ""
                    self.selectDepartment.setTitle("Select Department", for: .normal)
            case 1: selectedDepartment = "3"
                    self.selectDepartment.setTitle("Maintainance", for: .normal)
                    selectDepartment.isUserInteractionEnabled = false
            case 2: selectedDepartment = "1"
                    self.selectDepartment.setTitle("House Keeping", for: .normal)
                    selectDepartment.isUserInteractionEnabled = false
            case 3: jobstatus = "5"
            case 4: jobstatus = "1"
            case 5: jobstatus = "2"
            case 6: jobstatus = "3"
            case 7: jobstatus = "4"
            default : return
            }
        } else {
            
        }
//        if let status = UserDefaults.standard.value(forKey: "jobStatus") as? String{
//            jobstatus = status
//        }
//        if let department = UserDefaults.standard.value(forKey: "departmentId") as? String{
//            selectedDepartment = department
//            if department == "1" {
//                self.selectDepartment.setTitle("House Keeping", for: .normal)
//            } else {
//                self.selectDepartment.setTitle("Maintainance", for: .normal)
//            }
//            selectDepartment.isUserInteractionEnabled = false
//        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view != dropDownView {
            dropDownView.isHidden = true
            mainView.backgroundColor = UIColor.white
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Actions
    
    @IBAction func selectionOfDatesBtnPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            selection = Selected.yesterday
        } else if sender.tag == 2 {
            selection = Selected.today
        } else {
            selection = Selected.tomorrow
        }
        if let selection = selection {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            
            switch selection {
            case .yesterday:
                yesterdayBtn.setImage(#imageLiteral(resourceName: "radioButtonSelected"), for: .normal)
                todayBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                tomorowBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                jobDate = formatter.string(from: Date().yesterday)
            case .today:
                todayBtn.setImage(#imageLiteral(resourceName: "radioButtonSelected"), for: .normal)
                tomorowBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                yesterdayBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                jobDate = formatter.string(from: Date())
            case .tomorrow:
                tomorowBtn.setImage(#imageLiteral(resourceName: "radioButtonSelected"), for: .normal)
                todayBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                yesterdayBtn.setImage(#imageLiteral(resourceName: "radioButton"), for: .normal)
                jobDate = formatter.string(from: Date().tomorrow)
            }
        }
    }
    
    @IBAction func fromDatePressed(_ sender: Any) {
       
        guard let dateVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "DateAndTimePopView") as? DateAndTimePopView else {return}
        dateVC.showTime = false
        dateVC.from = "FromDate"
        dateVC.fromDateDelegate = self
        self.add(dateVC)
    }
    
    @IBAction func toDatePressed(_ sender: UIButton) {
        
        guard let dateVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "DateAndTimePopView") as? DateAndTimePopView else {return}
        dateVC.showTime = false
        dateVC.from = "ToDate"
        dateVC.toDateDelegate = self
        self.add(dateVC)
    }
    
    @IBAction func selectDepartmentPressed(_ sender: Any) {
        
        self.dropDownView.isHidden = false
        self.mainView.backgroundColor = UIColor.groupTableViewBackground
        UIView.animate(withDuration: 0.3) {
            self.dropDownView.layoutIfNeeded()
        }
        
    }
    
    @IBAction func userButtonPressed(_ sender: Any) {
    
        guard let usersVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "UsersViewController") as? UsersViewController else {return}
        usersVC.departMentId = 0
        usersVC.from = "GetUser"
        usersVC.selectedUserDelegate = self
        self.add(usersVC)
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        apiCallForFilterJobs()
    }
    
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        self.selectDepartment.setTitle("Select Department", for: .normal)
        fromDateButton.setTitle("From Date", for: .normal)
        toDateButton.setTitle( "To Date", for: .normal)
        yesterdayBtn.setImage(#imageLiteral(resourceName: "unselectionICon"), for: .normal)
        todayBtn.setImage(#imageLiteral(resourceName: "unselectionICon"), for: .normal)
        tomorowBtn.setImage(#imageLiteral(resourceName: "unselectionICon"), for: .normal)
        selection = nil
        userButton.setTitle("By User", for: .normal)
        selectedDepartment = ""
        fromDate = ""
        toDate = ""
        jobDate = ""
        selectedUserId = ""
        
    }
    @IBAction func removeButton(_ sender: UIButton) {
        self.remove()
    }
    
    func apiCallForFilterJobs(){
        
        _ = addActivityIndicator()
        
        let params = ["job_status": jobstatus,"department_id": selectedDepartment,"job_date": jobDate,"from_date": self.fromDate,"to_date": self.toDate,"worker_id": selectedUserId]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.filterJobs, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            print(response)
            do {
                let jobDetails = try JSONDecoder().decode(JobsData.self, from: response as! Data)
                self.delegate?.fileredArray(details: jobDetails.jobs)
                self.remove()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
        
    }
    func getDates() {
        
    }
    
}

extension FilterVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = departmentsArray[indexPath.row]
        cell.selectionStyle = .none
        cell.textLabel?.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dropDownView.isHidden = true
        mainView.backgroundColor = UIColor.white
        switch indexPath.row {
        case 0:
            selectedDepartment = ""
        case 1:
            selectedDepartment = "1"
        case 2:
            selectedDepartment = "3"
        default: break
        }
        selectDepartment.setTitle(departmentsArray[indexPath.row], for: .normal)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
}
extension FilterVC : FromDateDelegate, ToDateDelegate {
    func selectedToDate(value: String?) {
        toDateButton.setTitle(value, for: .normal)
        toDate = value ?? ""
    }
    
    func selectedFromDate(value: String) {
        fromDateButton.setTitle(value, for: .normal)
        fromDate = value
    }
}
extension FilterVC: UserNameDelegate {
    func selecteUserDetails(id: Int, userName: String?) {
        selectedUserId = "\(id)"
        userButton.setTitle(userName ?? "By User" , for: .normal)
    }
}





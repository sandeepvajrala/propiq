//
//  AddUsersViewController.swift
//  AddJobs
//
//  Created by Admin on 31/04/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit
enum Gender {
    case Male
    case Female
}

class AddUsersViewController: UIViewController {
    
    
    @IBOutlet weak var countriesBackgroundView: UIView!
    @IBOutlet weak var countriesTableView: UITableView!
    @IBOutlet weak var userDetailsTableView: UITableView!
    
    var countriesList = [Country]()
    let dimmingView = UIView()
    var gender: Gender?
    var placeholders = ["First Name","Last name"]
    var country: String?
    var selectedGender: String?
    var departmentId: Int?
    var workersDetails: Workers?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dimmingView.frame = self.view.bounds
        self.view.addSubview(self.dimmingView)
        self.dimmingView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dimmingView.isHidden = true
        countriesBackgroundView.isHidden = true
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        let touch: UITouch? = touches.first
        if touch?.view != countriesBackgroundView {
            countriesBackgroundView.isHidden = true
            dimmingView.isHidden = true
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func countriesBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        apiForCountriesApi()
        dimmingView.isHidden = false
        dimmingView.addSubview(countriesBackgroundView)
        countriesBackgroundView.isHidden = false
        
    }
    
    
    @IBAction func backButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
//        userDetailsTableView.reloadRows(at: [[0,3]], with: UITableViewRowAnimation.fade)
    }

    @IBAction func submitButtonTapped(_ sender: Any) {
       apiCallForAddUser()
    }
    @IBAction func maleButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        gender = Gender.Male
        selectedGender = "M"
        userDetailsTableView.reloadRows(at: [[0,3]], with: UITableViewRowAnimation.fade)
        
    }
    @IBAction func femaleBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        gender = Gender.Female
        selectedGender = "F"
        userDetailsTableView.reloadRows(at: [[0,3]], with: UITableViewRowAnimation.fade)
    }
    
    func apiForCountriesApi() {
        _ = ConnectivityHelper.executeGETRequest(URLString: ApiFile.countries, parameters: nil, headers: nil, success: { (response) in
            do {
                let countries = try JSONDecoder().decode(Countries.self, from: response as! Data)
                self.countriesList = countries.countries
                self.countriesTableView.reloadData()
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
    
    func apiCallForAddUser() {
        let values = getTextFeildValuesFromTableView()
        var params = [String: AnyObject]()
        params["first_name"] = values!["First Name"] as AnyObject
        params["last_name"] = values!["Last name"] as AnyObject
        params["country"] = country as AnyObject
        params["code"] = values!["code"] as AnyObject
        params["mobile_no"] = values!["mobileNumber"] as AnyObject
        params["work"] = departmentId as AnyObject
        params["email"] =  values!["email"] as AnyObject
        params["gender"] = selectedGender as AnyObject
        params["added_by"] = UserDetails.sharedInstance.User_ID as AnyObject
        
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.addUser, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            
            do {
                let workers = try JSONDecoder().decode(Workers.self, from: response as! Data)
                let message = workers.message
                if message == "Saved Successfully" {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.showAlert(title:"Something went wrong" , message: "Details are not saved")
                }

            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        },  failure: { (error) in
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
    }
    
    
    
    func getTextFeildValuesFromTableView() ->[String : String ]? {
        var values = [String : String]()
        
        for (index, value) in placeholders.enumerated() {
            let indexPath = IndexPath(row: index, section: 0)
            guard let cell = userDetailsTableView.cellForRow(at: indexPath) as? AddUSersTableCell else {
                self.showAlert(title: "check", message: "please enter all fields")
                return nil
            }
            if let text = cell.nameTextField.text, !text.isEmpty {
                values[value] = text
            }
        }
        let indexPath = IndexPath(row: 4, section: 0)
        guard let cell = userDetailsTableView.cellForRow(at: indexPath) as? AddUSersTableCell else {
            return nil }
        if let text = cell.nameTextField.text, !text.isEmpty {
            values["email"] = text
        } else { self.showAlert(title: "check", message: "please enter all fields") }
        let mobileNoIndex = IndexPath(row: 2, section: 0)
        guard let mobileNocell =  userDetailsTableView.cellForRow(at: mobileNoIndex) as? MobileNumberCell else {
            self.showAlert(title: "check", message: "please enter all fields")
            return nil }
        if let text = mobileNocell.mobileNoTxtFld.text, !text.isEmpty {
            values["mobileNumber"] = text
        } else { self.showAlert(title: "check", message: "please enter all fields") }
        if let dialCode = mobileNocell.countriesButton.currentTitle {
            values["code"] = dialCode
        } else { self.showAlert(title: "check", message: "please enter all fields") }
        
        
        return values
    }
    
   
}

//MARK: - Tableview Methods
extension AddUsersViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == countriesTableView {
            return countriesList.count > 0 ? countriesList.count : 0
        }else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == countriesTableView {
            
            tableView.contentInset = UIEdgeInsetsMake(0, -10, 0, -20)
            let cell = UITableViewCell()
            
            if countriesList.count > 0 {
                cell.textLabel?.text = countriesList[indexPath.row].code + " " + countriesList[indexPath.row].dialCode
            } else {
                cell.textLabel?.text = "In"
            }
            
            cell.textLabel?.font = UIFont(name: "Helvetica Neue", size: 14.0)
            cell.selectionStyle = .none
            cell.textLabel?.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
            return cell
        
        } else {
        
            switch indexPath.row {
            case 0,1,4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddUSersTableCell") as? AddUSersTableCell else {return UITableViewCell()}
                if indexPath.row == 0 {
                    cell.nameTextField.placeholder = placeholders[0]
                    cell.nameTextField.tag = 0
                } else if indexPath.row == 1 {
                    cell.nameTextField.placeholder = placeholders[1]
                    cell.nameTextField.tag = 1
                } else {
                    cell.nameTextField.placeholder = "Email"
                    cell.nameTextField.tag = 4
                }
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MobileNumberCell") as? MobileNumberCell else {return UITableViewCell()}
                cell.mobileNoTxtFld.placeholder = "Mobile Number"
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GenderTabelCell") as? GenderTabelCell else {return UITableViewCell()}
                if let gender = gender {
                    switch gender {
                    case .Male :
                        cell.maleSelectionBtn.setImage(#imageLiteral(resourceName: "selection"), for: .normal)
                        cell.femaleSelectionBtn.setImage(#imageLiteral(resourceName: "unSelection"), for: .normal)
                    case.Female:
                        cell.femaleSelectionBtn.setImage(#imageLiteral(resourceName: "selection"), for: .normal)
                        cell.maleSelectionBtn.setImage(#imageLiteral(resourceName: "unSelection"), for: .normal)
                    }
                }
                
                return cell
            default:
                return UITableViewCell()
            }
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        if tableView == countriesTableView {
            let cell = userDetailsTableView.cellForRow(at: [0,2]) as! MobileNumberCell
            cell.countriesButton.setTitle(countriesList[indexPath.row].dialCode, for: UIControlState.normal)
            cell.countriesButton?.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 16.0)!
            country = countriesList[indexPath.row].code
            dimmingView.isHidden = true
            countriesBackgroundView.isHidden = true
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == countriesTableView {
            return 30
        } else {
            switch indexPath.row {
            case 3:
                return 80
            default:
                return 55
            }
        }
    }
    
}







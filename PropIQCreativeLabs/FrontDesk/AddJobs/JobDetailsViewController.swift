//
//  JobDetailsViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 24/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
protocol UpdateJobDetailsDelegate: class {
    func updateJobDetails()
}

class JobDetailsViewController: UIViewController {
    
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var addJobBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addMoreJobView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    //Variables
    var roomsList = [Room]()
    var jobsDetailsList = [JobDetailsListElement]()
    var selectedIndexPaths = [Int]()
    var from = ""

    var jobLocation = false
    var departmentId: Int?
    var dimmingView  = UIView()
    weak var delegate: UpdateJobDetailsDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupDimView()
        textView.delegate = self
        
        if let id = departmentId {
            if id == 3 {
                jobLocation ? apiCallForRoomsList() : apiCallForJobDetailsList(id: id)
            } else {
                apiCallForJobDetailsList(id: id)
            }
        } else {
            apiCallForRoomsList()
        }
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 65
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dimmingView.isHidden = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        self.view.endEditing(true)
        if touch?.view != addMoreJobView {
            dimmingView.isHidden = true
        }
    }
    
    func setupUI(){
        if departmentId == 3 {
            if jobLocation {
                addJobBtn.isHidden = true
                submitButton.isHidden = false
            } else {
                addJobBtn.isHidden = false
                submitButton.isHidden = true
            }
            
        } else {
            if let _ = departmentId {
                addJobBtn.isHidden = false
                submitButton.isHidden = false
            } else {
                addJobBtn.isHidden = true
                submitButton.isHidden = true
            }
        }
        
    }
    
    func setupDimView() {
        dimmingView.frame = self.view.bounds
        self.view.addSubview(self.dimmingView)
        self.dimmingView.backgroundColor = UIColor.darkGray.withAlphaComponent(0.1)
    }
    
    func apiCallForRoomsList() {
        _ = addActivityIndicator()
        var params = [String : AnyObject]()
        params["room_type"] = "" as AnyObject
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.roomsList, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            print(response)
            do {
                let roomsList = try JSONDecoder().decode(Rooms.self, from: response as! Data)
                self.roomsList = roomsList.rooms
                self.selectedIndexPaths = [0].repeated(count: self.roomsList.count)
                self.tableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    
    func apiCallForJobDetailsList(id: Int) {
        _ = addActivityIndicator()
        let params = ["department_id":"\(id)"]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.jobDetailsList, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            print(response)
            do {
                let jobDetails = try JSONDecoder().decode(JobDetailsList.self, from: response as! Data)
                self.jobsDetailsList = jobDetails.jobDetailsList
                self.selectedIndexPaths = [0].repeated(count: self.jobsDetailsList.count)
                self.tableView.reloadData()
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    func apiCallForAddJobDetailList(id: Int) {
        _ = addActivityIndicator()
        if textView.text != nil {
            let params = ["department_id":"\(id)","detail":"\(textView.text!)"]
            _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.addjobDetailList, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
                removeIndicator()
                print(response)
                do {
                    let jobDetails = try JSONDecoder().decode(JobDetailsList.self, from: response as! Data)
                    if jobDetails.status {
                        if let id = self.departmentId {
                            self.apiCallForJobDetailsList(id: id)
                        }
                        self.cancelJob()
                    } else {
                        self.showAlert(title: "Alert", message: "Something went wrong")
                    }
                } catch let error {
                    self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                    print(error.localizedDescription)
                }
                
            }, failure: { (error) in
                removeIndicator()
                self.showAlert(title: "Server Error", message: "Failure")
            }, notRechable: {
                removeIndicator()
                self.showAlert(title: "Server Error", message: "Server Not Reachable")
            }, sessionExpired: {
                removeIndicator()
                self.showAlert(title: "Server Error", message: "Session Expired")
                
            })
        }
        
    }
    func cancelJob() {
        dimmingView.isHidden = true
    }
    
    //MARK: - Actions
    @IBAction func backBtnPressed(_ sender: Any) {
        self.remove()
    }
    @IBAction func addJobBtnPressed(_ sender: Any) {
        dimmingView.isHidden = false
        dimmingView.addSubview(addMoreJobView)
    }
    
    @IBAction func AcceptBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        if let id = departmentId {
            apiCallForAddJobDetailList(id: id)
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        self.view.endEditing(true)
        cancelJob()
        
    }
    
    @IBAction func submitBtnPressed(_ sender: Any) {
        var arrayOfJobs = [String]()
        for i in 0..<selectedIndexPaths.count {
            if selectedIndexPaths[i] == 1 {
                if departmentId == 3 {
                    arrayOfJobs.append(roomsList[i].roomNo)
                } else {
                    arrayOfJobs.append(jobsDetailsList[i].work)
                }
            }
        }
        let arr = arrayOfJobs.joined(separator: ",")
        UserDefaults.standard.set("[\(arr)]", forKey: from)
        delegate?.updateJobDetails()
        self.remove()
    }
}

extension JobDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let _ = departmentId {
            if jobLocation {
                return roomsList.count > 0 ? roomsList.count : 0
            } else {
                return jobsDetailsList.count > 0 ? jobsDetailsList.count : 0
            }
        } else {
            return roomsList.count > 0 ? roomsList.count : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "jobDetailsCell") as? jobDetailsCell else { return UITableViewCell() }
        
        if roomsList.count > 0 {
            cell.titleLabel.text = roomsList[indexPath.row].roomNo
        }
        if jobsDetailsList.count > 0 {
            cell.titleLabel.text = jobsDetailsList[indexPath.row].work
        }
        if selectedIndexPaths.count > 0 {
            if selectedIndexPaths[indexPath.row] == 0 {
                cell.selectionImage.image = #imageLiteral(resourceName: "unSelection")
            } else {
                cell.selectionImage.image = #imageLiteral(resourceName: "selection")
            }
        }
        cell.contentView.setNeedsLayout()
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if departmentId == 3 {
            if jobLocation {
                if selectedIndexPaths[indexPath.row] == 0 {
                    selectedIndexPaths[indexPath.row] = 1
                } else {
                    selectedIndexPaths[indexPath.row] = 0
                }
                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            } else {
                UserDefaults.standard.set(jobsDetailsList[indexPath.row].work, forKey: from) //Bool
                
                let cell = tableView.cellForRow(at: indexPath) as! jobDetailsCell
                cell.selectionImage.image = #imageLiteral(resourceName: "selection")
                delegate?.updateJobDetails()
                self.remove()
            }
            
        } else {
            if let _ =  departmentId {
                if selectedIndexPaths[indexPath.row] == 0 {
                    selectedIndexPaths[indexPath.row] = 1
                } else {
                    selectedIndexPaths[indexPath.row] = 0
                }
                tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            } else {
                UserDefaults.standard.set(roomsList[indexPath.row].roomNo, forKey: from) //Bool
                
                let cell = tableView.cellForRow(at: indexPath) as! jobDetailsCell
                cell.selectionImage.image = #imageLiteral(resourceName: "selection")
                delegate?.updateJobDetails()
                self.remove()
            }
        }
        
        
    }
    
}
extension JobDetailsViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if (textView.text == "Other Jobs")
        {
            textView.text = ""
            textView.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
        }
        textView.becomeFirstResponder() //Optional
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        if (textView.text == "")
        {
            textView.text = "Other Jobs"
            textView.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
        }
        textView.resignFirstResponder()
    }
}


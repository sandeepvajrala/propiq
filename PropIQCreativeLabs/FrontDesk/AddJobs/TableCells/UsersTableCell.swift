//
//  UsersTableCell.swift
//  AddJobs
//
//  Created by Admin on 31/04/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit

class UsersTableCell: UITableViewCell {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var workTitle: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var selectionImage: UIImageView!
    @IBOutlet weak var backgroundForCircle: UIView!
    @IBOutlet weak var backgroundViewForCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundViewForCell.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
        backgroundForCircle.circleWithBorderColor(enable: true, color: .white, width: 2.0)
        userImage.makeCircle()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

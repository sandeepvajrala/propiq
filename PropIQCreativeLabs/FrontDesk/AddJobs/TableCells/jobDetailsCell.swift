//
//  jobDetailsCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 24/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class jobDetailsCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    @IBOutlet weak var backgroundCellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundCellView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

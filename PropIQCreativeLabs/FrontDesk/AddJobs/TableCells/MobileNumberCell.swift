//
//  MobileNumberCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class MobileNumberCell: UITableViewCell {

    @IBOutlet weak var countriesButton: UIButton!
    @IBOutlet weak var mobileNoTxtFld: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        mobileNoTxtFld.delegate  = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension MobileNumberCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

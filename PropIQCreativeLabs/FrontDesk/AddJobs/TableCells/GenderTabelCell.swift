//
//  GenderTabelCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class GenderTabelCell: UITableViewCell {

    @IBOutlet weak var maleSelectionBtn: UIButton!
    @IBOutlet weak var femaleSelectionBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

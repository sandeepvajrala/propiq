//
//  SelectPriorityVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 25/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class SelectPriorityVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var popupBackgroundView: UITableView!
    
    var prioritiesArray = ["Select Priority", "Priority 1","Priority 2","Priority 3","Urgent Basis"]
    var from = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popupBackgroundView.layer.cornerRadius = 10.0
        popupBackgroundView.layer.masksToBounds = true
        popupBackgroundView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func coverButtonPressed(_ sender: Any) {
        self.remove()
    }
    
    
}
extension SelectPriorityVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prioritiesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = prioritiesArray[indexPath.row]
        cell.selectionStyle = .none
        cell.textLabel?.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.remove()
        } else {
            UserDefaults.standard.set(prioritiesArray[indexPath.row], forKey: from)
            UserDefaults.standard.set(indexPath.row, forKey: "priority")
            self.remove()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "peru") ,object: nil, userInfo: nil)
        }

    }
}

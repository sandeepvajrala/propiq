//
//  DateAndTimePopView.swift
//  AddJobs
//
//  Created by Admin on 30/04/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit


protocol FromDateDelegate: class {
    func selectedFromDate(value: String)
}
protocol ToDateDelegate: class {
    func selectedToDate(value: String?)
}
protocol DateCheckInCheckOut: class {
    func selectedDateAndTime(date: String?, time: String?, fromCheckIn: Bool?)
}

class DateAndTimePopView: UIViewController {
    
    @IBOutlet weak var selectDateLable: UILabel!
    @IBOutlet weak var saveDataButton: UIButton!
    @IBOutlet weak var showDatePicker: UIDatePicker!
    @IBOutlet weak var pickerBackgroundView: UIView!
    
    var from : NSString = ""
    var showTime = false
    weak var fromDateDelegate: FromDateDelegate?
    weak var toDateDelegate: ToDateDelegate?
    weak var checkInDelegate: DateCheckInCheckOut?
    var fromCheckInOut = false
    var fromCheckIn = false
    var selectedDate = ""
    
    var formattedDate: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: showDatePicker.date)
    }
    
    var formattedTime: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: showDatePicker.date)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        updateUI()
    }
    
    func updateUI() {
        if showTime{
            self.showDatePicker.datePickerMode = .time
        } else {
            self.showDatePicker.datePickerMode = .date
            self.showDatePicker.minimumDate = Date()
        }
    }
    func setupUI() {
        pickerBackgroundView.layer.cornerRadius = 10.0
        pickerBackgroundView.layer.masksToBounds = true
        pickerBackgroundView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
        saveDataButton.layer.cornerRadius = 17.0
        saveDataButton.layer.masksToBounds = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func saveDateButtonPressed(_ sender: UIButton) {
        
        if fromCheckInOut {

            var selectedTime = ""
            if showTime {
                selectedTime = formattedTime
                checkInDelegate?.selectedDateAndTime(date: selectedDate, time: selectedTime, fromCheckIn: fromCheckIn)
                self.remove()
            } else {
                selectedDate = formattedDate
                showTime = true
                self.updateUI()
            }
        } else {
            if showTime {
                UserDefaults.standard.set(formattedTime, forKey: from as String) //Bool
                UserDefaults.standard.synchronize()
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "peru") ,object: nil, userInfo: nil)
            } else {
                if from == "FromDate" {
                    fromDateDelegate?.selectedFromDate(value: formattedDate)
                }else if from == "ToDate" {
                    toDateDelegate?.selectedToDate(value: formattedDate)
                } else {
                    UserDefaults.standard.set(formattedDate, forKey: from as String) //Bool
                    UserDefaults.standard.synchronize()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "peru") ,object: nil, userInfo: nil)
                }
            }
            self.remove()
        }
        
    }
    @IBAction func coverButtonPressed(_ sender: Any) {
        self.remove()
    }
}







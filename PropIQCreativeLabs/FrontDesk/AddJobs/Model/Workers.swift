//
//  Workers.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Workers: Codable {
    let message: String
    let workers: [Worker]
    let status: Bool
    let responseCode: Int
}

struct Worker: Codable {
    let firstName, lastName, image, work: String
    let id: Int
}

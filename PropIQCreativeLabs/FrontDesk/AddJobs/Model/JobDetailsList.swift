//
//  JobDetailsList.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 24/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct JobDetailsList: Codable {
    let jobDetailsList: [JobDetailsListElement]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct JobDetailsListElement: Codable {
    let departmentID: Int
    let work: String
    let detailID: Int
    let department: String
    
    enum CodingKeys: String, CodingKey {
        case departmentID = "department_id"
        case work
        case detailID = "detail_id"
        case department
    }
}

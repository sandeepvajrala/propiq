//
//  RoomsList.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 24/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Rooms: Codable {
    let rooms: [Room]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct Room: Codable {
    let roomID: Int
    let roomNo, roomType: String
    
    enum CodingKeys: String, CodingKey {
        case roomID = "room_id"
        case roomNo = "room_no"
        case roomType = "room_type"
    }
}

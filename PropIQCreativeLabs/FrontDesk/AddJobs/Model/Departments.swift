//
//  Departments.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 25/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

enum Departments {
    case SelectDepartment
    case Maintainence
    case HouseKeeping
    
    static let all:[Departments] = [.SelectDepartment, .Maintainence, .HouseKeeping]
    
    var title: String {
        switch self {
        case .SelectDepartment:
            return "Select Department"
        case .Maintainence:
            return "Maintainence"
        case .HouseKeeping:
            return "House Keeping"
        }
    }
    
    var rows: [(title: String, image: UIImage)]{
        switch self {
        case .SelectDepartment:
            return [(title:"Select Department", image: #imageLiteral(resourceName: "downArrow")),(title:"Date", image: #imageLiteral(resourceName: "calender")),(title:"Time", image: #imageLiteral(resourceName: "time")),(title:"Users", image: #imageLiteral(resourceName: "user")),(title:"Jobs List", image: #imageLiteral(resourceName: "do-not-disturb"))]
        case .Maintainence:
            return [(title:"Maintainence", image: #imageLiteral(resourceName: "downArrow")),(title:"Date", image: #imageLiteral(resourceName: "calender")),(title:"Time", image: #imageLiteral(resourceName: "time")),(title:"Users", image: #imageLiteral(resourceName: "user")),(title:"JobDetail", image: #imageLiteral(resourceName: "do-not-disturb")),(title:"Location", image: #imageLiteral(resourceName: "downArrow")),(title:"Select Priority", image: #imageLiteral(resourceName: "downArrow"))]
        case .HouseKeeping:
            return [(title:"House Keeping", image: #imageLiteral(resourceName: "downArrow")),(title:"Date", image: #imageLiteral(resourceName: "calender")),(title:"Time", image: #imageLiteral(resourceName: "time")),(title:"Users", image: #imageLiteral(resourceName: "user")),(title:"Rooms", image: #imageLiteral(resourceName: "do-not-disturb")),(title:"Jobs List", image: #imageLiteral(resourceName: "downArrow"))]
            
        }
    }
    
}


struct AddJobsResponse: Codable {
    let message: String
    let status: Bool
    let responseCode: Int
}

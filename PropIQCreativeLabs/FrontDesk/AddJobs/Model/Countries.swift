//
//  Countries.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Countries: Codable {
    let countries: [Country]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct Country: Codable {
    let code, dialCode, countryName: String
    
    enum CodingKeys: String, CodingKey {
        case code
        case dialCode = "dial_code"
        case countryName = "country_name"
    }
}

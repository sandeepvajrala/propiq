//
//  AddViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 20/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

protocol UploadedJobDelegate: class {
    func updaloadJob()
}

class AddViewController: UIViewController {
    //Outlets
    @IBOutlet weak var detailsTableView: UITableView!
    @IBOutlet weak var heightConstraintOfView: NSLayoutConstraint!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropdownTableView: UITableView!
    @IBOutlet weak var heightConstraintForTableView: NSLayoutConstraint!
    @IBOutlet weak var submitButton: UIButton!
    
    var jobsDetails: Job?
    var flag: Departments?
    var dimmingView  = UIView()
    var selectedIndexPath: IndexPath?
    var selectedValue: String?
    var selectedIndex: Int?
    var date:NSString = ""
    var time:NSString = ""
    weak var delegate: UploadedJobDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDimView()
        selectedIndex = 0
//        for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
//            print("\(key) = \(value) \n")
//        }
        removeUserDefaults()
        if let jobDetails = jobsDetails {
            submitButton.setTitle("Update", for: .normal)
            getList(details: jobDetails)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(setToPeru(notification:)), name:NSNotification.Name(rawValue: "peru"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tabBarController?.tabBar.isHidden = true
        super.viewWillAppear(animated)
        dimmingView.isHidden = true
        dimmingView.addSubview(dropDownView)
        heightConstraintOfView.constant = 0
        detailsTableView.reloadData()
        
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        if touch?.view != dropDownView {
            dimmingView.isHidden = true
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        UIView.animate(withDuration: 0.1) {
            self.dropDownView.layoutIfNeeded()
        }
        
    }
    
    func setupDimView(){
        dimmingView.frame = self.view.bounds
        self.view.addSubview(self.dimmingView)
        self.dimmingView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
    }
    func removeUserDefaults() {
        for i in 0...3 {
            if UserDefaults.standard.value(forKey: "Date\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Date\(i)")
            }
            if UserDefaults.standard.value(forKey: "Time\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Time\(i)")
            }
            if UserDefaults.standard.value(forKey: "Users\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Users\(i)")
            }
            if UserDefaults.standard.value(forKey: "Rooms\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Rooms\(i)")
            }
            if UserDefaults.standard.value(forKey: "Jobs List\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Jobs List\(i)")
            }
            if UserDefaults.standard.value(forKey: "Location\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Location\(i)")
            }
            if UserDefaults.standard.value(forKey: "JobDetail\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "JobDetail\(i)")
            }
            if UserDefaults.standard.value(forKey: "Select Priority\(i)") != nil {
                UserDefaults.standard.removeObject(forKey: "Select Priority\(i)")
            }
        }
        if UserDefaults.standard.value(forKey: "workerId") != nil {
            UserDefaults.standard.removeObject(forKey: "workerId")
        }
        if UserDefaults.standard.value(forKey: "priority") != nil {
            UserDefaults.standard.removeObject(forKey: "priority")
        }
    }
    
    //MARK: - Actions
    
    @IBAction func backButtonPressed(_ sender: Any) {
        tabBarController?.tabBar.isHidden = false
        self.remove()
    }
    @IBAction func submitAction(_ sender: Any) {
        var params = [String: Any]()
        if let jobDetails = jobsDetails {
            guard let str = UserDefaults.standard.value(forKey: "jobid") else { return }
            params["job_id"] = str
            params["worker_id"] = "\(jobDetails.workerDetails.workerID)"
        }
        if flag?.title == "House Keeping" {
            if let str = UserDefaults.standard.value(forKey: "Date2") {
                params["date"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "Time2") {
                params["time"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "Jobs List2") {
                params["job_list"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "Rooms2") {
                params["room_no"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "workerId") {
                params["worker_id"] = "\(str)"
            }
            params["job_priority"] = "0"
            params["department_id"] = "1"
            params["assigned_by"] = "\(UserDetails.sharedInstance.User_ID)"
            
            print(params)
            apiForAddJob(params: params)
        } else {
            if let str = UserDefaults.standard.value(forKey: "Date1") {
                params["date"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "Time1") {
                params["time"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "Location1") {
                params["job_list"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "JobDetail1") {
                params["room_no"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "workerId") {
                params["worker_id"] = "\(str)"
            }
            if let str = UserDefaults.standard.value(forKey: "priority") {
                params["job_priority"] = "\(str)"
            }
            params["department_id"] = "3"
            params["assigned_by"] = "\(UserDetails.sharedInstance.User_ID)"
            
            print(params)
            apiForAddJob(params: params)
        }
        
    }
    
    func apiForAddJob(params: [String: Any]) {
        var urlString = String()
        if let _ = jobsDetails{
            urlString = ApiFile.updateJobs
        } else {
            urlString = ApiFile.addJobs
        }
        _ = ConnectivityHelper.callPOSTRequest(URLString: urlString, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            
            do {
                let details = try JSONDecoder().decode(AddJobsResponse.self, from: response as! Data)
                let message = details.message
                if message == "Saved Successfully" {
                    self.tabBarController?.tabBar.isHidden = false
                    self.delegate?.updaloadJob()
                    self.remove()
                    
                } else {
                    self.showAlert(title:"Something went wrong" , message: "Details are not saved")
                }
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        },  failure: { (error) in
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
}


//MARK: - Tableview Methods

extension AddViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == dropdownTableView {
            return 3
        } else {
            if let flag = flag {
                switch flag {
                case .SelectDepartment:
                    return 5
                case .Maintainence:
                    return 7
                case .HouseKeeping:
                    return 6
                }
            }
            return 5
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == dropdownTableView{
            let currentDepartments = Departments.all
            let cell = UITableViewCell()
            cell.textLabel?.text = currentDepartments[indexPath.row].title
            cell.textLabel?.textColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
            cell.selectionStyle = .none
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddDetailsTableCell") as? AddDetailsTableCell else { return UITableViewCell()}
            
            let curentDepartmentSection = Departments.all[selectedIndex!]
            let currentRowOption = curentDepartmentSection.rows[indexPath.row]
            let str = String(format: "%@%d", currentRowOption.title,selectedIndex!)
            print(str,UserDefaults.standard.value(forKey: str) as? String ?? "")
            
            if let _ = jobsDetails {
                if indexPath.row == 0{
                    cell.isUserInteractionEnabled = false
                } else if indexPath.row == 4 {
                    cell.isUserInteractionEnabled = false
                }
            }
            if let str = UserDefaults.standard.value(forKey: str) {
                cell.titleLabel?.text = str as? String
                cell.titleImage.image = currentRowOption.image
            } else {
                cell.titleLabel.text = currentRowOption.title
                cell.titleImage.image = currentRowOption.image
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == dropdownTableView {
            flag = Departments.all[indexPath.row]
            selectedIndex = Int(indexPath.row)
//            if let rowCount = flag?.rows.count {
//                heightConstraintForTableView.constant = CGFloat(60 * rowCount)
//            }
            dimmingView.isHidden = true
          heightConstraintOfView.constant = 0
            detailsTableView.reloadData()
        } else {
            let curentDepartmentSection = Departments.all[selectedIndex!]
            let currentRowOption = curentDepartmentSection.rows[indexPath.row]
            switch indexPath.row {
            case 0:
                dimmingView.isHidden = false
                
                heightConstraintOfView.constant = 120
            case 1:
                let sb = UIStoryboard.init(name: "Jobs", bundle: nil)
                let popup = sb.instantiateViewController(withIdentifier: "DateAndTimePopView") as! DateAndTimePopView
                popup.showTime = false
                popup.from = String(format: "%@%d", currentRowOption.title,selectedIndex!) as NSString
                self.add(popup)
            case 2:
                let sb = UIStoryboard.init(name: "Jobs", bundle: nil)
                let popup = sb.instantiateViewController(withIdentifier: "DateAndTimePopView") as! DateAndTimePopView
                popup.showTime = true
                popup.from = String(format: "%@%d", currentRowOption.title,selectedIndex!) as NSString
                self.add(popup)
            case 3:
                let sb = UIStoryboard(name: "Jobs", bundle: nil)
                let usersVC = sb.instantiateViewController(withIdentifier: "UsersViewController") as! UsersViewController
                usersVC.from = String(format: "%@%d", currentRowOption.title,selectedIndex!) as String
                usersVC.delegate = self
                if selectedIndex == 1 {
                    usersVC.departMentId = 3
                    self.add(usersVC)
                } else if selectedIndex == 2 {
                    usersVC.departMentId = 1
                    self.add(usersVC)
                } else {
                    self.showAlert(title: "Alert", message: "Please Select Department")
                }
                
            default:
                if let flag = flag {
                    switch flag {
                        
                    case .SelectDepartment:
                        self.showAlert(title: "Check", message: "Department is mandatory")
                        
                    case .Maintainence:
                        if indexPath.row == 6 {
                            let popup = UIStoryboard.init(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "SelectPriorityVC") as! SelectPriorityVC
                            popup.from = String(format: "%@%d", currentRowOption.title,selectedIndex!)
                            self.add(popup)
                        } else {
                            let jobDetailsVc = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
                            jobDetailsVc.delegate = self
                            jobDetailsVc.departmentId = 3
                            jobDetailsVc.from = String(format: "%@%d", currentRowOption.title,selectedIndex!) as String
                            if indexPath.row == 5 {
                                jobDetailsVc.jobLocation = true
                            }
                            self.add(jobDetailsVc)
                        }
                        
                    case .HouseKeeping:
                        let jobDetailsVc = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "JobDetailsViewController") as! JobDetailsViewController
                        jobDetailsVc.from = String(format: "%@%d", currentRowOption.title,selectedIndex!) as String
                        jobDetailsVc.delegate = self
                        if indexPath.row == 5 {
                            jobDetailsVc.departmentId = 1
                        }
                        self.add(jobDetailsVc)
                    }
                } else {
                    self.showAlert(title: "Check", message: "Department is mandatory")
                }
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let flag = flag {
            switch flag {
                
            case .SelectDepartment:
                return 60
            case .Maintainence:
                if indexPath.row == 5 {
                    return UITableViewAutomaticDimension
                } else { return 60 }
            case .HouseKeeping:
                if indexPath.row == 5 {
                    return UITableViewAutomaticDimension
                } else { return 60 }
            }
        }
        return tableView == dropdownTableView ? 40 : 60
        
    }
    @objc func setToPeru(notification: NSNotification) {
        
        detailsTableView.reloadData()
        
    }
}
extension AddViewController  {
    func getList(details: Job )  {
        let date = details.jobDate
        let time = details.jobTime
        let detail = details.detail
        let jobid = details.jobID
        let priority = details.jobPriority
        
        var roomNo = details.roomNo
        roomNo.remove(at: roomNo.startIndex)
        let departmentId = details.workerDetails.departmentID
        let workerName = details.workerDetails.workerFirstName + " " + details.workerDetails.workerLastName
        let workerId = details.workerDetails.workerID
        var newDetail = [String]()
        
        var formattedTime: String {
            let inFormatter = DateFormatter()
            inFormatter.timeStyle = .short
            
            let outFormatter = DateFormatter()
            outFormatter.dateFormat = "HH:mm:ss"
            
            let date = inFormatter.date(from: time)!
            let outStr = outFormatter.string(from: date)
            return outStr
        }
        for item in detail {
            newDetail.append(item.jobDetail)
        }
        
        let arr = newDetail.joined(separator: ",")
        
        if departmentId == 1 {
            UserDefaults.standard.set("HouseKeeping", forKey: "House Keeping2")
            UserDefaults.standard.set(date, forKey: "Date2")
            UserDefaults.standard.set(formattedTime, forKey: "Time2")
            UserDefaults.standard.set(workerName, forKey: "Users2")
            UserDefaults.standard.set(roomNo, forKey: "Rooms2")
            UserDefaults.standard.set("[\(arr)]", forKey: "Jobs List2")
            flag = Departments.all[2]
            selectedIndex = 2
//            heightConstraintForTableView.constant = CGFloat(60 * 6 + 10)
        } else {
//            UserDefaults.standard.set("HouseKeeping", forKey: "House Keeping2")
            UserDefaults.standard.set(date, forKey: "Date1")
            UserDefaults.standard.set(formattedTime, forKey: "Time1")
            UserDefaults.standard.set(workerName, forKey: "Users1")
            UserDefaults.standard.set("[\(arr)]", forKey: "Location1")
            UserDefaults.standard.set(roomNo, forKey: "JobDetail1")
            UserDefaults.standard.set("Priority \(priority)", forKey: "Select Priority1")
            flag = Departments.all[1]
            selectedIndex = 1
//            heightConstraintForTableView.constant = CGFloat(60 * 7 + 10)
        }
        UserDefaults.standard.set(jobid, forKey: "jobid")
//        UserDefaults.standard.set(details.workerDetails.workerID, forKey: "workerId")
//        UserDefaults.standard.set(workerId, forKey: "workerId")
        
    }
}
extension AddViewController: UpdateTableDelegate {
    func updateDetailsTable() {
        detailsTableView.reloadData()
    }
}
extension AddViewController: UpdateJobDetailsDelegate {
    func updateJobDetails() {
        detailsTableView.reloadData()
    }
}




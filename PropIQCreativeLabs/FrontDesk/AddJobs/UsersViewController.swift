//
//  UsersViewController.swift
//  AddJobs
//
//  Created by Admin on 31/04/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit

protocol UpdateTableDelegate: class {
    func updateDetailsTable()
}
protocol UserNameDelegate: class {
    func selecteUserDetails(id: Int, userName: String?)
}

class UsersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    var from = ""
    var departMentId: Int?
    var workersDetails = [Worker]()
    var selectedIndexPaths = [Int]()
    weak var delegate: UpdateTableDelegate?
    weak var selectedUserDelegate: UserNameDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _ = addActivityIndicator()
        apiCallForAllUsers()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    func apiCallForAllUsers() {
        guard let departmentId = departMentId else { return }
        let params = ["department_id": departmentId]
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.allUsers, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            do {
                let workers = try JSONDecoder().decode(Workers.self, from: response as! Data)
                
                self.workersDetails = workers.workers
                self.tableView.reloadData()
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        let sb = UIStoryboard(name: "Jobs", bundle: nil)
        let addUsersVC = sb.instantiateViewController(withIdentifier: "AddUsersViewController") as! AddUsersViewController
        addUsersVC.departmentId = departMentId
        self.present(addUsersVC, animated: true, completion: nil)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        self.remove()
    }
    
}

//MARK: - TableView Methods

extension UsersViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return workersDetails.count > 0 ? workersDetails.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UsersTableCell") as? UsersTableCell else {return UITableViewCell()}
        if workersDetails.count > 0 {
            cell.userName.text = workersDetails[indexPath.row].firstName + " " + workersDetails[indexPath.row].lastName
            cell.workTitle.text = workersDetails[indexPath.row].work
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let name = workersDetails[indexPath.row].firstName + " " + workersDetails[indexPath.row].lastName
        if from == "GetUser" {
            selectedUserDelegate?.selecteUserDetails(id: workersDetails[indexPath.row].id, userName: workersDetails[indexPath.row].firstName + " " + workersDetails[indexPath.row].lastName)
        } else{
            UserDefaults.standard.set(name, forKey: from as String) //Bool
            UserDefaults.standard.set(workersDetails[indexPath.row].id, forKey: "workerId")
            delegate?.updateDetailsTable()
        }
        let cell = tableView.cellForRow(at: indexPath) as! UsersTableCell
        cell.selectionImage.image = #imageLiteral(resourceName: "selection")
        self.remove()
        
    }
}





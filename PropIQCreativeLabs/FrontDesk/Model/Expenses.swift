//
//  Expenses.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 01/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation


struct Expenses: Codable {
    let message: String
    let status: Bool
    let responseCode: Int
    let expenses: [Expense]
}

struct Expense: Codable {
    let amount: Int
    let updatedAt: String
    let departmentID, monthID: Int
    let imagePath, createdAt, description: String
    let id: Int
    let department: String
    let workerID: Int
    
    enum CodingKeys: String, CodingKey {
        case amount
        case updatedAt = "updated_at"
        case departmentID = "department_id"
        case monthID = "month_id"
        case imagePath = "image_path"
        case createdAt = "created_at"
        case description, id, department
        case workerID = "worker_id"
    }
}


struct ApproveOrReject: Codable {
    let message: String
    let status: Bool
    let responseCode: Int
}

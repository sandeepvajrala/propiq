//
//  CheckInCheckOutVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 30/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class CheckInCheckOutVC: UIViewController {

    //Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //properties
    var checkInOutArray = [CheckInOutElement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        apiCallForCheckInCheckOut()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    //MARK: Actions
    
    @IBAction func addCheckInCheckOutPressed(_ sender: UIButton) {
        
        guard let addCheckVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "AddCheckInCkeckOutVC") as? AddCheckInCkeckOutVC else {return}
        self.present(addCheckVC, animated: false, completion: nil)
    }
    
    func apiCallForCheckInCheckOut() {
        self.view.showLoader()
        let params = ["check_date":"","from_date":"","to_date":"","check_status":"1"]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.userCheckInList, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                
                let checkInOutDetails = try JSONDecoder().decode(CheckInOut.self, from: response as! Data)
                self.checkInOutArray = checkInOutDetails.checkInOut
                self.tableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })

    }
    
}

//MARK: Tableview Methods
extension CheckInCheckOutVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checkInOutArray.count > 0 ? checkInOutArray.count : 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CheckInCheckOutCell") as? CheckInCheckOutCell else { return UITableViewCell() }
        cell.configureCell(checkList: checkInOutArray[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard  let checkoutVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "CheckOutVC") as? CheckOutVC else { return }
        checkoutVC.checkInDetails = checkInOutArray[indexPath.row]
        self.present(checkoutVC, animated: true, completion: nil)
    }
}


//
//  CheckOutVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

struct CheckOutModel: Codable {
    let message: String
    let status: Bool
    let responseCode: Int
}

class CheckOutVC: UIViewController {

    //Outlets
    @IBOutlet weak var totalAmount: UILabel!
    @IBOutlet weak var advanceAmount: UILabel!
    @IBOutlet weak var pendingAmount: UILabel!
    @IBOutlet weak var amountOuterView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    //Properties
    var checkInDetails: CheckInOutElement?
    var checkInRoomsList: [CheckinRoomList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // View Methods
    
    func updateUI() {
        guard let checkInDetails = checkInDetails else { return }
        totalAmount.text = "\(checkInDetails.totalAmount ?? 0)"
        advanceAmount.text = "\(checkInDetails.advance ?? 0)"
        pendingAmount.text = "\(checkInDetails.pendingAmount ?? 0)"
        checkInRoomsList = checkInDetails.checkinRoomList
        amountOuterView.round(enable: true, withRadius: 10.0)
    }
    
    //MARK: Actions
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkOutButtonTapped(_ sender: UIButton) {
        apiCallForCheckOut()
    }
    
    //MARK: Api Call For Check Out
    
    func apiCallForCheckOut() {
        self.view.showLoader()
        var roomIds = [Int]()
        guard let checkInDetails = checkInDetails else { return }
        guard let checkId = checkInDetails.checkID else { return }
        guard let checkInRoomsList = checkInRoomsList else { return }
        for checkInRoom in checkInRoomsList {
            let roomId = checkInRoom.roomID
            roomIds.append(roomId)
        }
        
        let params = ["rooms":"\(roomIds)","checkInOut_id":"\(checkId)"]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.roomCheckOut, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let statusDetails = try JSONDecoder().decode(CheckOutModel.self, from: response as! Data)
                let status = statusDetails.status
                status ? self.dismiss(animated: true, completion: nil) : self.showAlert(title: "Alert", message: "Something Went Wrong")
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
    
}

//MARK: - TableView Methods

extension CheckOutVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let checkInRoomsList = checkInRoomsList else { return 0 }
        return checkInRoomsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CheckOutCell") as? CheckOutCell else { return UITableViewCell() }
        guard let checkInRoosList = checkInRoomsList else { return cell }
        cell.configureCell(checKInDetails: checkInRoosList[indexPath.row])
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}





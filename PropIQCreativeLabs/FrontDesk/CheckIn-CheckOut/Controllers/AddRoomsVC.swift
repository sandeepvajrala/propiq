//
//  AddRoomsVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 05/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import DropDown

struct CheckInRooms {
    var adults: String
    var childs: String
    var roomType: String
    var room: String
    var price: Int
    var roomId: Int
}

class AddRoomsVC: UIViewController {

    @IBOutlet weak var roomsListView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var adultsButton: UIButton!
    @IBOutlet weak var childsButton: UIButton!
    @IBOutlet weak var selectRoomTypeButton: UIButton!
    @IBOutlet weak var selectRoomNoButton: UIButton!
    @IBOutlet weak var userButton: UIButton!
    @IBOutlet weak var confirmButton: UIView!
    
    //Properties
    var checkInDate: String?
    var checkOutDate: String?
    let dropDown = DropDown()
    
    var roomTypesArray = [RoomType]()
    var availableRoomsList = [AvailableRoom]()
    var confirmedRoomsListArray = [CheckInRooms]()

    var selectedAdults: String? {
        didSet {
            guard let selectedAdults = self.selectedAdults else {
                self.adultsButton.setTitle("1", for: .normal); return
            }
            self.adultsButton.setTitle(selectedAdults, for: .normal)
        }
    }
    var selectedChilds: String? {
        didSet{
            guard let selectedChilds = self.selectedChilds else {
                self.childsButton.setTitle("0", for: .normal); return
            }
            self.childsButton.setTitle(selectedChilds, for: .normal)
        }
    }
    var selectedRoomType: String? {
        didSet {
            guard let selectedRoomType = self.selectedRoomType else {
                self.selectRoomTypeButton.setTitle("Please Select Room Type", for: .normal); return
            }
            self.selectRoomTypeButton.setTitle(selectedRoomType, for: .normal)
        }
    }
    
    var selectedRoom: String? {
        didSet{
            guard let selectedRoom = self.selectedRoom else {
                self.selectRoomNoButton.setTitle("Please Select Room No", for: .normal); return
            }
            self.selectRoomNoButton.setTitle(selectedRoom, for: .normal)
        }
    }
    var price: Int?
    var roomId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiCallForRoomType()
        setupUI()
        dropDown.dismissMode = .automatic
        userButton.isHidden = true
        roomsListView.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI() {
        roomsListView.round(enable: true, withRadius: 5)
        confirmButton.round(enable: true, withRadius: 8.0)
    }
    
    //MARK: Actions
    @IBAction func selectAdultsButtonPressed(_ sender: UIButton) {
        setupForDropDown(sender: sender)
    }
    
    @IBAction func selectChildsButtonPressed(_ sender: UIButton) {
        setupForDropDown(sender: sender)
    }
    
    @IBAction func selectRoomTypePressed(_ sender: UIButton) {
        setupForDropDown(sender: sender)
    }
    
    @IBAction func selectRoomNoPressed(_ sender: UIButton) {
        setupForDropDown(sender: sender)
    }
    
    @IBAction func confirmButtonPressed(_ sender: UIButton) {
        
        let check = checkForTypeAndRoom()
        if check {
            self.tableView.reloadData()
            self.selectedAdults = nil
            self.selectedChilds = nil
            self.selectedRoomType = nil
            self.selectedRoom = nil
            userButton.isHidden = false
            sender.setTitle("Add More Rooms", for: .normal)
        }
        roomsListView.isHidden =  confirmedRoomsListArray.count < 1 ? true : false

    }
    
    @IBAction func removeRoomButtonTapped(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: buttonPosition) else { return }
        self.confirmedRoomsListArray.remove(at: indexPath.row)
        tableView.reloadData()
    }
    
    @IBAction func userButtonPressed(_ sender: UIButton) {
        guard let userDetailsVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "AddUserDetailsVC") as? AddUserDetailsVC else { return }
        userDetailsVC.confirmedRooms = confirmedRoomsListArray
        if let checkInDate = checkInDate, let checkOutDate = checkOutDate {
            userDetailsVC.checkInDate = checkInDate
            userDetailsVC.checkOutDate = checkOutDate
        }
        self.present(userDetailsVC, animated: true, completion: nil)
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Check For all buttons are selected or not
    
    func checkForTypeAndRoom() -> Bool {
        
        guard let selectedRoomType = selectedRoomType else { self.showAlert(title: "Alert", message: "Plese select the room type and Room"); return false}
        guard let selectedRoom = selectedRoom else {self.showAlert(title: "Alert", message: "Plese select the Room"); return false}
        let confirmedRoom = CheckInRooms(adults: selectedAdults ?? "1", childs: selectedChilds ?? "0", roomType: selectedRoomType, room: selectedRoom, price: price ?? 0,roomId: roomId ?? 0 )
        confirmedRoomsListArray.append(confirmedRoom)
        return true
    }
    
    //Api Calls
    
    func apiCallForRoomType() {
        self.view.showLoader()
        _ = ConnectivityHelper.executeGETRequest(URLString: ApiFile.roomTypes, parameters: nil, headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let roomTypes = try JSONDecoder().decode(RoomTypes.self, from: response as! Data)
                self.roomTypesArray = roomTypes.roomTypes
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "error", message: "Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Error", message: "session expired")
        })
        
    }

    func apiCallForRoomsList(roomTypeId: String) {
        
        self.view.showLoader()
        let params = ["room_type": roomTypeId]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.availableRoomList, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let roomsLists = try JSONDecoder().decode(AvailableRoomsList.self, from: response as! Data)
                self.availableRoomsList = roomsLists.rooms
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    
    //DropDown setup
    func setupForDropDown(sender: UIButton) {
        
        dropDown.anchorView = sender
        dropDown.cellHeight = 40.0
    
        switch sender.tag {
        case 1:
            dropDown.dataSource = ["1", "2","3","4"]
            dropDown.width = 100
            dropDown.selectionAction = { (index: Int, item: String) in
                self.selectedAdults = item
            }
        case 2:
            dropDown.dataSource = ["0","1", "2","3","4"]
            dropDown.width = 100
            dropDown.selectionAction = { (index: Int, item: String) in
                self.childsButton.setTitle(item, for: .normal)
                self.selectedChilds = item
            }
        case 3:
            var roomTypes = [String]()
            for rooms in roomTypesArray {
                if let roomType = rooms.roomType {
                    roomTypes.append(roomType)
                }
            }
            dropDown.dataSource = roomTypes
            dropDown.width = selectRoomTypeButton.frame.width - 30
            dropDown.selectionAction = { (index: Int, item: String) in
                self.selectRoomTypeButton.setTitle(item, for: .normal)
                self.selectedRoomType = item
                if let roomTypeId = self.roomTypesArray[index].roomTypeID {
                    self.apiCallForRoomsList(roomTypeId: "\(roomTypeId)")
                    self.price = self.roomTypesArray[index].price
                }
            }
        case 4:
            var roomsList = [String]()
            if availableRoomsList.count > 0 {
                for room in availableRoomsList {
                    if let roomNo = room.roomNo {
                        roomsList.append(roomNo)
                    }
                    
                }
                if roomsList.count > 0 {
                    dropDown.dataSource = roomsList
                    dropDown.width = 200
                    dropDown.selectionAction = { (index: Int, item: String) in
                        self.selectRoomNoButton.setTitle(item, for: .normal)
                        self.selectedRoom = item
                        self.roomId = self.availableRoomsList[index].roomID
                    }
                }
            } else {
                self.showAlert(title: "Alert", message: "There is no rooms for selected Type please select another type")
            }
            
        default: return
        }
        dropDown.show()
        
    }

    
}

//MARK: TableView Methods
extension AddRoomsVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return confirmedRoomsListArray.count > 0 ? confirmedRoomsListArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RoomsCell") as? RoomsCell else { return UITableViewCell() }
        cell.configureCell(index: indexPath, list: confirmedRoomsListArray[indexPath.row])
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}







//
//  AddCheckInCkeckOutVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 30/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class AddCheckInCkeckOutVC: UIViewController {

    //Outlets
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var checkOutButton: UIButton!
    @IBOutlet weak var checkInDateLabel: UILabel!
    @IBOutlet weak var checkOutDateLabel: UILabel!
    @IBOutlet weak var roomsButton: UIButton!
    
    //Properties
    var checkInDate = ""
    var checkOutDate = ""
    
    //Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDefaultDateForCheckIn()
        setupDefaultDateForCheckOut()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Methods
    func setupUI() {
        roomsButton.circle(enable: true)
    }
    func setupDefaultDateForCheckIn() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        checkInDate = formatter.string(from: Date())
        checkInDateLabel.text = checkInDate
    }
    
    func setupDefaultDateForCheckOut() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: Date().tomorrow)
        formatter.dateFormat = "HH:mm:ss"
        let time  = formatter.string(from: Date())
        checkOutDate = date + " " + time
        checkOutDateLabel.text = checkOutDate
    }

    
    
    //MARK: Actions
    @IBAction func backButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkInButtonPressed(_ sender: UIButton) {
        moveToDateVC(fromCheckIn: true)
   
    }
    
    @IBAction func checkOutButtonPressed(_ sender: UIButton) {
        moveToDateVC(fromCheckIn: false)
    }
    
    @IBAction func roomsButtonPressed(_ sender: UIButton) {
        moveToCheckInOutVC()
    }

    // Action Methods
    
    func moveToDateVC(fromCheckIn: Bool) {
        guard let dateVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "DateAndTimePopView") as? DateAndTimePopView else { return }
        dateVC.showTime = false
        dateVC.fromCheckInOut = true
        dateVC.fromCheckIn = fromCheckIn
        dateVC.checkInDelegate = self
        self.add(dateVC)
    }
    
    func moveToCheckInOutVC() {
        guard let checkInOutVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "AddRoomsVC") as? AddRoomsVC else { return }
        checkInOutVC.checkInDate = checkInDate
        checkInOutVC.checkOutDate = checkOutDate
        self.present(checkInOutVC, animated: true, completion: nil)
        
    }
}

//MARK: Update Date and Time

extension AddCheckInCkeckOutVC: DateCheckInCheckOut {
    func selectedDateAndTime(date: String?, time: String?, fromCheckIn: Bool?) {
        guard let date = date, let time = time, let fromCheckIn = fromCheckIn  else { return }
        if fromCheckIn {
            checkInDateLabel.text = date + " " + time
        } else {
            checkOutDateLabel.text = date + " " + time
        }
    }
    
    func selectedDateAndTime(date: String?, time: String?) {
    }
}









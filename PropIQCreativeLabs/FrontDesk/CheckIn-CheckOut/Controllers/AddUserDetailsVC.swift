//
//  AddUserDetailsVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 05/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import DropDown
import Alamofire

class AddUserDetailsVC: UIViewController {

    //Outlets
    @IBOutlet weak var fromDate: UILabel!
    @IBOutlet weak var toDate: UILabel!
    @IBOutlet weak var noOfRoomsLabel: UILabel!
    @IBOutlet weak var noOfGuestsLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var userDetailsView: UIView!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var infoTableHeight: NSLayoutConstraint!
    
    //Properties
    var confirmedRooms: [CheckInRooms]?
    var checkInDate: String?
    var checkOutDate: String?
    let placeHolders = ["First Name", "Last Name", "Email Id", "Advance Amount"]
    var imagePicker = UIImagePickerController()
    var image = (UIImage(), "")
    let dropDown = DropDown()
    var countriesList = [Country]()
    
    var firstName = ""
    var lastName = ""
    var mobileNo = ""
    var emailId = ""
    var advanceAmount = ""
    var selecteedCountryIndex: Int?
    var paymentMode: String?
    var totalRevenue = 0
    
    //Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        updateRevenueView()
        updateInfoTableView()
        dropDown.dismissMode = .automatic
        apiCallForCountriesApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Methods
    
    func setupUI() {
        infoButton.circle()
        revenueView.round(enable: true, withRadius: 10.0)
        infoTableView.round(enable: true, withRadius: 5.0)
    }
    func updateInfoTableView() {
        guard let confirmedrooms = confirmedRooms else { return }
        infoTableHeight.constant = CGFloat(confirmedrooms.count * 60)
    }
    
    func updateRevenueView() {
        if let checkInDate = checkInDate {
            fromDate.text = checkInDate
        }
        if let checkOutDate = checkOutDate {
            toDate.text = checkOutDate
        }
        if let rooms = confirmedRooms {
            
            var totalGuests = 0
            
            for room in rooms {
                let noOfAdults = Int(room.adults)
                totalGuests += noOfAdults ?? 0
                totalRevenue += room.price
            }
            noOfRoomsLabel.text = "\(rooms.count)"
            noOfGuestsLabel.text = "\(totalGuests)"
            totalAmountLabel.text = "\(totalRevenue)"
            
        }
    }
    
    //Api call for countries
    func apiCallForCountriesApi() {
        _ = ConnectivityHelper.executeGETRequest(URLString: ApiFile.countries, parameters: nil, headers: nil, success: { (response) in
            do {
                let countries = try JSONDecoder().decode(Countries.self, from: response as! Data)
                self.countriesList = countries.countries
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
    
    //MARK: Actions
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        infoTableView.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.infoTableView.isHidden = true
        }
    }
    @IBAction func checkInButtonPressed(_ sender: UIButton) {
        checkForAllFields()
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func countriesListButtonTapped(_ sender: UIButton) {
        
        dropDown.anchorView = sender
        dropDown.cellHeight = 40
        if countriesList.count > 0 {
            var listOfCountries = [String]()
            for index in countriesList {
                let codeAndDialCode = index.code + " " + index.dialCode
                listOfCountries.append(codeAndDialCode)
            }
            dropDown.dataSource = listOfCountries
        } else { return }
        dropDown.show()
        dropDown.selectionAction = { (index: Int, item: String) in
            sender.setTitle(item, for: .normal)
            self.selecteedCountryIndex = index
        }
    }
    
    // MARK: Helper Methods
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func getActionSheetForImage() {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhotoOrVideo = UIAlertAction(title: "Camera", style: .default) { (action) in
            print("Camera")
            self.pickFromCamera()
        }
        
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            print("Photo Library")
            self.pickImage()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
        }
        
        takePhotoOrVideo.setValue(#imageLiteral(resourceName: "camera"), forKey: "image")
        sharePhoto.setValue(#imageLiteral(resourceName: "picture"), forKey: "image")
        
        optionMenu.addAction(takePhotoOrVideo)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    fileprivate func pickImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    fileprivate func pickFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showAlert(title: "Alert", message: "Something wrong")
        }
    }
    
    func checkForAllFields() {
        if firstName.isEmpty || lastName.isEmpty || mobileNo.isEmpty || emailId.isEmpty || advanceAmount.isEmpty {
            self.showAlert(title: "Alert", message: "All Details are mandatory")
        } else if !isValidEmail(testStr: emailId) {
            self.showAlert(title: "Check Mail Id", message: "It's not a valid email Please change")
        } else {
            uploadCustomerDetails()
        }
    }
    func dropDownSetup() {
        dropDown.cellHeight = 40.0
        dropDown.backgroundColor = UIColor.white
        dropDown.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
        dropDown.dataSource = ["Select Payment Mode","By Cash", "Online"]
        dropDown.width = 200
        dropDown.show()
    }
    
    func uploadCustomerDetails() {
        self.view.showLoader()

        let customerName = firstName + " " + lastName
        let country = countriesList[selecteedCountryIndex ?? 0].code
        let dialCode = countriesList[selecteedCountryIndex ?? 0].dialCode
        guard let checkInDate = self.checkInDate else { return }
        guard let checkOutDate = self.checkOutDate else { return }
        guard let paymentmode = paymentMode else { self.view.hideLoader(); return }
        if paymentmode == "Select Payment Mode" { self.view.hideLoader(); return }
        let checkInDateArr = checkInDate.components(separatedBy: " ")
        let checkOutDateArr = checkOutDate.components(separatedBy: " ")
        var arrayOfDictionariesRooms = [[String: Any]]()
        
        for confirmedRoom in confirmedRooms! {
            let room = ["room_id": "\(confirmedRoom.roomId)", "adults": confirmedRoom.adults, "children": confirmedRoom.childs, "room_no": confirmedRoom.room,"price": "\(confirmedRoom.price)"] as [String : Any]
            arrayOfDictionariesRooms.append(room)
        }
        let json = arrayOfDictionariesRooms.toJSONString()
        
        let params = ["rooms": json, "customer_name": customerName, "country": country, "code": dialCode,"mobile_no": mobileNo,"checkin_date": checkInDateArr[0], "checkin_time": checkInDateArr[1], "checkout_date": checkOutDateArr[0], "checkout_time": checkOutDateArr[1], "total_amount": "\(totalRevenue)", "advance": advanceAmount, "payment_mode": paymentmode  ] as [String : Any]
        
        var imageData = Data()
        
        imageData = UIImageJPEGRepresentation(image.0, 0.2)!
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(imageData, withName: "Id Proof", fileName: "\(Date().timeIntervalSince1970).jpg", mimeType: "image/jpeg")
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: ApiFile.roomCheckIn) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    self.view.hideLoader()
                    self.dismiss(animated: true, completion: nil)
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                self.view.hideLoader()
            }
        }
        
    }
    
}

//MARK: - TableView Methods

extension AddUserDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == infoTableView {
            guard let confirmdrooms = confirmedRooms else { return 0 }
            return confirmdrooms.count
        } else {
            return placeHolders.count + 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == infoTableView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell") as? InfoCell else { return UITableViewCell()}
            guard let confirmedrooms = confirmedRooms else { return cell }
            cell.configureCell(list: confirmedrooms[indexPath.row])
            return cell
        } else {
            switch indexPath.row {
            case 0,1,3,6:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserDetailsCell") as? UserDetailsCell else { return UITableViewCell()}
                cell.textField.delegate = self
                switch indexPath.row {
                case 0: cell.textField.placeholder = placeHolders[0]
                cell.textField.tag = 1
                case 1: cell.textField.placeholder = placeHolders[1]
                cell.textField.tag = 2
                case 3: cell.textField.placeholder = placeHolders[2]
                cell.textField.tag = 4
                case 6: cell.textField.placeholder = placeHolders[3]
                cell.textField.tag = 5
                default: break
                }
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MobileNumberCell") as? MobileNumberCell else { return UITableViewCell()}
                cell.mobileNoTxtFld.placeholder = "Mobile Number"
                cell.mobileNoTxtFld.tag = 3
                cell.mobileNoTxtFld.delegate = self
                return cell
                
            case 4, 5:
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.clear
                cell.selectionStyle = .none
                if indexPath.row == 4 {
                    cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "camera"))
                    cell.textLabel?.text = "image"
                    if image.1 != "" {
                        cell.textLabel?.text = image.1
                    }
                } else {
                    cell.accessoryView = UIImageView(image: #imageLiteral(resourceName: "dropDown"))
                    cell.textLabel?.text = "Select Payment Mode"
                }
                
                let label = UILabel(frame: CGRect(x: 0, y: 58.5 , width: cell.bounds.width + (cell.accessoryView?.frame.width)!, height: 1.3))
                label.backgroundColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
                 cell.addSubview(label)
                cell.textLabel?.textColor = #colorLiteral(red: 0.5694254637, green: 0.09673180431, blue: 0.08610128611, alpha: 1)
                return cell
                
            default: break
            }
            
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == infoTableView {
    
        } else {
            switch indexPath.row {
            case 4:
                getActionSheetForImage()
            case 5:
                let cell = tableView.cellForRow(at: indexPath)
                dropDown.anchorView = cell
                dropDownSetup()
                tableView.beginUpdates()
                dropDown.selectionAction = { (index: Int, item: String) in
                    cell?.textLabel?.text = item
                    self.paymentMode = item
                }
                tableView.endUpdates()
            default:
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == infoTableView ? 60 : 60
       
    }
    
}

//MARK: Image Picker Delegate Methods

extension AddUserDetailsVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true) {
        }
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
          
            image.0 = pickedImage
            image.1 = "\(Date().timeIntervalSince1970).jpg"
            self.tableView.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .fade)
        }
    }
}

// textfields Delegate methods

extension AddUserDetailsVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1: firstName = textField.text ?? ""
        case 2: lastName = textField.text ?? ""
        case 3: mobileNo = textField.text ?? ""
        case 4: emailId = textField.text ?? ""
        case 5: advanceAmount = textField.text ?? ""
        default: break
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1: firstName = ""
        case 2: lastName = ""
        case 3: mobileNo = ""
        case 4: emailId = ""
        case 5: advanceAmount = ""
        default: break
        }
    }
    
}








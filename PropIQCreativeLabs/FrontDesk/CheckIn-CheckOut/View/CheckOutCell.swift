//
//  CheckOutCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class CheckOutCell: UITableViewCell {

    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var guestsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var outerViewInCell: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(checKInDetails: CheckinRoomList) {
        roomsLabel.text = checKInDetails.roomNo
        guestsLabel.text = "\(checKInDetails.adults)"
        priceLabel.text = "\(checKInDetails.price)"
    }
}




//
//  InfoCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import DropDown

class InfoCell: DropDownCell {

    @IBOutlet weak var roomsLabel: UILabel!
    @IBOutlet weak var guestsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var outerViewInsideCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        outerViewInsideCell.round(enable: true, withRadius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(list: CheckInRooms) {
        roomsLabel.text = list.room
        guestsLabel.text = list.adults + " - " + list.childs
        priceLabel.text = "$\(list.price)"
    }
}

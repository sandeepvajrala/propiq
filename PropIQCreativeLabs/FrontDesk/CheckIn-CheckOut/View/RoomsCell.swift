//
//  RoomsCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 05/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class RoomsCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var serialNo: UILabel!
    @IBOutlet weak var RoomNoLabel: UILabel!
    @IBOutlet weak var guestsLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        outerView.round(enable: true, withRadius: 5.0)
        outerView.round(enable: true, withRadius: 5.0)
        outerView.shadow(enable: true, colored: UIColor.gray.cgColor, withRadius: 2.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(index: IndexPath, list: CheckInRooms) {
        RoomNoLabel.text = list.room
        guestsLabel.text = list.adults + " - " + list.childs
        serialNo.text = "\(index.row + 1)"
        priceLabel.text = "$\(list.price)"
    }
}

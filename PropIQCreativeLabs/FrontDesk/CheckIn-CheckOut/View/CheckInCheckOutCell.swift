//
//  CheckInCheckOutCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 30/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class CheckInCheckOutCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var outerViewInCell: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var workerName: UILabel!
    @IBOutlet weak var checkInDate: UILabel!
    @IBOutlet weak var checkInTime: UILabel!
    @IBOutlet weak var checkOutDate: UILabel!
    @IBOutlet weak var checkOutTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //MARK: Methods
    
    func setupUI() {
        profileImage.circleWithBorderColor(enable: true, color: UIColor.white, width: 2.0)
        outerViewInCell.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 3.0)
    }
    
    func configureCell(checkList: CheckInOutElement) {
        workerName.text = checkList.customerName ?? ""
        checkInDate.text = checkList.checkinDate ?? ""
        checkInTime.text = checkList.checkinTime ?? ""
        checkOutDate.text = checkList.checkoutDate ?? ""
        checkOutTime.text = checkList.checkoutTime ?? ""
        guard let urlString = checkList.customerID else {return}
        profileImage.sd_setImage(with: URL(string: urlString), placeholderImage: #imageLiteral(resourceName: "avatarPlaceholder"))
    }
}

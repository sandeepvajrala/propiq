//
//  UserDetailsCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 05/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class UserDetailsCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

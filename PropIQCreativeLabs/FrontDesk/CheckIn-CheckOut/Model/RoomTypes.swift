//
//  RoomTypes.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 31/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct RoomTypes: Codable {
    let roomTypes: [RoomType]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct RoomType: Codable {
    let noOfBeds, price, roomTypeID: Int?
    let roomType: String?
    
    enum CodingKeys: String, CodingKey {
        case noOfBeds = "NoOfBeds"
        case price = "Price"
        case roomTypeID = "roomType_id"
        case roomType
    }
}

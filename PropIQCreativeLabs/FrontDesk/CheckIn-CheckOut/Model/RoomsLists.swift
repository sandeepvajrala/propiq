//
//  RoomsLists.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 31/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct AvailableRoomsList: Codable {
    let rooms: [AvailableRoom]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct AvailableRoom: Codable {
    let roomID, price: Int
    let roomNo, roomType: String?
    let roomStatus: Int?
    
    enum CodingKeys: String, CodingKey {
        case roomID = "room_id"
        case price
        case roomNo = "room_no"
        case roomType = "room_type"
        case roomStatus = "room_status"
    }
}

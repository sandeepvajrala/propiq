//
//  CheckInOut.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 30/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct CheckInOut: Codable {
    let checkInOut: [CheckInOutElement]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct CheckInOutElement: Codable {
    let checkinTime, paymentMode: String?
    let checkID: Int?
    let checkoutDate: String?
    let checkStatus, advance, pendingAmount, totalAmount: Int?
    let checkinDate, checkoutTime, customerName: String?
    let checkinRoomList: [CheckinRoomList]
    let customerID: String?
    
    enum CodingKeys: String, CodingKey {
        case checkinTime = "checkin_time"
        case paymentMode = "payment_mode"
        case checkID = "check_id"
        case checkoutDate = "checkout_date"
        case checkStatus = "check_status"
        case advance
        case pendingAmount = "pending_amount"
        case totalAmount = "total_amount"
        case checkinDate = "checkin_date"
        case checkoutTime = "checkout_time"
        case customerName = "customer_name"
        case checkinRoomList = "checkin_room_list"
        case customerID = "customer_id"
    }
}

struct CheckinRoomList: Codable {
    let roomID, children, price, adults: Int
    let roomNo: String
    
    enum CodingKeys: String, CodingKey {
        case roomID = "room_id"
        case children, price, adults
        case roomNo = "room_no"
    }
}





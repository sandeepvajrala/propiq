//
//  AddDetailsTableCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 20/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit


class AddDetailsTableCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleImage: UIImageView!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell( _ details: String){
        titleLabel.text = details
//        titleImage.image = details.image
    }

}

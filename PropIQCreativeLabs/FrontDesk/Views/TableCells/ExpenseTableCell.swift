//
//  ExpenseTableCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 01/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import SDWebImage

class ExpenseTableCell: UITableViewCell {

    @IBOutlet weak var expenseBackgroundView: UIView!
    @IBOutlet weak var departmentTitle: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageBackgroundView: UIView!
    @IBOutlet weak var workerImage: UIImageView!
    
    var monthsArray = ["Select Month","January","February","March","April","May","June","July","August","September","October","November","December"]

    override func draw(_ rect: CGRect) {
        expenseBackgroundView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
        imageBackgroundView.circleWithBorderColor(enable: true, color: UIColor.white, width: 2)
        workerImage.circle()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(expenses: Expense) {
        departmentTitle.text = expenses.department
        amountLabel.text = "\(expenses.amount)"
        monthLabel.text = monthsArray[expenses.monthID]
        descriptionLabel.text = expenses.description
        let imageUrl = URL(string: expenses.imagePath)
        workerImage.sd_setImage(with: imageUrl, placeholderImage: #imageLiteral(resourceName: "avatarPlaceholder"))        
    }

}

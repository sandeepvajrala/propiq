//
//  DateAndTimeCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 20/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class DateAndTimeCell: UITableViewCell {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension DateAndTimeCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

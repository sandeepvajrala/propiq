//
//  AddJobsCell.swift
//  AddJobs
//
//  Created by Admin on 29/04/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit

class AddJobsCell: UITableViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

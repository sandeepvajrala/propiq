//
//  DescriptionCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 02/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class DescriptionCell: UITableViewCell {

    @IBOutlet weak var descriptionTextView: UITextView!
    
    override func draw(_ rect: CGRect) {
        descriptionTextView.border(enable: true, withWidth: 1.5, andColor: #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1))
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

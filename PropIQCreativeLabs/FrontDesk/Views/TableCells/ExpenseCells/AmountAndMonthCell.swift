//
//  AmountAndMonthCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 02/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class AmountAndMonthCell: UITableViewCell {

    @IBOutlet weak var amountAndMonthTF: UITextField!
    @IBOutlet weak var imageForAmount: UIImageView!
    @IBOutlet weak var underlineLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

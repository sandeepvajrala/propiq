//
//  DepartmentAndIamgeCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 02/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class DepartmentAndIamgeCell: UITableViewCell {

    @IBOutlet weak var titleLAbel: UILabel!
    @IBOutlet weak var imageForDepartments: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ImagesCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 06/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ImagesCell: UICollectionViewCell {
    
    @IBOutlet weak var imagesView: UIImageView!
}

//
//  LoginPageViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 14/05/18.
//  Developer name : - Shashank K. Atray 
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController

class LoginPageViewController: UIViewController {
    
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var scrScrollView: UIScrollView!
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraintOfStackView: NSLayoutConstraint!
    
    
    // life cycle of the app
    override func viewDidLoad() {
        super.viewDidLoad()
        intialViewSetup()
//        addKeyBoardNotificationMethods()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        txtEmail.text = ""
        txtPassword.text = ""
        txtEmail.resignFirstResponder()
        txtPassword.resignFirstResponder()
    }
    
    // MARK :- keyboard notification value
    
    
    func addKeyBoardNotificationMethods()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -150

    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0

    }

    // MARK: - methods description
    
    
    // set intial view value of the screen
    func intialViewSetup()
    {
        txtEmail.textFieldPadding()
        txtPassword.textFieldPadding()
        btnSignIn.setButtonEnableForPrimaryButton(false)
        txtEmail.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
        txtPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
        
    }
    
    func buttonActive(_ textField : UITextField, string: String){
        
        if (!(textField.text?.trimmedString().isEmpty)! && !(string.isEmpty))
        {
            btnSignIn.setButtonEnableForPrimaryButton(true)
        }
        else
        {
            btnSignIn.setButtonEnableForPrimaryButton(false)
        }
    }
    
    // api call value
    func apiCallForTesting()
    {
        
        self.view.endEditing(true)

        var requestParams = [String : AnyObject]()
        self.view.endEditing(true)
        
        requestParams["user_id"] = txtEmail.text as AnyObject
        requestParams["user_pass"] = txtPassword.text as AnyObject
        requestParams["source"] = "direct" as AnyObject
        requestParams["OSType"] = "iOS" as AnyObject
        requestParams["DeviceModel"] = UIDevice.current.model as AnyObject
        requestParams["OSVersion"] = UIDevice.current.systemVersion as AnyObject
        requestParams["AppVersion"] = String(format: "%@", Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! CVarArg) as AnyObject
        let deviceUUID: String = (UIDevice.current.identifierForVendor?.uuidString)!
        requestParams["DeviceID"] = deviceUUID as AnyObject
        
        print(requestParams)
        
        //https://f1e1bcf0.ngrok.io/PropIQ/api    34.215.99.251:8080/api
        
        _ = ConnectivityHelper.executePOSTRequest(URLString: ApiFile.loginApi, parameters: requestParams, headers: nil, success: { (response) in
            
            print("response: \(response)")
            let json =  self.convertStringToDictionary(text: response as! String)
            
            
            if let responseDict = json
            {
                if let responseCode = responseDict["responseCode"] as? Int
                {
                    print("respCode \(responseCode)")
                    if responseCode == 100
                    {
                        UserDefaults.standard.set(true, forKey: "user_status")
                        UserDetails.sharedInstance.fillingLoginData(loginDetail:responseDict["data"] as! [String : AnyObject?] )
//                        UserDetails.sharedInstance.saveUserDetailstoDefaults(loginDetail: responseDict["data"] as! [String : AnyObject?])
                        self.navigationToNext()
                        
                    }  else if responseCode == 120 {
                        
                        let alert = UIAlertController(title: "Sign In", message: responseDict["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
            }
          
            
        },  failure: { (error) in
            
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }
    
    
    // MARK: - Actions
    
    @IBAction func SignInButtonClicked()
    {
        if  (txtPassword.text?.isValidPassword())!{
            apiCallForTesting()
        }
        else{
            let alert = UIAlertController(title: "Sign In", message: "Please enter correct User Name and password to Sign in", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    // navigation value to setting
    func navigationToNext()
    {
        if UserDetails.sharedInstance.first_login
        {
            performSegue(withIdentifier: "ChangePassword", sender: self)
            // self.navigationController?.performSegue(withIdentifier: "ChangePassword", sender: self)
            
        } else {
            let userWorkStaus = UserDetails.sharedInstance.user_workStatus
            switch userWorkStaus {
            case "1" :
                let navVC = UIStoryboard(name: "landingTabbar", bundle: nil).instantiateViewController(withIdentifier: "PropIQNavigationController") as! PropIQNavigationController
                
                self.present(navVC, animated: false, completion: {
                    
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                    
                })
            case "2" :
                let navVC = UIStoryboard(name: "Frontdesk", bundle: nil).instantiateViewController(withIdentifier: "PBRevealViewController") as! PBRevealViewController
                
                self.present(navVC, animated: false, completion: {
                    
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                    
                })
            case "3" :
                let navVC = UIStoryboard(name: "Maintenance", bundle: nil).instantiateViewController(withIdentifier: "MaintenanceNavigationController") as! PropIQNavigationController
                self.present(navVC, animated: false, completion: {
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                })
                
            case "4" :
                let navVC = UIStoryboard(name: "PropertyManager", bundle: nil).instantiateViewController(withIdentifier: "PMPBRevealViewController") as! PBRevealViewController
                self.present(navVC, animated: false, completion: {
                    (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController = navVC
                })
                
            default : return
            }
            
        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
}

extension LoginPageViewController: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (txtEmail.text?.count)! > 0 && (txtPassword.text?.count)! > 0
        {
            btnSignIn.setButtonEnableForPrimaryButton(true)
        }else{
            btnSignIn.setButtonEnableForPrimaryButton(false)
        }
        
        if textField == txtPassword
        {
            textField.resignFirstResponder()
        }else if textField == txtEmail
        {
            txtPassword.becomeFirstResponder()
        }
        return true
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        self.btnSignIn.setButtonEnableForPrimaryButton(false)
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = textField.text as NSString?
        var newString = nsString?.replacingCharacters(in: range, with: string)
        newString = newString?.trimmedString()
        
        if textField == txtEmail
        {
            textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
            txtPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
            
            buttonActive(txtPassword, string: newString!)
            
        }
        else if textField == txtPassword
        {
            textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
            txtEmail.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
            
            buttonActive(txtEmail, string: newString!)
        }
        return true
        
    }
    
}

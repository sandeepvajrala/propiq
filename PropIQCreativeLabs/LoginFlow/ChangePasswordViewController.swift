//
//  ChangePasswordViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 16/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var btnChangPassword: UIButton!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfPassword: UITextField!
    @IBOutlet weak var scrScrollView: UIScrollView!
    @IBOutlet weak var bottomLayout: NSLayoutConstraint!


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        intialViewSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    // MARK :- keyboard notification value
    
    
    func addKeyBoardNotificationMethods()
    {
        
//        NotificationCenter.default.addObserver(self, selector: #selector(LoginPageViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(LoginPageViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        let info = notification.userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
            self.bottomLayout.constant = keyboardFrame.size.height - 20
            self.scrScrollView.setContentOffset(CGPoint(x:0, y: keyboardFrame.size.height - 50), animated: true)
            
            /*
             if self.isIpad()
             {
             self.lblScreenSeperator.isHidden = true
             self.scrScrollView.setContentOffset(CGPoint(x:0, y:keyboardFrame.size.height + 40), animated: true)
             }
             else
             {
             self.scrScrollView.setContentOffset(CGPoint(x:0, y: keyboardFrame.size.height), animated: true)
             self.lblScreenSeperator.isHidden = true
             //keyboardFrame.size.height   self.scrScrollView.frame.height - ( self.lblScreenSeperator.frame.origin.y + self.lblScreenSeperator.frame.size.height + 30)
             }*/
        })
    }
    
    
    @objc func keyboardWillHide(_ notification: NSNotification)
    {
        // self.lblScreenSeperator.isHidden = false
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.bottomLayout.constant = 0
            /*
             if self.isIpad()
             {
             self.bottomLayout.constant = -160
             }
             else
             {
             self.bottomLayout.constant = 0
             }*/
        })
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    // MARK: - methods description
    
    
    // set intial view value of the screen
    func intialViewSetup()
    {
        txtConfPassword.textFieldPadding()
        txtPassword.textFieldPadding()
       
        txtConfPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
        txtPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
        
         btnChangPassword.setButtonEnableForPrimaryButton(false)
        
    }
    
    func buttonActive(_ textField : UITextField, string: String){
        
        if (!(textField.text?.trimmedString().isEmpty)! && !(string.isEmpty))
        {
            btnChangPassword.setButtonEnableForPrimaryButton(true)
        }
        else
        {
            btnChangPassword.setButtonEnableForPrimaryButton(false)
        }
    }
    
    
    func CheckValidPassword()
    {
        
        self.view.endEditing(true)
        if ((txtPassword.text?.isValidPassword())!)
        {
            if (txtPassword.text != txtConfPassword.text)
            {
                let alert = UIAlertController(title: "Change Password", message:  "Password and Confirm Password do not match.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            }
            else{
                ApiCallForChangePassword()
            }
        }
        else
        {
            var strText = "\u{2022} Password must contain one capital letter."
            strText = strText + "\n\u{2022} Must be atleast 8 digit long."
            strText = strText + "\n\u{2022} Can not contain Special Characters."
            let alert = UIAlertController(title: "Sign In", message: strText, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    // MARK: - Ibaction Values
    
    // MARK: - IBaction
    @IBAction func SignUpButtonClicked()
    {
        // LogFile.addToLog("ConfirmPasswordViewController.swift Password button clicked", forFunction: "\(#function)", withUserID: UserDetails.sharedInstance.user_id)
        CheckValidPassword()
    }

    
    // MARK:- APi calls
    
    func ApiCallForChangePassword()
    {
        
        self.view.endEditing(true)

        var requestParams = [String : AnyObject]()
        self.view.endEditing(true)
        
        requestParams["user_id"] = UserDetails.sharedInstance.User_ID as AnyObject
        requestParams["user_newPass"] = txtPassword.text as AnyObject
       
        
        print(requestParams)
        
        //https://f1e1bcf0.ngrok.io/PropIQ/api    34.215.99.251:8080/api
        
        _ = ConnectivityHelper.executePOSTRequest(URLString: "http://34.215.99.251:8080/User_FirstLogin", parameters: requestParams, headers: nil, success: { (response) in
            
            print("response: \(response)")
            let json =  self.convertStringToDictionary(text: response as! String)
            
            
            if let responseDict = json
            {
                if let responseCode = responseDict["responseCode"] as? Int
                {
                    if responseCode == 100
                    {
                        let alert = UIAlertController(title: "Sign In", message: responseDict["message"] as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                  
                  
                }
            }
            
            
            
        },  failure: { (error) in
            
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }

}


extension ChangePasswordViewController: UITextFieldDelegate
{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (txtConfPassword.text?.count)! > 0 && (txtPassword.text?.count)! > 0
        {
            btnChangPassword.setButtonEnableForPrimaryButton(true)
        }else{
            btnChangPassword.setButtonEnableForPrimaryButton(false)
        }
        
        if textField == txtConfPassword
        {
            textField.resignFirstResponder()
        }else if textField == txtPassword
        {
            txtPassword.becomeFirstResponder()
        }
        return true
        
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        self.btnChangPassword.setButtonEnableForPrimaryButton(false)
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = textField.text as NSString?
        var newString = nsString?.replacingCharacters(in: range, with: string)
        newString = newString?.trimmedString()
        
        if textField == txtPassword
        {
            textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
            txtConfPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
            
            buttonActive(txtPassword, string: newString!)
            
        }
        else if textField == txtConfPassword
        {
            textField.layer.borderColor = UIColor(red: 41.0/255.0, green: 41.0/255.0, blue: 41.0/255.0, alpha: 1.0).cgColor
            txtPassword.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
            
            buttonActive(txtConfPassword, string: newString!)
        }
        return true
        
    }
    
}


//
//  UserDetails.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 21/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class UserDetails: NSObject {
    
    static let sharedInstance: UserDetails = {
        let instance = UserDetails()
        // setup code
        return instance
    }()
    
    var User_ID = ""
    var country_code = ""
    var user_email = ""
    var mobile_num = ""
    var user_firstName = ""
    var user_lastName = ""
    var country_name = ""
    var user_gender = ""
    var user_work = ""
    var first_login = false
    var user_workStatus = ""
    var user_image = ""
        
    
    func clearUserData() {
        User_ID = ""
        country_code = ""
        user_email = ""
        mobile_num = ""
        user_firstName = ""
        user_lastName = ""
        country_name = ""
        user_gender = ""
        user_work = ""
        user_workStatus = ""
        first_login = false
        user_image = ""
        
    }
    
    
    func fillingLoginData (loginDetail:[String: AnyObject?])
    {
        
        if let FirstName = loginDetail["user_firstName"] as? String
        {
            user_firstName = FirstName
        }
        if let lastname = loginDetail["user_lastName"] as? String
        {
            user_lastName = lastname
        }
        if let userId = loginDetail["user_id"] as? Int
        {
            User_ID = String(userId)
        }
        if let UserEmail = loginDetail["user_email"] as? String
        {
            user_email = UserEmail
        }
        
        if let countryName = loginDetail["country_name"] as? String
        {
            country_name = countryName
        }
        if let countryCode = loginDetail["country_code"] as? String
        {
            country_code = countryCode
        }
        if let mobileNum = loginDetail["mobile_num"] as? String
        {
            mobile_num = mobileNum
        }
        if let userGender = loginDetail["user_gender"] as? String
        {
            user_gender = userGender
        }
        if let userWork = loginDetail["user_work"] as? String {
            user_work = userWork
        }
        
        if let userWorkStatus = loginDetail["user_workStatus"] as? Int {
            user_workStatus = "\(userWorkStatus)"
        }
        
        if let userImage = loginDetail["user_image"] as? String {
            user_image = userImage
            UserDefaults.standard.set(userImage, forKey: "user_imageString")
            let url = URL(string: userImage)
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: url, options: SDWebImageDownloaderOptions.lowPriority, progress: nil, completed: { (image, data, error, status) in
                if let image = image {
                    UserDefaults.standard.set(UIImageJPEGRepresentation(image, 100), forKey: "user_image")
                }
            })
            
//            DispatchQueue.global().async {
//                let image = self.convertUrlToImage(urlString: userImage)
//                if let image = image {
//                    UserDefaults.standard.set(UIImageJPEGRepresentation(image, 100), forKey: "user_image")
//                }
//            }

        }

        if let IsFirstLogin = loginDetail["is_firstLogin"] as? Int {
            if IsFirstLogin == 0 {
                first_login = false
            } else {
                first_login = true
            }
        }
        saveUserDetailstoDefaults()
        
    }
    
   
    
    func saveUserDetailstoDefaults() {
        
        UserDefaults.standard.set(user_firstName, forKey: "user_firstName")
        
        UserDefaults.standard.set(user_lastName, forKey: "user_lastName")
        
        UserDefaults.standard.set(User_ID, forKey: "user_id")
        
        UserDefaults.standard.set(user_email, forKey: "user_email")
        
        UserDefaults.standard.set(country_name, forKey: "country_name")
        
        UserDefaults.standard.set(country_code, forKey: "country_code")
        
        UserDefaults.standard.set(mobile_num, forKey: "mobile_num")
        
        UserDefaults.standard.set(user_gender, forKey: "user_gender")
        
        UserDefaults.standard.set(user_work, forKey: "user_work")
        
        UserDefaults.standard.set(user_workStatus, forKey: "user_workStatus")
        
        UserDefaults.standard.set(first_login, forKey: "is_firstLogin")
        
    }
    
    func isUserLogIn() -> Bool{
        if let fname = UserDefaults.standard.value(forKey: "user_firstName") as? String {
            user_firstName = fname
        }
        if let lname = UserDefaults.standard.value(forKey: "user_lastName") as? String {
            user_lastName = lname
        }
        if let emailAdd = UserDefaults.standard.value(forKey: "user_email") as? String {
            user_email = emailAdd
        }
        if let uID = UserDefaults.standard.value(forKey: "user_id") as? String {
            User_ID = uID
        }
        
        if let cell = UserDefaults.standard.value(forKey: "mobile_num") as? String {
            mobile_num = cell
        }
        if let dialCode = UserDefaults.standard.value(forKey: "country_code") as? String{
            country_code = dialCode
        }
        
        if let Gender = UserDefaults.standard.value(forKey: "user_gender") as? String {
            user_gender = Gender
        }
        if let work = UserDefaults.standard.value(forKey: "user_work") as? String {
            user_work = work
        }
        if let userWorkStatus = UserDefaults.standard.value(forKey: "user_workStatus") as? String {
            user_workStatus = userWorkStatus
        }
        if let userImageString = UserDefaults.standard.value(forKey: "user_imageString") as? String{
            user_image = userImageString
        }
        
        // Check if logged in
        
        let status = UserDefaults.standard.bool(forKey: "user_status")
        
        
        if status {
            return true
        } else {
            return false
        }
    }
}







/*
func isUserLoggedIn() -> Bool {
    
    let docDirPath:NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    
    let filePath = docDirPath.appendingPathComponent("UserDetails.plist")
    
    CFBridgingRetain(filePath)
    
    if !FileManager.default.fileExists(atPath: filePath)
    {
        NSDictionary().write(toFile: filePath, atomically: true)
    }
    
    let plistData = NSDictionary(contentsOfFile: filePath) as! [String:AnyObject]
    
    
    if let fname = plistData["user_firstName"] as? String {
        user_firstName = fname
    }
    if let lname = plistData["user_lastName"] as? String {
        user_lastName = lname
    }
    if let emailAdd = plistData["user_email"] as? String {
        user_email = emailAdd
    }
    if let uID = plistData["User_ID"] as? String {
        User_ID = uID
    }
    
    if let cell = plistData["mobile_num"] as? String {
        mobile_num = cell
    }
    if let dialCode = plistData["country_code"] as? String {
        country_code = dialCode
    }
    
    if let Gender = plistData["user_gender"] as? String {
        user_gender = Gender
    }
    if let work = plistData["user_work"] as? String {
        user_work = work
    }
    if let userWorkStatus = plistData["user_workStatus"] as? String {
        user_workStatus = userWorkStatus
    }
    
    // Check if logged in
    
    let status = UserDefaults.standard.bool(forKey: "user_status")
    
    if status {
        return true
    } else {
        return false
    }
    
    //        if User_ID.isEmptyString()  {
    //            return false
    //        } else {
    //            return true
    //        }
}


func saveLoginDetailsToPlist()
{
    let docDirPath:NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    
    let filePath = docDirPath.appendingPathComponent("UserDetails.plist")
    
    CFBridgingRetain(filePath)
    
    if !FileManager.default.fileExists(atPath: filePath)
    {
        NSDictionary().write(toFile: filePath, atomically: true)
    }
    
    var plistData = NSDictionary(contentsOfFile: filePath) as! [String:AnyObject]
    
    plistData["user_firstName"] = user_firstName as AnyObject?
    plistData["user_lastName"] = user_lastName as AnyObject?
    plistData["user_email"] = user_email as AnyObject?
    plistData["User_ID"] = User_ID as AnyObject?
    plistData["mobile_num"] = mobile_num as AnyObject?
    plistData["country_code"] = country_code as AnyObject?
    plistData["country_name"] = country_name as AnyObject?
    plistData["user_gender"] = user_gender as AnyObject?
    plistData["user_work"] = user_work as AnyObject?
    plistData["first_login"] = first_login as Bool as AnyObject
    plistData["user_workStatus"] = user_workStatus as AnyObject?
    
    NSDictionary(dictionary: plistData).write(toFile: filePath, atomically: true)
}
func removeDataFromPlist(){
    let docDirPath:NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
    
    let filePath = docDirPath.appendingPathComponent("UserDetails.plist")
    
    CFBridgingRetain(filePath)
    
    if !FileManager.default.fileExists(atPath: filePath)
    {
        NSDictionary().write(toFile: filePath, atomically: true)
    }
    
    var plistData = NSDictionary(contentsOfFile: filePath) as! [String:AnyObject]
    plistData.removeAll()
    //
    //        plistData["user_firstName"] = "" as AnyObject
    //        plistData["user_lastName"] = "" as AnyObject
    //        plistData["user_email"] = "" as AnyObject
    plistData["User_ID"] = nil
    //        plistData["mobile_num"] = "" as AnyObject
    //        plistData["country_code"] = "" as AnyObject
    //        plistData["country_name"] = "" as AnyObject
    //        plistData["user_gender"] = "" as AnyObject
    //        plistData["user_work"] = "" as AnyObject
    //        plistData["first_login"] = false as AnyObject
    
    NSDictionary(dictionary: plistData).write(toFile: filePath, atomically: true)
}
*/


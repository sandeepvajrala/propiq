//
//  DashboardVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController

class DashboardVC: UIViewController {

    @IBOutlet weak var revenueView: UIView!
    @IBOutlet weak var roomsBookedTableView: UITableView!
    @IBOutlet weak var totalRevenueLabel: UILabel!
    @IBOutlet weak var totalExpenseLabel: UILabel!
    @IBOutlet weak var netAmountLabel: UILabel!
    @IBOutlet var progressButtons: [UIButton]!
    
    //Properties
    
    var jobStatusArray: [String] = []
    var dashboards: Dashboard?
    var dashboardArray = [RoomsBooked]()

    //Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apiForDashboardListApi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

//MARK: - Methods

    func setup() {
        revenueView.round(enable: true, withRadius: 10.0)
        jobStatusArray = ["In-Progress Jobs","Completed Jobs","Rejected Jobs","Pending Jobs","Not Approved Jobs"]
    }
    
    func apiForDashboardListApi() {
        self.view.showLoader()
        _ = ConnectivityHelper.executeGETRequest(URLString: ApiFile.dahsboard, parameters: nil, headers: nil, success: { (response) in

            self.view.hideLoader()
            do {
                let dashboardList = try JSONDecoder().decode(Dashboard.self, from: response as! Data)
                
                self.dashboards = dashboardList
                self.dashboardArray = dashboardList.roomsBooked
                self.updateView()
                self.roomsBookedTableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
    }
    
    func updateView() {
        guard let dashBaord = dashboards else { return }
        self.totalRevenueLabel.text = "$" + dashBaord.totalRevenue
        self.totalExpenseLabel.text = "$" + dashBaord.totalExpense
        self.netAmountLabel.text = "$" + dashBaord.netProfit
        
        progressButtons[0].setTitle(dashBaord.totalInProgressJobs, for: .normal)
        progressButtons[1].setTitle(dashBaord.totalCompletedJobs, for: .normal)
        progressButtons[2].setTitle(dashBaord.totalRejectedJobs, for: .normal)
        progressButtons[3].setTitle(dashBaord.totalPendingJobs, for: .normal)
        progressButtons[4].setTitle(dashBaord.totalNotApprovedJobs, for: .normal)
        
    }
    
    func returnParams(status: String) -> [String: Any] {
        return ["job_status":status,"job_priority":"0","user_id": UserDetails.sharedInstance.User_ID,"department_id":"0"]
    }
    
    //MARK: Actions
    
    @IBAction func progressButtonActions(_ sender: UIButton) {
        
        guard let jobVC = UIStoryboard(name: "Jobs", bundle: nil).instantiateViewController(withIdentifier: "JobViewController") as? JobViewController else { return }
        jobVC.fromPM = true
        
        switch sender.tag {
        case 1:
            jobVC.params = returnParams(status: "2")
            UserDefaults.standard.set(5, forKey: "index")
            self.present(jobVC, animated: true, completion: nil)
        case 2:
            jobVC.params = returnParams(status: "3")
            UserDefaults.standard.set(6, forKey: "index")
            self.present(jobVC, animated: true, completion: nil)
        case 3:
            jobVC.params = returnParams(status: "4")
            UserDefaults.standard.set(7, forKey: "index")
            self.present(jobVC, animated: true, completion: nil)
        case 4:
            jobVC.params = returnParams(status: "1")
            UserDefaults.standard.set(4, forKey: "index")
            self.present(jobVC, animated: true, completion: nil)
        case 5:
            jobVC.params = returnParams(status: "5")
            UserDefaults.standard.set(3, forKey: "index")
            self.present(jobVC, animated: true, completion: nil)
        default:
            return
        }
    }
    
}

//MARK: TableView Methods

extension DashboardVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dashboardArray.count > 0 ? dashboardArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RoomsBookedCell") as? RoomsBookedCell else { return UITableViewCell() }
        if dashboardArray.count > 0 {
            cell.configureCell(roomsBooked: dashboardArray[indexPath.row])
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}









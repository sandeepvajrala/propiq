//
//  RoomsBookedCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 27/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class RoomsBookedCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var roomTypeLabel: UILabel!
    @IBOutlet weak var totalRoomsLabel: UILabel!
    @IBOutlet weak var bookedRoomsLabel: UILabel!
    @IBOutlet weak var totalRevenueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Methods
    
    func setup() {
        outerView.round(enable: true, withRadius: 5)
        outerView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 2.0)
    }
    
    func configureCell(roomsBooked: RoomsBooked) {
        roomTypeLabel.text = roomsBooked.roomType
        totalRoomsLabel.text = "\(roomsBooked.totalRooms)"
        bookedRoomsLabel.text = "\(roomsBooked.totalBookedRooms)"
        totalRevenueLabel.text = "$ \(roomsBooked.totalBookedPrice)"
    }

}

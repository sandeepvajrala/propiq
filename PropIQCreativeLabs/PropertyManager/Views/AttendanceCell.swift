//
//  AttendanceCell.swift
//  PM
//
//  Created by Admin on 02/06/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit


class AttendanceCell: UITableViewCell {

    @IBOutlet weak var workerImage: UIImageView!
    @IBOutlet weak var workerName: UILabel!
    @IBOutlet weak var workerDepartment: UILabel!
    @IBOutlet weak var fromMonth: UILabel!
    @IBOutlet weak var numberOfPresents: UILabel!
    @IBOutlet weak var numberOfAbsents: UILabel!
    @IBOutlet weak var numberOfLeaves: UILabel!
    @IBOutlet weak var outerViewInCell: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        outerViewInCell.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
        outerViewInCell.round(enable: true, withRadius: 3.0)
        workerImage.circleWithBorderColor(enable: true, color: UIColor.white, width: 2.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configureCell(attendanceList: AttendanceDetail) {
        if let firstName = attendanceList.workerFirstName, let lastName = attendanceList.workerLastName {
            workerName.text =  firstName + " " + lastName
        }
        workerDepartment.text = attendanceList.department ?? ""
        if let fromDate = attendanceList.fromDate, let toDate = attendanceList.toDate {
            fromMonth.text = fromDate + " - " +  toDate
        }
        numberOfPresents.text = "\(attendanceList.present ?? 0)"
        numberOfAbsents.text = "\(attendanceList.absent ?? 0)"
        numberOfLeaves.text = "\(attendanceList.onLeave ?? 0)"
        guard let imageString = attendanceList.workerImage else { return }
        workerImage.sd_setImage(with: URL(string: imageString), placeholderImage: #imageLiteral(resourceName: "avatarPlaceholder") )
        
    }
    
}

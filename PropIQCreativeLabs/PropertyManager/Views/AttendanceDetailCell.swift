//
//  AttendanceDetailCell.swift
//  PM
//
//  Created by Admin on 02/06/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit

class AttendanceDetailCell: UITableViewCell {

    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var inTimeLabel: UILabel!
    @IBOutlet weak var inTimeCalculation: UILabel!
    @IBOutlet weak var outTime: UILabel!
    @IBOutlet weak var outTimeCalculation: UILabel!
    @IBOutlet weak var totalHours: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
   
        outerView.round(enable: true, withRadius: 10.0)
        outerView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
   
    func configureCell(attendance: Attendance) {
        
        dateLabel.text = attendance.inDate ?? ""
        inTimeLabel.text = attendance.inTime ?? ""
        inTimeCalculation.text = "(\(attendance.totalInHours ?? 0)h" + "\(attendance.totalInMin ?? 0)m)"
        outTime.text = attendance.outTime
        outTimeCalculation.text = "(\(attendance.totalOutHours ?? 0)h" + "\(attendance.totalOutMin ?? 0)m)"
        totalHours.text = "\(attendance.totalHours ?? 0)h" + "\(attendance.totalMin ?? 0)m"
    }
    
}









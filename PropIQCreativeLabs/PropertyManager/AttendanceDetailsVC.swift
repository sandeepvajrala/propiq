//
//  AttendanceDetailsVC.swift
//  PM
//
//  Created by Admin on 02/06/1940 Saka.
//  Copyright © 1940 Sandeep. All rights reserved.
//

import UIKit

class AttendanceDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var attendanceDetails = [Attendance]()
    var workerId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiCallForuserAttendance()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func apiCallForuserAttendance() {
        

        self.view.showLoader()
        guard let workerDepartmentId = workerId else { return }
        let params = ["worker_id": "\(workerDepartmentId)"]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.userAttendance, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let attendance = try JSONDecoder().decode(WorkerAttendance.self, from: response as! Data)
                self.attendanceDetails = attendance.attendance
                self.tableView.reloadData()
                
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
            
        })
        
    }
    
}

//MARK: - Tableview Methods
extension AttendanceDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendanceDetails.count > 0 ? attendanceDetails.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceDetailCell") as? AttendanceDetailCell else { return UITableViewCell()}
        if attendanceDetails.count > 0 {
            cell.configureCell(attendance: attendanceDetails[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}


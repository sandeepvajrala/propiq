//
//  FrontdeskPerformanceVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 29/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class FrontdeskPerformanceVC: UIViewController {

    //Outlets
    @IBOutlet weak var totalJobs: UILabel!
    @IBOutlet weak var inProgressJobs: UILabel!
    @IBOutlet weak var completedJobs: UILabel!
    @IBOutlet weak var pendingJobs: UILabel!
    @IBOutlet weak var rejectedJobs: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apiCallForPerformanceStats()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Methods
    
    func apiCallForPerformanceStats() {
        self.view.showLoader()
        let params = ["job_date":"","from_date":"","to_date":"","department_id":"","worker_id":""]
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.userPerformanceStats, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let performanceDetails = try JSONDecoder().decode(PerformanceStats.self, from: response as! Data)
                
                self.updateUI(jobs: performanceDetails)
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
        
    }
    func updateUI(jobs: PerformanceStats) {
        totalJobs.text = "\(jobs.totalJobs ?? 0)"
        inProgressJobs.text = "\(jobs.totalInProgressJobs ?? 0)"
        completedJobs.text = "\(jobs.totalCompletedJobs ?? 0)"
        pendingJobs.text = "\(jobs.totalPendingJobs ?? 0)"
        rejectedJobs.text = "\(jobs.totalRejectedJobs ?? 0)"
    }

}

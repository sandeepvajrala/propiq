//
//  Dashboard.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 27/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Dashboard: Codable {
    let totalExpense, totalPendingJobs: String
    let jobs: [JobDetails]
    let message, totalCompletedJobs: String
    let roomsBooked: [RoomsBooked]
    let responseCode: Int
    let totalNotApprovedJobs, totalRevenue, totalInProgressJobs, netProfit: String
    let totalRejectedJobs: String
    let status: Bool
    
    enum CodingKeys: String, CodingKey {
        case totalExpense = "total_expense"
        case totalPendingJobs = "total_pendingJobs"
        case jobs, message
        case totalCompletedJobs = "total_completedJobs"
        case roomsBooked = "rooms_booked"
        case responseCode
        case totalNotApprovedJobs = "total_notApprovedJobs"
        case totalRevenue = "total_revenue"
        case totalInProgressJobs = "total_inProgressJobs"
        case netProfit = "net_profit"
        case totalRejectedJobs = "total_rejectedJobs"
        case status
    }
}

struct JobDetails: Codable {
    let workerName: String
    let inProgressJobs, rejectedJobs, notApprovedJobs, completedJobs: Int
    let pendingJobs, workerTotalJobs, workerID: Int
    
    enum CodingKeys: String, CodingKey {
        case workerName = "worker_name"
        case inProgressJobs = "inProgress_jobs"
        case rejectedJobs = "rejected_jobs"
        case notApprovedJobs = "notApproved_jobs"
        case completedJobs = "completed_jobs"
        case pendingJobs = "pending_jobs"
        case workerTotalJobs = "worker_total_jobs"
        case workerID = "worker_id"
    }
}

struct RoomsBooked: Codable {
    let roomTypeID, totalBookedPrice, totalBookedRooms, totalRooms: Int
    let roomType: String
    
    enum CodingKeys: String, CodingKey {
        case roomTypeID = "room_typeId"
        case totalBookedPrice = "total_bookedPrice"
        case totalBookedRooms = "total_bookedRooms"
        case totalRooms = "total_rooms"
        case roomType = "room_type"
    }
}

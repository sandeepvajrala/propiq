//
//  WorkerAttendance.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 27/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct WorkerAttendance: Codable {
    let workerLastName: String?
    let departmentID: Int?
    let workerFirstName, workerImage, department, message: String?
    let attendance: [Attendance]
    let workerID: Int?
    let status: Bool
    let responseCode: Int
    
    enum CodingKeys: String, CodingKey {
        case workerLastName = "worker_lastName"
        case departmentID = "department_id"
        case workerFirstName = "worker_firstName"
        case workerImage = "worker_image"
        case department, message, attendance
        case workerID = "worker_id"
        case status, responseCode
    }
}

struct Attendance: Codable {
    let outTime: String?
    let attendanceID: Int?
    let inDate: String?
    let totalInMin: Int?
    let outDate, workerShift: String?
    let totalMin, totalHours, workerID: Int?
    let inTime: String?
    let totalOutMin, totalOutHours, totalInHours: Int?
    let attendance: String?
    
    enum CodingKeys: String, CodingKey {
        case outTime = "out_time"
        case attendanceID = "attendance_id"
        case inDate = "in_date"
        case totalInMin = "total_in_min"
        case outDate = "out_date"
        case workerShift = "worker_shift"
        case totalMin = "total_min"
        case totalHours = "total_hours"
        case workerID = "worker_id"
        case inTime = "in_time"
        case totalOutMin = "total_out_min"
        case totalOutHours = "total_out_hours"
        case totalInHours = "total_in_hours"
        case attendance
    }
}

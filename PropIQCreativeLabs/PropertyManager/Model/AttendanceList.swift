//
//  AttendanceList.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 29/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct AttendanceList: Codable {
    let message: String
    let attendance: [AttendanceDetail]
    let status: Bool
    let responseCode: Int
}

struct AttendanceDetail: Codable {
    let workerLastName, fromDate, toDate: String?
    let departmentID, totalAttendance: Int?
    let workerFirstName: String?
    let absent: Int?
    let workerImage: String?
    let present: Int?
    let department: String?
    let onLeave, workerID: Int?
    
    enum CodingKeys: String, CodingKey {
        case workerLastName = "worker_lastName"
        case fromDate = "from_date"
        case toDate = "to_date"
        case departmentID = "department_id"
        case totalAttendance
        case workerFirstName = "worker_firstName"
        case absent
        case workerImage = "worker_image"
        case present, department, onLeave
        case workerID = "worker_id"
    }
}

//
//  PerformanceStats.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 29/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct PerformanceStats: Codable {
    let totalJobs, totalNotApprovedJobs, totalPendingJobs, totalInProgressJobs: Int?
    let jobs: [PerformanceJobs]
    let message: String
    let totalCompletedJobs, totalRejectedJobs: Int?
    let status: Bool
    let responseCode: Int
    
    enum CodingKeys: String, CodingKey {
        case totalJobs = "total_jobs"
        case totalNotApprovedJobs = "total_notApprovedJobs"
        case totalPendingJobs = "total_pendingJobs"
        case totalInProgressJobs = "total_inProgressJobs"
        case jobs, message
        case totalCompletedJobs = "total_completedJobs"
        case totalRejectedJobs = "total_rejectedJobs"
        case status, responseCode
    }
}

struct PerformanceJobs: Codable {
    let inProgressJobs, rejectedJobs, notApprovedJobs, completedJobs: Int
    let pendingJobs, day: Int
    
    enum CodingKeys: String, CodingKey {
        case inProgressJobs = "inProgress_jobs"
        case rejectedJobs = "rejected_jobs"
        case notApprovedJobs = "notApproved_jobs"
        case completedJobs = "completed_jobs"
        case pendingJobs = "pending_jobs"
        case day
    }
}

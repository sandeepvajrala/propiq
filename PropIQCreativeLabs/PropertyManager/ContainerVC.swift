//
//  ContainerVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 29/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController

class ContainerVC: UIViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    
    let dimmingView = UIView()
    lazy var dashboardVC: DashboardVC = {
        var dashboardController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
        return dashboardController
        
    }()
    lazy var attendanceVC: AttendanceVC = {
        var attendanceController = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceVC") as! AttendanceVC
        return attendanceController
    }()
    
    lazy var performanceVC: FrontdeskPerformanceVC = {
       var performanceController = self.storyboard?.instantiateViewController(withIdentifier: "FrontdeskPerformanceVC") as! FrontdeskPerformanceVC
        return performanceController
    }()
   

    override func viewDidLoad() {
        super.viewDidLoad()

        menuSetup()
        NotificationCenter.default.addObserver(self, selector: #selector(onNotificationDetails(notification:)), name: Constants.PMLeftMenuNotificationName, object: nil)
        addChildVC(childVC: dashboardVC)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Methods
    @objc func onNotificationDetails(notification:Notification) {
        
        let dict = notification.userInfo as? [String: Any]
        if let dict = dict {
            let myTitle = dict["title"] as? String
            self.topLabel.text = myTitle
            let items = PMLeftItems.all
            switch myTitle {
            case items[0].title:
                
                self.removeChildViewControllers()
                self.addChildVC(childVC: dashboardVC)
                
            case items[1].title:
                
                self.removeChildViewControllers()
                self.addChildVC(childVC: performanceVC)
                
            case items[2].title:

                self.removeChildViewControllers()
                self.addChildVC(childVC: attendanceVC)
            default: break
            }
        }
    }
    
    func menuSetup() {
        menuButton.addTarget(self.revealViewController(), action: #selector(PBRevealViewController.revealLeftView), for: UIControlEvents.touchUpInside)
        
        self.revealViewController()!.delegate = self
        self.revealViewController()!.leftToggleSpringDampingRatio = 1.0
    }
    
    func removeChildVC(childVC: UIViewController){
        childVC.willMove(toParentViewController: nil)
        childVC.view.removeFromSuperview()
        childVC.removeFromParentViewController()
    }
    func addChildVC(childVC: UIViewController) {
        self.addChildViewController(childVC)
        containerView.addSubview(childVC.view)
        childVC.view.frame = containerView.bounds
        childVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        childVC.didMove(toParentViewController: self)
    
    }
    
}

//MARK: ReavealController Delegate Methods

extension ContainerVC: PBRevealViewControllerDelegate {
    
    func revealController(_ revealController: PBRevealViewController, willShowLeft viewController: UIViewController) {
        dimmingView.frame = self.view.bounds
        self.view.addSubview(self.dimmingView)
        self.dimmingView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.4)
        revealController.mainViewController?.view.isUserInteractionEnabled = false
        revealController.leftViewRevealWidth = 300
    }
    
    func revealController(_ revealController: PBRevealViewController, willHideLeft viewController: UIViewController) {
        self.dimmingView.removeFromSuperview()
        revealController.mainViewController?.view.isUserInteractionEnabled = true
    }
    
}






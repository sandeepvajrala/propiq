//
//  AttendanceVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 27/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import PBRevealViewController

class AttendanceVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dimmingView = UIView()
    var attendenceListArray = [AttendanceDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apiCallForAttendance()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Methods
    func apiCallForAttendance() {
        self.view.showLoader()
        let params = ["from_date": getFirstDayInMonth(startDate: true),"to_date": getFirstDayInMonth(startDate: false)]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.userAttendanceFilter, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            self.view.hideLoader()
            do {
                let attendanceDetail = try JSONDecoder().decode(AttendanceList.self, from: response as! Data)
                self.attendenceListArray = attendanceDetail.attendance
                self.tableView.reloadData()
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Failure")
        }, notRechable: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            self.view.hideLoader()
            self.showAlert(title: "Server Error", message: "Session Expired")
        })
    }
    
    func getFirstDayInMonth(startDate: Bool) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = startDate ?  dateFormatter.string(from: Date().startOfMonth()) : dateFormatter.string(from: Date())
        return date
    }
    
}
extension AttendanceVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendenceListArray.count > 0 ? attendenceListArray.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceCell") as? AttendanceCell else { return UITableViewCell() }
        cell.configureCell(attendanceList: attendenceListArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let attendenceDetails = self.storyboard?.instantiateViewController(withIdentifier: "AttendanceDetailsVC") as? AttendanceDetailsVC else { return }
        if let id = attendenceListArray[indexPath.row].workerID {
            attendenceDetails.workerId = id
        }
        self.present(attendenceDetails, animated: true, completion: nil)
        
    }
}


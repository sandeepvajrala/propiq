//
//  PMLeftVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 23/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
enum PMLeftItems {
//    case title
//    case image
//
//    static let all: [PMLeftItems] = [.title, .image]
//
//    var rows: [(title: String, image: UIImage)]{
//        return  [(title: "Dashboard",image: #imageLiteral(resourceName: "downArrow")),(title: "Front Desk Performance",image: #imageLiteral(resourceName: "downArrow")),(title: "Attendence",image: #imageLiteral(resourceName: "downArrow"))]
//    }
    
    
    case Dashboard
    case FrontDeskPerformance
    case Attendance

    static let all:[PMLeftItems] = [.Dashboard, .FrontDeskPerformance, .Attendance]

    var title: String {
        switch self {
        case .Dashboard: return "Dashboard"
        case .FrontDeskPerformance: return "Front Desk Performance"
        case .Attendance: return "Attendence"
        }
    }

    var rows: [(title: String, image: UIImage)]{
        return  [(title: "Dashboard",image: #imageLiteral(resourceName: "All")),(title: "Front Desk Performance",image: #imageLiteral(resourceName: "All")),(title: "Attendence",image: #imageLiteral(resourceName: "All"))]
    }

}

class PMLeftVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//MARK: - Tableview Delegate Methods

extension PMLeftVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PMLeftItems.all.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let allValues = PMLeftItems.all[indexPath.row]
        let rows = allValues.rows[indexPath.row]
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableCell") as? MenuTableCell else{  return UITableViewCell()}
        cell.nameLabel.text = rows.title
        cell.iconImageView.image = rows.image
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let allValues = PMLeftItems.all[indexPath.row]
        let rows = allValues.rows[indexPath.row]
        switch indexPath.row {
        case 0:
            NotificationCenter.default.post(name: Constants.PMLeftMenuNotificationName, object: nil, userInfo:["title": rows.title])
            self.revealViewController()?.hideLeftView(animated: true)
        case 1:
            NotificationCenter.default.post(name: Constants.PMLeftMenuNotificationName, object: nil, userInfo:["title": rows.title])
            self.revealViewController()?.hideLeftView(animated: true)
        case 2:

//            guard let attendanceVC = UIStoryboard(name: "Dashboard", bundle: nil).instantiateViewController(withIdentifier: "AttendanceVC") as? AttendanceVC else { return }
//            present(attendanceVC, animated: true, completion: nil)
            NotificationCenter.default.post(name: Constants.PMLeftMenuNotificationName, object: nil, userInfo:["title": rows.title])
            self.revealViewController()?.hideLeftView(animated: true)
            
        default: return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}












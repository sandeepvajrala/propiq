//
//  Modal.swift
//  ModalView

//  Copyright © 2017. All rights reserved.
//

import Foundation

import UIKit

protocol LoaderModal {
    func show(animated:Bool)
    func dismiss(animated:Bool)
    var backgroundView:UIView { get }
    var dialogView:UIView { get set }
}

extension LoaderModal where Self:UIView {
    func show(animated:Bool){
        self.backgroundView.alpha = 0
        UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self)
        if animated {
            UIView.animate(withDuration: 0.23, animations: {
                self.backgroundView.alpha = 0.88
            })
            UIView.animate(withDuration: 0.23, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 20, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center  = self.center
            }, completion: { (completed) in
                
            })
        }else{
            self.backgroundView.alpha = 0.88
            self.dialogView.center  = self.center
        }
    }
    
    func dismiss(animated:Bool){
        
        if animated {
            UIView.animate(withDuration: 0.23, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            UIView.animate(withDuration: 0.23, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
            }, completion: { (completed) in
                self.removeFromSuperview()
            })
        }else{
            self.removeFromSuperview()
        }
        
    }
}


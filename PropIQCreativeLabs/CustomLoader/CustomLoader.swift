//
//  CustomLoader.swift
//  Younique
//
//  Created by AcmeMinds on 24/11/17.
//  Copyright © 2017 AcmeMinds. All rights reserved.
//

import UIKit

class CustomLoader: UIView , LoaderModal {
    var backgroundView = UIView()
    var dialogView = UIView()
    
    
    
    convenience init(title:String,image:UIImage) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title, image: image)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(title:String, image:UIImage){
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.white
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappedOnBackgroundView)))
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-64
        
        let titleLabel = UILabel(frame: CGRect(x: 8, y: 8, width: dialogViewWidth-16, height: 30))
        titleLabel.text = title
        titleLabel.textAlignment = .center
      //  dialogView.addSubview(titleLabel)
        
        let separatorLineView = UIView()
        separatorLineView.frame.origin = CGPoint(x: 0, y: titleLabel.frame.height + 8)
        separatorLineView.frame.size = CGSize(width: dialogViewWidth, height: 1)
        separatorLineView.backgroundColor = UIColor.groupTableViewBackground
     //   dialogView.addSubview(separatorLineView)
        
        
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.clear
       
        imageView.frame.origin = CGPoint(x: 50, y: separatorLineView.frame.height + separatorLineView.frame.origin.y + 50)
        imageView.frame.size = CGSize(width: dialogViewWidth - 170 , height: dialogViewWidth - 170)
        /*
        if isIpad()
        {
           
            imageView.frame.size = CGSize(width: dialogViewWidth - 400 , height: dialogViewWidth - 530)
        }*/
        print(imageView.frame.size)
        imageView.layer.cornerRadius = 4
        imageView.image = image
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleTopMargin]
        imageView.contentMode = .scaleAspectFit// OR .scaleAspectFill
        
        imageView.clipsToBounds = true
       
        dialogView.addSubview(imageView)
        
        let dialogViewHeight = titleLabel.frame.height + 8 + separatorLineView.frame.height + 8 + imageView.frame.height + 8
        
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: dialogViewHeight)
        dialogView.backgroundColor = UIColor.clear
        dialogView.layer.cornerRadius = 6
        addSubview(dialogView)
    }
    
    @objc func didTappedOnBackgroundView(){
        dismiss(animated: true)
    }
    
}

//
//  ContactsViewController.swift
//  Chat
//
//  Created by Tangerine Creative on 09/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let chatApi = "http://34.215.99.251:8080/User_Friends"
    var friends: [Friend]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        callApiForFriendsList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func callApiForFriendsList() {
        
        self.view.showLoader()
        
        let params = ["user_id": myId,"department_id": myDepartmentId]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: self.chatApi, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            print(response)
            self.view.hideLoader()
            do {
                let friends = try JSONDecoder().decode(Friends.self, from: response as! Data)
                self.friends = friends.friends
                self.tableView.reloadData()
            } catch let error {
                print(error)
            }
            
        }, failure: { (error) in
            self.view.hideLoader()
            
        }, notRechable: {
            self.view.hideLoader()
            
        }, sessionExpired: {
            self.view.hideLoader()
            
        })
    }

}

extension ContactsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let friends = self.friends else { return 0 }
        return friends.count > 0 ? friends.count : 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactsTableCell") as? ContactsTableCell else { return UITableViewCell() }
        if let friends = friends {
            cell.configureCell(friend: friends[indexPath.row])
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatVC = ChatViewController()
        if let friends = friends {
            chatVC.friendDetails = friends[indexPath.row]
        }
        self.navigationController?.pushViewController(chatVC, animated: true)
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}


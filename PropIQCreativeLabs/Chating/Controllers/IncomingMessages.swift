//
//  IncomingMessages.swift
//  Chat
//
//  Created by Tangerine Creative on 13/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class IncomingMessages {
    
    var collectionView: JSQMessagesCollectionView
    var incomingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero).incomingMessagesBubbleImage(with: UIColor.white)
    
    init(collectionView_ : JSQMessagesCollectionView) {
        collectionView = collectionView_
    }
    
    //MARK: Creeate Message
    func createMessage(messageDictionary: NSDictionary, chatRoomId: String) -> JSQMessage? {
        var message: JSQMessage?
        let text = messageDictionary["text"] as! String
        if text.isValidURL {
            message = createPictureMessage(messageDictionary: messageDictionary)
        } else {
            message = createText(messageDictionary: messageDictionary)
        }
        

//        let type = messageDictionary[""] as? String
//
//        switch type {
//        case "text":
//            message = createText(messageDictionary: messageDictionary)
//
//        default:
//            print("unknown message type")
//        }

        if message != nil{
            return message!
        }
        return nil

    }
    
    func createText(messageDictionary: NSDictionary) -> JSQMessage {
        
        let senderId = messageDictionary["senderid"] as? String
        let text = messageDictionary["text"] as? String
        let name = messageDictionary["sendername"] as? String
        let timeStamp = messageDictionary["timestamp"] as? String
        var dateAndTime = Date()
        if let timeStamp = timeStamp {
            if let intTime = Int(timeStamp) {
                let date = Date(milliseconds: intTime)
                dateAndTime = date ?? Date()
            }
        }
    
        return JSQMessage(senderId: senderId, senderDisplayName: name, date: dateAndTime, text: text)
    
    }
    
    func createPictureMessage(messageDictionary: NSDictionary) -> JSQMessage {
        
        let senderId = messageDictionary["senderid"] as? String ?? ""
        
        let text = messageDictionary["text"] as? String ?? ""
        let name = messageDictionary["sendername"] as? String ?? ""
        let timeStamp = messageDictionary["timestamp"] as? String
        var dateAndTime = Date()
        if let timeStamp = timeStamp {
            if let intTime = Int(timeStamp) {
                let date = Date(milliseconds: intTime)
                dateAndTime = date ?? Date()
            }
        }
        
        let mediaItem = PhotoMediaItem(image: nil)
//        mediaItem?.appliesMediaViewMaskAsOutgoing = false
        downloadImage(imageUrl: text ) { (image) in
            
            if image != nil {
                mediaItem?.image = image!
                self.collectionView.reloadData()
            }
        }
        return JSQMessage(senderId: senderId, senderDisplayName: name, date: dateAndTime, media: mediaItem)

    }
    
//    func containsURL() -> Bool {
//        let str = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=_|!:,.;]*[-a-zA-Z0-9+&@#/%=_|]"
//        let pattern =
//    }
//
//    public boolean containsURL(String content){
//    String REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=_|!:,.;]*[-a-zA-Z0-9+&@#/%=_|]";
//    Pattern p = Pattern.compile(REGEX,Pattern.CASE_INSENSITIVE);
//    Matcher m = p.matcher(content);
//    if(m.find()) {
//    return true;
//    }
//
//    return false;
//    }
    func returnOutgoingStatusForUser(senderId: String) -> Bool {
        
        return senderId == myId
    }

}

extension String {
    
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.endIndex.encodedOffset)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.endIndex.encodedOffset
        } else {
            return false
        }
    }
}

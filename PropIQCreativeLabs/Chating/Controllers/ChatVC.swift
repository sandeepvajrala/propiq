//
//  ChatVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 08/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import CarbonKit
import MBProgressHUD

class ChatVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupForCarbonNavigationBar()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ChatVC: CarbonTabSwipeNavigationDelegate {
    
    func setupForCarbonNavigationBar() {
        let items = ["Chat", "Contacts", "User Info"]
        let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        
        let menuTabItems = items.count
        let tabWidth = (self.view.frame.width / CGFloat(menuTabItems))
        for index in 0 ..< menuTabItems {
            carbonTabSwipeNavigation.carbonSegmentedControl?.setWidth(tabWidth, forSegmentAt: index)
        }
        carbonTabSwipeNavigation.toolbar.barTintColor = UIColor.white
        carbonTabSwipeNavigation.toolbar.backgroundColor = UIColor.white
        carbonTabSwipeNavigation.setTabBarHeight(50)
        carbonTabSwipeNavigation.setIndicatorHeight(1.5)
        carbonTabSwipeNavigation.setIndicatorColor(#colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1))
        carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1))
        carbonTabSwipeNavigation.setSelectedColor(#colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1), font: UIFont(name: "Helvetica Neue", size: 20.0)!)
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.setNormalColor(#colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1), font: UIFont(name: "Helvetica Neue", size: 20.0)!)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
//        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
//        navigationController?.navigationBar.backItem?.title = "Chat"
        
//        navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1)
    }

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        guard let storyboard = storyboard else { return UIViewController() }
        if index == 0 {
            return storyboard.instantiateViewController(withIdentifier: "ChatListVC")
        } else if index == 1 {
            return storyboard.instantiateViewController(withIdentifier: "ContactsViewController")
        } else {
            return storyboard.instantiateViewController(withIdentifier: "UserInfoVC")

        }
    }
    
}







 //
//  ChatViewController.swift
//  Chat
//
//  Created by Tangerine Creative on 13/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

 import UIKit
 import JSQMessagesViewController
 import Firebase
 import FirebaseDatabase

class ChatViewController: JSQMessagesViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    var messageArray : [Message] = []
    var myMessageDetails:[Messages] = []
    var friendDetails: Friend?
    var messages:[JSQMessage] = []
    var groupKey = ""
    
    var bgView: UIView!
    var animated: Bool = true
    var intDuration = 0.25
    
    var selectedImage: UIImage?
    
    
    var outgoingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero).outgoingMessagesBubbleImage(with: #colorLiteral(red: 1, green: 0.8999102712, blue: 0.7498986125, alpha: 1))
    
    var incomingBubble = JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero).incomingMessagesBubbleImage(with: UIColor.white)
 
    override func viewDidLoad() {
        super.viewDidLoad()
        messageArray = [Message]()
        myMessageDetails = [Messages]()
        messages = [JSQMessage]()
        
        self.inputToolbar.preferredDefaultHeight = 50
        self.backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        
        self.view.backgroundColor = #colorLiteral(red: 0.9562498927, green: 0.9562721848, blue: 0.9562601447, alpha: 1)
        self.collectionView.backgroundColor = #colorLiteral(red: 0.9562498927, green: 0.9562721848, blue: 0.9562601447, alpha: 1)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = UIColor.white
        }
        UIApplication.shared.statusBarStyle = .lightContent
            self.senderId = myId
            self.senderDisplayName = myName
        if let friend = friendDetails {
            self.nameLabel.text = friend.name
            
        }
        self.title = myName
        retrieveMessages()
        collectionView.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
    }
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
//        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.scrollToBottom(animated: true)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //JSQMsg Delegate Functions

    override func didPressAccessoryButton(_ sender: UIButton!) {
        
        let camera = Camera(delegate_: self)
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhotoOrVideo = UIAlertAction(title: "Camera", style: .default) { (action) in
            camera.presentPhotoCamera(target: self, canEdit: false)
            print("Camera")
        }
        
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            camera.presentPhotoLibrary(target: self, canEdit: false)
            print("Photo Library")
        }
        
        let shareVideo = UIAlertAction(title: "Video Library", style: .default) { (action) in
            print("Video Library")
        }
        
        let shareLocation = UIAlertAction(title: "Share Location", style: .default) { (action) in
            print("Share Location")
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
        }
        
        takePhotoOrVideo.setValue(UIImage(), forKey: "image")
        sharePhoto.setValue(UIImage(), forKey: "image")
        shareVideo.setValue(UIImage(), forKey: "image")
        shareLocation.setValue(UIImage(), forKey: "image")
        
        optionMenu.addAction(takePhotoOrVideo)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(shareVideo)
        optionMenu.addAction(shareLocation)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)

    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        if text != "" && text != nil {
            self.sendMessage(text: text, date: date, picture: nil, location: nil, video: nil, audio: nil)
            updateSendButton(isSend: false)
            
        } else{
            print("audio message")
        }
    }
    //MARK : Send Message
    
    func sendMessage(text: String?, date: Date, picture: UIImage?, location: String?, video: NSURL?, audio: String? ) {
        guard let friend = friendDetails else { return }
        
        if groupKey == "" {
            groupKey = myId + "-" + friend.uid
        }
        
        
        if let text = text {
            DataService.instance.uploadMessages(withMessage: text, forUID: "", withGroupKey: groupKey, receiverName: friend.name, receiverPhoto: friend.image, receiverId: friend.uid , senderName: myName, senderId: myId, senderPhoto: myImage) { (returnedSnapshot) in
                
                print(returnedSnapshot)
                self.finishSendingMessage()
            }
        }
        if let pic = picture {
            uploadImage(image: pic, chatRoomId: groupKey, view: self.view) { (imageLink) in
                if imageLink != nil {
                    guard let text = imageLink else { return }
                    DataService.instance.uploadMessages(withMessage: text, forUID: "", withGroupKey: self.groupKey, receiverName: friend.name, receiverPhoto: friend.image, receiverId: friend.uid, senderName: myName, senderId: myId, senderPhoto: myImage, sendComplete: { (returnedSnapshot) in
                        print(returnedSnapshot)
                        self.finishSendingMessage()
                    })
                }
            }
        }
        
    }
    
    //MARK: Retrieve Messages
    
    func retrieveMessages() {

        let messageDB = DB_BASE.child("messages")
        guard let friend = friendDetails else { return }
        
        messageDB.observeSingleEvent(of: .value) { (snapshot) in
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                let child = item.key
                if child == friend.uid+"-"+myId {
                    self.groupKey = child
                    break
                } else if child == myId + "-" + friend.uid {
                    self.groupKey = child
                    break
                } else {
                    self.groupKey = myId + "-" + friend.uid
                }
            }
            if self.groupKey != "" {
                
                let incomingMessages = IncomingMessages(collectionView_: self.collectionView!)
                
                let childMessageDB = messageDB.child(self.groupKey)
                childMessageDB.observe(.childAdded) { (snapshot) in
                    
                    guard let snapshotValue = snapshot.value as? NSDictionary else { return }
                    
                    let message = incomingMessages.createMessage(messageDictionary: snapshotValue as NSDictionary, chatRoomId: self.groupKey)
                    if let message = message {
                        self.messages.append(message)
                    }
                    self.collectionView.reloadData()
                    self.scrollToBottom(animated: true)
                    self.finishReceivingMessage()

                }
            }
        }
    }
    
    //MARK: JSQ datasourse Functions
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        if messages.count > 0{
            
            let data = messages[indexPath.row]
            if let text = checkForDayToShowTimestamp(index: indexPath) {
                cell.timeLable?.text = text
            }
            guard let friend = friendDetails else { return cell }
            if data.senderId == friend.uid {
                cell.messageBubbleContainerView.backgroundColor = UIColor.white
            } else {
                cell.messageBubbleContainerView.backgroundColor = #colorLiteral(red: 1, green: 0.8999102712, blue: 0.7498986125, alpha: 1)

            }
            cell.messageBubbleContainerView.layer.cornerRadius = 10.0
            cell.messageBubbleContainerView.shadow(enable: true, colored: UIColor.gray.cgColor, withRadius: 2.0)
            
            cell.textView?.textColor = .black
            
            
        }
        
        return cell
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        
        let message = messages[indexPath.row]
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date)
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData? {
        if messages.count > 0 {
            return messages[indexPath.row]
        }

        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        let message = messages[indexPath.row]
        if message.isMediaMessage {
            if let test = self.getImage(indexPath: indexPath) {
                selectedImage = test
                popUpImageToFullScreen()
                
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if messages.count > 0 {
            return messages.count
        }
        return 0
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource? {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        if indexPath.item == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        } else {
            return self.firstMessageOfTheDay(indexOfMessage: indexPath) ? kJSQMessagesCollectionViewCellLabelHeightDefault : 0.0
        }
        
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellBottomLabelAt indexPath: IndexPath!) -> CGFloat {
        return 10.0
    }
    
    // Update send button
    
    override func textViewDidChange(_ textView: UITextView) {

        if textView.text != "" {
            updateSendButton(isSend: false)
        } else {
            updateSendButton(isSend: true)
        }
    }
    
    func updateSendButton(isSend: Bool) {
        if isSend {
            self.inputToolbar.contentView.rightBarButtonItem.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.inputToolbar.contentView.rightBarButtonItem.setTitle("", for: .normal)
            self.inputToolbar.contentView.rightBarButtonItem.isUserInteractionEnabled = false
            
        } else {
            self.inputToolbar.contentView.rightBarButtonItem.setImage(#imageLiteral(resourceName: "send"), for: .normal)
            self.inputToolbar.contentView.rightBarButtonItem.setTitle("", for: .normal)
            self.inputToolbar.contentView.rightBarButtonItem.isUserInteractionEnabled = true

        }
    }
    
    //MARK: UIImagePickerController Delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let video = info[UIImagePickerControllerMediaURL] as? NSURL
        let picture = info[UIImagePickerControllerOriginalImage] as? UIImage
        sendMessage(text: nil, date: Date(), picture: picture, location: nil, video: video, audio: nil)
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    //MARK: Helper functions
    override func scrollToBottom(animated: Bool) {
        if messages.count > 0 {
             let lastIndex = messages.count - 1
            let scrollHeight = self.collectionView.collectionViewLayout.collectionViewContentSize.height
            let isContentTooSmall = scrollHeight < self.collectionView.bounds.size.height
            
            if !isContentTooSmall {
                // general case
                let indexpath = IndexPath(item: lastIndex, section: 0)
                self.collectionView.scrollToItem(at: indexpath, at: UICollectionViewScrollPosition.top, animated: true)
                
            } else {
                // fix for the first few messages not scrolling properly
                let scrollRect = CGRect(x: 0, y: scrollHeight - 1, width: 1, height: 1)
                self.collectionView.scrollRectToVisible(scrollRect , animated: animated)
            }
        }
       
    }
    
    func firstMessageOfTheDay(indexOfMessage: IndexPath) -> Bool {
        if messages.count > 0 {
            let messageDate = messages[indexOfMessage.item].date
           
            guard let previouseMessageDate = messages[indexOfMessage.item - 1].date else {
                return true // because there is no previous message so we need to show the date
            }
            let day = Calendar.current.component(.day, from: messageDate!)
            let previouseDay = Calendar.current.component(.day, from: previouseMessageDate)
            if day == previouseDay {
                return false
            } else {
                return true
            }
        }
        return false
    }
    
    func checkForDayToShowTimestamp(index: IndexPath) -> String? {
        guard let messageDate = messages[index.item].date else { return ""}
        let day = Calendar.current.component(.day, from: messageDate)
        let today = Calendar.current.component(.day, from: Date())
        if day == today {
            let formater = DateFormatter()
            formater.dateFormat = "h:mm a"
            return formater.string(from: messageDate)
        } else {
            let formater = DateFormatter()
            formater.dateFormat = "MMM d"
            return formater.string(from: messageDate)
        }
    }
}

 extension ChatViewController {

    func getImage(indexPath: IndexPath) -> UIImage? {
        let message = self.messages[indexPath.row]
        if message.isMediaMessage == true {
            let mediaItem = message.media
            if mediaItem is JSQPhotoMediaItem {
                let photoItem = mediaItem as! JSQPhotoMediaItem
                if let test: UIImage = photoItem.image {
                    let image = test
                    return image
                }
            }
        }
        return nil
    }
    @objc func exitFullScreen () {
        let imageV = bgView.subviews[0] as! UIImageView
        
        UIView.animate(withDuration: intDuration, animations: {
            imageV.frame = self.view.bounds
            self.bgView.alpha = 0
        }) { (bol) in
            self.bgView.removeFromSuperview()
        }
    }
    @objc func popUpImageToFullScreen() {
        
        if let window = UIApplication.shared.delegate?.window {
            let parentView = self.view
            
            bgView = UIView(frame: UIScreen.main.bounds)
            bgView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.exitFullScreen)))
            bgView.alpha = 0
            bgView.backgroundColor = UIColor.darkGray
            let imageV = UIImageView(image: self.selectedImage)

            imageV.frame = self.view.bounds
            imageV.contentMode = .scaleAspectFit
            self.bgView.addSubview(imageV)
            window?.addSubview(bgView)
            
            if animated {
                UIView.animate(withDuration: intDuration, animations: {
                    self.bgView.alpha = 1
                    imageV.frame = CGRect(x: 0, y: 0, width: (parentView?.frame.width)!, height: (parentView?.frame.width)!)
                    imageV.center = (parentView?.center)!
                })
            }
        }
    }

 }
 

//
//  ChatListVC.swift
//  Chat
//
//  Created by Tangerine Creative on 10/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

var myId = UserDetails.sharedInstance.User_ID
var myName = UserDetails.sharedInstance.user_firstName + " " + UserDetails.sharedInstance.user_lastName
var myDepartmentId = UserDetails.sharedInstance.user_workStatus
var myImage = UserDetails.sharedInstance.user_image

class ChatListVC: UIViewController {
    
    var messages:[Messages] = []
    var messagesModel = [MessagesModel]()
    var groupKeys = 0
    var messagesDictionary = [String: MessagesModel]()

    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messages = [Messages]()
//        retrieveMessages()
        messagesModel.removeAll()
        observeUserMessages()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        myId = UserDetails.sharedInstance.User_ID
        myName = UserDetails.sharedInstance.user_firstName + " " + UserDetails.sharedInstance.user_lastName
        myDepartmentId = UserDetails.sharedInstance.user_workStatus
        myImage = UserDetails.sharedInstance.user_image

    }
     
    
    func observeUserMessages() {
        let uid =  UserDetails.sharedInstance.User_ID
        if uid == "" {
            return
        }
        
        let ref = DB_BASE.child("messages")
        self.view.showLoader()
        ref.observe(.childAdded, with: { (snapshot) in
            let userId = snapshot.key
            if userId.contains(uid) {
                ref.child(userId).observe(.childAdded, with: { (snapshot) in
                    let messageId = snapshot.key
                    //                self.fetchMessageWithMessageId(messageId)
                    
                    ref.child(userId).child(messageId).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        if let dictionary = snapshot.value as? [String: AnyObject] {
                            let message = MessagesModel(dictionary: dictionary)
                            
                            if let chatPartnerId = message.chatPartnerId() {
                                self.messagesDictionary[chatPartnerId] = message
                            }
                            
                            self.attemptReloadOfTable()
                        }
                        
                    }, withCancel: nil)
                    
                }, withCancel: nil)
            } 
            
        self.view.hideLoader()
        }, withCancel: nil)
        
    }
    
    fileprivate func fetchMessageWithMessageId(_ messageId: String) {
        
        let messagesReference = DB_BASE.child("messages").child(messageId)
        
        messagesReference.observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let message = MessagesModel(dictionary: dictionary)
                
                if let chatPartnerId = message.chatPartnerId() {
                    self.messagesDictionary[chatPartnerId] = message
                }
                
                self.attemptReloadOfTable()
            }
            
        }, withCancel: nil)
    }
    
    var timer: Timer?
    
    fileprivate func attemptReloadOfTable() {
        self.timer?.invalidate()
        
        self.timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.handleReloadTable), userInfo: nil, repeats: false)
    }
    
    @objc func handleReloadTable() {
        self.messagesModel = Array(self.messagesDictionary.values)
        self.messagesModel.sort(by: { (message1, message2) -> Bool in
            if let m1TimeStamp = message1.timestamp, let m2Timestamp = message2.timestamp {
                if let firstInt = Int(m1TimeStamp) , let secondInt = Int(m2Timestamp) {
                    let time1 = Date(milliseconds: firstInt)
                    let time2 = Date(milliseconds: secondInt)
                    
                    return time1! > time2!
                }
             
            }
            return false
        })
        
//        self.messagesModel = self.messagesModel.sorted(by: {$0.timestamp! > $1.timestamp!})
        
        //this will crash because of background thread, so lets call this on dispatch_async main thread
        DispatchQueue.main.async(execute: {
            self.tableView.reloadData()
        })
    }
    
    func retrieveMessages() {
        
        let messageDB = DB_BASE.child("messages")
        messageDB.observe(.value) { (snapshot) in
            self.messages.removeAll()
            self.messages = []
            for item in snapshot.children.allObjects as! [DataSnapshot] {

                let child = item.key
                if child.contains(myId) {
                    let childMessageDB = messageDB.child(child)
                    
                    childMessageDB.queryLimited(toLast: 1).observe(.value, with: { (snapshot) in
                        
                        let snapshotValue = snapshot.value
                        guard let message = self.createMessage(messageDictionary: snapshotValue as! NSDictionary ) else { return }
                        let receiverId = message.receiverid
                        if receiverId == myId && message.senderid == myId {
                            return
                        } else if receiverId == myId {
                            self.messages.append(message)
                            self.messages = self.messages.sorted(by: {$0.timestamp > $1.timestamp})
                            
                            
                        } else if message.senderid == myId {
                            self.messages.append(message)
                            self.messages = self.messages.sorted(by: {$0.timestamp > $1.timestamp})
                            
                        }
                        self.tableView.reloadData()
                        
                    })

                }
                
            }
        }
    }
    
    func createMessage(messageDictionary: NSDictionary) -> Messages? {
        
        for (_,value) in messageDictionary {
            guard let myDictionary = value as? NSDictionary else { return nil}
            let receiverId = myDictionary["receiverid"] as? String ?? ""
            let senderId = myDictionary["senderid"] as? String ?? ""
            let receiverName = myDictionary["receivername"] as? String ?? ""
            let text = myDictionary["text"] as? String ?? ""
            let senderName = myDictionary["sendername"] as? String ?? ""
            let timeStamp = myDictionary["timestamp"] as? String
            let receiverphoto = myDictionary["receiverphoto"] as? String ?? ""
            let senderphoto = myDictionary["senderphoto"] as? String ?? ""
            var myDate = Date()
            if let dat = timeStamp {
                if let val = Int(dat) {
                    myDate = Date(milliseconds: val) ?? Date()
                }
                
            }
            let message = Messages(receiverid: receiverId  , receivername: receiverName , receiverphoto: receiverphoto, senderid: senderId, sendername: senderName, senderphoto: senderphoto, text: text, timestamp: myDate )
            return message
        }
        
        return nil
    }
    
    func createFriendDetails(messageDetails: Messages) -> Friend? {
        
        if messageDetails.receiverid == myId {
            let uid = messageDetails.senderid
            let image = messageDetails.senderphoto
            let name = messageDetails.sendername

            return Friend(uid: uid, image: image, deviceID: nil, work: "", name: name)
        } else {
            let uid = messageDetails.receiverid
            let image = messageDetails.receiverphoto
            let name = messageDetails.receivername
            return Friend(uid: uid, image: image, deviceID: nil,work: "", name: name)
        }

    }
    
}

extension ChatListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messagesModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as? ChatListCell else { return UITableViewCell() }
        
        let message = messagesModel[indexPath.row]
        cell.message = message
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatVC = ChatViewController()

        guard let uid = messagesModel[indexPath.row].chatPartnerId() else { return }
        guard let image = messagesModel[indexPath.row].chatPartnerImage() else { return }
        guard let name = messagesModel[indexPath.row].chatPartnerName() else { return }
        chatVC.friendDetails = Friend(uid: uid, image: image, deviceID: nil, work: "", name: name)
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}


/*
extension ChatListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as? ChatListCell else { return UITableViewCell() }
        
        if messages.count > 0 {

            cell.configureCell(message: messages[indexPath.row])

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let chatVC = ChatViewController()
        let friends = createFriendDetails(messageDetails: messages[indexPath.row])
        if let friends = friends {
            chatVC.friendDetails = friends
        }
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
*/

//
//  UserInfoVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 10/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class UserInfoVC: UIViewController {

    //Outlets
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDepartment: UILabel!
    @IBOutlet weak var userMailId: UIButton!
    @IBOutlet weak var userContactNo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupProfileDetails()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func setupProfileDetails() {
        let firstName = UserDefaults.standard.value(forKey: "user_firstName") as? String
        let lastname = UserDefaults.standard.value(forKey: "user_lastName") as? String
        if let firstName = firstName, let lastName = lastname {
            userName.text = firstName + " " + lastName
        }
        let mailId = UserDefaults.standard.value(forKey: "user_email") as? String
        let contactNo = UserDefaults.standard.value(forKey: "mobile_num") as? String
        if let mail = mailId, let contactNumber = contactNo {
            userMailId.setTitle(mail, for: .normal)
            userContactNo.setTitle(contactNumber, for: .normal)
        }
        userDepartment.text = UserDetails.sharedInstance.user_work
        guard let imageData = UserDefaults.standard.value(forKey: "user_image") as? Data else { return }
        userImage.image = UIImage(data: imageData)
        
        
    }
    
    func setupUI() {
        userImage.circleWithBorderColor(enable: true, color: UIColor.white, width: 3.0)
    }
    
}







//
//  Friends.swift
//  Chat
//
//  Created by Tangerine Creative on 09/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Friends: Codable {
    let message: String
    let friends: [Friend]
    let status: Bool
    let responseCode: Int
}

struct Friend: Codable {
    let uid, image: String
    let deviceID: String?
    let work, name: String
    
    enum CodingKeys: String, CodingKey {
        case uid, image
        case deviceID = "device_id"
        case work, name
    }
}


//
//  MessagesModel.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 10/09/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Firebase

class MessagesModel: NSObject {

    var receiverid: String?
    var receivername: String?
    var receiverphoto: String?
    var senderid: String?
    var sendername: String?
    var senderphoto: String?
    var text: String?
    var timestamp: String?
    
    init(dictionary: [String: Any]) {
        self.receiverid = dictionary["receiverid"] as? String
        self.receivername = dictionary["receivername"] as? String
        self.receiverphoto = dictionary["receiverphoto"] as? String
        self.senderid = dictionary["senderid"] as? String
        self.sendername = dictionary["sendername"] as? String
        self.senderphoto = dictionary["senderphoto"] as? String
        
        self.text = dictionary["text"] as? String
        self.timestamp = dictionary["timestamp"] as? String
    }
    
    func chatPartnerId() -> String? {
        return senderid == myId ? receiverid : senderid
    }
    func chatPartnerImage() -> String? {
        return senderid == myId ? receiverphoto : senderphoto
    }
    func chatPartnerName() -> String? {
        return senderid == myId ? receivername : sendername
    }
}

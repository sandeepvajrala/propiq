//
//  Downloads.swift
//  Chat
//
//  Created by Tangerine Creative on 21/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation
import FirebaseStorage
import MBProgressHUD
import AVFoundation
import Firebase


let storage = Storage.storage()
let kFILEPREFERENCE = "gs://propiq-7ea9b.appspot.com"

//image

func uploadImage(image: UIImage, chatRoomId: String, view: UIView, completion: @escaping (_ imageLink: String?) -> Void) {
    
    let progressHud = MBProgressHUD.showAdded(to: view, animated: true)
    progressHud.mode = .determinateHorizontalBar
//    let dateString = DateFormatter().string(from: Date())
    let photoFileName = myId + "\(Date().millisecondsSince1970)" + ".jpg"
    
    let storageRef = storage.reference(forURL: kFILEPREFERENCE).child(photoFileName)
    let imageData = UIImageJPEGRepresentation(image, 0.7)
    var task: StorageUploadTask!
    
    task = storageRef.putData(imageData!, metadata: nil, completion: { (metadata, error) in
    
        task.removeAllObservers()
        progressHud.hide(animated: true)
        
        if error != nil {
            print("error uploading image \(error!.localizedDescription)")
            return
        }
        
        storageRef.downloadURL(completion: { (url, error) in
            guard let downloadUrl = url else {
                completion (nil)
                return
            }
            completion(downloadUrl.absoluteString)
            
        })
        
        
    })
    
    task.observe(StorageTaskStatus.progress) { (snapshot) in
        progressHud.progress = Float((snapshot.progress?.completedUnitCount)!) / Float((snapshot.progress?.totalUnitCount)!)
    }

}

func downloadImage(imageUrl: String, completion: @escaping (_ image: UIImage?) -> Void) {
    
    let imageURL = NSURL(string: imageUrl)
    let imageFileName = (imageUrl.components(separatedBy: "%").last!).components(separatedBy: "?").first!
    print("filename \(imageFileName)")
    
    if fileExistsAtPath(path: imageFileName) {
        //exist
        if let contentsOfFile = UIImage(contentsOfFile: fileInDocumentDirectory(fileName: imageFileName)) {
            completion(contentsOfFile)
        } else {
            print("coudn't generate Image")
            completion(nil)
        }
    } else {
        let downloadQueue = DispatchQueue(label: "ImageDownloadQueue")
        
        downloadQueue.async {
            let data = NSData(contentsOf: imageURL! as URL)
            if data != nil {
                var docurl = getDocumetns()
                
                docurl = docurl.appendingPathComponent(imageFileName, isDirectory: false)
                
                data!.write(to: docurl, atomically: true)
                
                let imageToReturn = UIImage(data: data! as Data)
                DispatchQueue.main.async {
                    if let imageToReturn = imageToReturn {
                        completion(imageToReturn)

                    }
                }
                
            } else {
                DispatchQueue.main.async {
                    print("no image in database")
                    completion(nil)
                }
            }
        }
    }

}

//Helpers

func fileInDocumentDirectory(fileName: String) -> String {
    
    let fileUrl = getDocumetns().appendingPathComponent(fileName)
    return fileUrl.path

}

func getDocumetns() -> URL {
    
    let documentUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    return documentUrl!
    
}

func fileExistsAtPath(path: String) -> Bool {
    
    var doesExist = false
    
    let filePath = fileInDocumentDirectory(fileName: path)
    let fileManager = FileManager.default
    
    if fileManager.fileExists(atPath: filePath){
        doesExist = true
    } else {
        doesExist = false
    }
    
    return doesExist
}






















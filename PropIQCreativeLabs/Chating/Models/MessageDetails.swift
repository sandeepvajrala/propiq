//
//  MessageDetails.swift
//  Chat
//
//  Created by Tangerine Creative on 09/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct Messages {
    
    let receiverid: String
    let receivername: String
    let receiverphoto: String
    let senderid: String
    let sendername: String
    let senderphoto: String
    let text: String
    let timestamp: Date

}

//
//  PhotoMediaItem.swift
//  Chat
//
//  Created by Tangerine Creative on 22/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class PhotoMediaItem : JSQPhotoMediaItem {
    
    
    var cachedImageView: UIImageView?
    var cachedPlaceholderView: UIView?
    
//    override func mediaViewDisplaySize() -> CGSize {
//        var screenSize = UIScreen.main.bounds.width
//        guard let image = image else {
//            return CGSize(width: screenSize * 0.80, height: screenSize * 0.80 * 0.66)
//        }
//
//        if image.size.width >= image.size.height {
//            screenSize = screenSize * 0.80
//        }
//        else {
//            screenSize = screenSize * 0.65
//        }
//        let resizeFactor = image.size.width / screenSize
//
//        return CGSize(width: image.size.width/resizeFactor, height: image.size.height/resizeFactor)
//    }
    
    override func mediaView() -> UIView? {
        if image == nil {
            return nil
        }
        
        if cachedImageView == nil {
            let size = self.mediaViewDisplaySize()
            let imageView = UIImageView(image: image)
            imageView.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            let masker = JSQMessagesMediaViewBubbleImageMasker(bubbleImageFactory: JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero))
            if appliesMediaViewMaskAsOutgoing {
                masker?.applyOutgoingBubbleImageMask(toMediaView: imageView)
            }
            else {
                masker?.applyIncomingBubbleImageMask(toMediaView: imageView)
            }
            
            cachedImageView = imageView
        }
        
        return cachedImageView
    }
    
    override func mediaPlaceholderView() -> UIView! {
        if cachedPlaceholderView == nil {
            let size = mediaViewDisplaySize()
            let view = JSQMessagesMediaPlaceholderView.withActivityIndicator()
            view?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
            let masker = JSQMessagesMediaViewBubbleImageMasker(bubbleImageFactory: JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero))
            if appliesMediaViewMaskAsOutgoing {
                masker?.applyOutgoingBubbleImageMask(toMediaView: view)
            }
            else {
                masker?.applyIncomingBubbleImageMask(toMediaView: view)
            }
            cachedPlaceholderView = view
        }
        
        return cachedPlaceholderView
    }
    
    
    
    override func mediaViewDisplaySize() -> CGSize {
        let defaultSize:CGFloat = 256
        var thumbSize: CGSize = CGSize(width: defaultSize, height: defaultSize)

        if self.image != nil && self.image.size.height > 0 && self.image.size.width > 0 {
            let aspect: CGFloat = self.image.size.width / self.image.size.height
            if (self.image.size.width > self.image.size.height) {
                thumbSize = CGSize(width: defaultSize, height: defaultSize / aspect)
            } else {
                thumbSize = CGSize(width: defaultSize, height: defaultSize)
            }
        }

        return thumbSize
    }
//
//    override func mediaView() -> UIView?
//    {
//        if let imageView = super.mediaView() as? UIImageView {
//            let masker = JSQMessagesMediaViewBubbleImageMasker(bubbleImageFactory: JSQMessagesBubbleImageFactory(bubble: UIImage.jsq_bubbleCompactTailless(), capInsets: UIEdgeInsets.zero))
//            masker?.applyIncomingBubbleImageMask(toMediaView: imageView)
//
//            return imageView
//        }
//
//        return nil
//    }
}

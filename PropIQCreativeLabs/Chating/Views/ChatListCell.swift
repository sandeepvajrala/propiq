//
//  ChatListCell.swift
//  Chat
//
//  Created by Tangerine Creative on 10/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ChatListCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var latestMessage: UILabel!
    @IBOutlet weak var dateOrTimeLabel: UILabel!
    @IBOutlet weak var viewInInsideCell: UIView!
    
    var message: MessagesModel? {
        didSet {
            
            setupNameAndProfileImage()
            
            if let messageText = message?.text {
                latestMessage?.text = messageText.isValidURL ? "image" : messageText
            }
            
            if let timeStamp = message?.timestamp {
                guard let time = Int(timeStamp) else { return }
                guard let timeStampDate = Date(milliseconds: time) else { return }
                let date = checkForDayToShowTimestamp(date: timeStampDate)
                dateOrTimeLabel.text = date
            }
            
            if let profileImageUrl = message?.chatPartnerImage() {
                self.userImage.sd_setImage(with: URL(string: profileImageUrl), placeholderImage: #imageLiteral(resourceName: "avatarPlaceholder"))
            }
        }
    }

    fileprivate func setupNameAndProfileImage() {
        
        if let senderId = message?.senderid {
            
            let senderName = (senderId == myId) ? message?.receivername : message?.sendername
            if let senderName = senderName {
                self.userName.text = senderName
            }
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup() {
        userImage.circleWithBorderColor(color: UIColor.white, width: 2.0)
        viewInInsideCell.round(enable: true, withRadius: 8.0)
        viewInInsideCell.shadow(enable: true, colored: UIColor.gray.cgColor, withRadius: 4.0)
    }

    func configureCell(message: Messages) {
        
        userName.text = (message.senderid == myId) ? message.receivername : message.sendername
        let text = message.text
        latestMessage.text = text.isValidURL ? "image" : text
        let formater = DateFormatter()
        formater.dateFormat = "h:mm a"
        dateOrTimeLabel.text = formater.string(from: message.timestamp)
    }
    
    func checkForDayToShowTimestamp(date: Date) -> String? {

        let day = Calendar.current.component(.day, from: date)
        let today = Calendar.current.component(.day, from: Date())
        if day == today {
            let formater = DateFormatter()
            formater.dateFormat = "h:mm a"
            return formater.string(from: date)
        } else {
            let formater = DateFormatter()
            formater.dateFormat = "MMM d"
            return formater.string(from: date)
        }
    }


}

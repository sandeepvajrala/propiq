//
//  ContactsTableCell.swift
//  Chat
//
//  Created by Tangerine Creative on 09/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ContactsTableCell: UITableViewCell {

    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var departmentLabel: UILabel!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var viewInsideCell: UIView!
    
    var profileImage : UIImage? {
        didSet {
            if let image = self.profileImage {
                self.imageButton.setImage(image, for: .normal)
            } else {
                self.imageButton.setImage(#imageLiteral(resourceName: "avatarPlaceholder"), for: .normal)
            }
        }
    }
    
    override func draw(_ rect: CGRect) {

        imageButton.circleWithBorderColor(enable: true, color: UIColor.white, width: 2.0)
        viewInsideCell.round(enable: true, withRadius: 8.0)
        viewInsideCell.shadow(enable: true, colored: UIColor.gray.cgColor, withRadius: 4.0)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(friend: Friend) {
        userNameLabel.text = friend.name
        departmentLabel.text = friend.work
        loadImageUsingCacheWithUrlString(friend.image)
        
    }
    
    func loadImageUsingCacheWithUrlString(_ urlString: String) {
        
        self.profileImage = nil
        
        //check cache for image first
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.profileImage = cachedImage
            return
        }
        
        //otherwise fire off a new download
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
            
            //download hit an error so lets return out
            if error != nil {
                print(error ?? "")
                return
            }
            
            DispatchQueue.main.async(execute: {
                
                if let downloadedImage = UIImage(data: data!) {
                    imageCache.setObject(downloadedImage, forKey: urlString as AnyObject)
                    
                    self.profileImage = downloadedImage
                }
            })
            
        }).resume()
    }

}




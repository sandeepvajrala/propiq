//
//  ShowImagePopupVC.swift
//  Chat
//
//  Created by Tangerine Creative on 22/08/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ShowImagePopupVC: UIViewController {
    
    var image: UIImage? = nil
    var titreText: String!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titre: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if image != nil {
            imageView.image = image
        } else {
            print("image not found")
        }    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}


//
//  SettingMailViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 22/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

import MessageUI

class SettingMailViewController: UIViewController {
    
    @IBOutlet weak var tblMainSettingTable: UITableView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDepartment: UILabel!
    
    var arrVaLueListSetting = ["Profile","Change Password", "Logout"]
    
    let AppVersion =  String(format: "%@", Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! CVarArg)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupProfileDetails()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

    //MARK: Methods
    
    func setupUI() {
        profileImage.circleWithBorderColor(enable: true, color: UIColor.white, width: 3.0)
    }
    
    func setupProfileDetails() {
        let firstName = UserDefaults.standard.value(forKey: "user_firstName") as? String
        let lastname = UserDefaults.standard.value(forKey: "user_lastName") as? String
        if let firstName = firstName, let lastName = lastname {
            userName.text = firstName + " " + lastName
        }
        userDepartment.text = UserDetails.sharedInstance.user_work
        guard let imageData = UserDefaults.standard.value(forKey: "user_image") as? Data else { return }
        profileImage.image = UIImage(data: imageData)
    }
    
}

//MARK: - Tableview Delegate and Datasource methods

extension SettingMailViewController: UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVaLueListSetting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "SetingMainTableViewCell", for: indexPath) as! SetingMainTableViewCell
            
            cell.lblMenuType.text = arrVaLueListSetting[indexPath.row]
            
            return cell
        
  
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
       
        if indexPath.row == 0 {
            
            let profileVC = UIStoryboard(name: "SettingStoryboard", bundle: nil).instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            DispatchQueue.main.async {
                self.present(profileVC, animated: true, completion: nil)
            }
        
        } else if indexPath.row == 1 {
            
            performSegue(withIdentifier: "ChangePassWord", sender: self)

        }  else if indexPath.row == 2 {
            
            myId = ""
            myName = ""
            myDepartmentId = ""
            guard let domain = Bundle.main.bundleIdentifier else { return }
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
//            UserDefaults.standard.set(false, forKey: "user_status")
            UserDetails.sharedInstance.clearUserData()
            DataService.instance.REF_BASE.removeAllObservers()
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginPageViewController") as! LoginPageViewController
            DispatchQueue.main.async {
                self.present(loginVC, animated: true, completion: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return 45
    }
    
}

// MARK:- Mail composer Delegate

extension SettingMailViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            print("Sent")
            break
            
        case .saved:
            print("Saved")
            break
            
        case .cancelled:
            print("Cancelled")
            break
            
        case .failed:
            print("Failed")
            break
        }
        LogFile.removeLogFile()
        controller.dismiss(animated: true, completion: nil)
    }
}



/*

// FeedBackMail

func FeedBackMail() {
    if MFMailComposeViewController.canSendMail() {
        let mcVC: MFMailComposeViewController = MFMailComposeViewController()
        mcVC.mailComposeDelegate = self
        let strMail = "Shashank.atray@tangerinedesignlabs.com"
        mcVC.setToRecipients([strMail])
        
        mcVC.setSubject("Feedback Younique")
        
        let messageBody = "\n\nVersion \(Bundle.main.infoDictionary!["CFBundleShortVersionString"]!)" + "\n\(UIDevice.current.systemName) \(UIDevice.current.systemVersion)"
        
        mcVC.setMessageBody(messageBody, isHTML: false)
        let filePath = NSHomeDirectory() + "/Documents/" + "log.txt"
        let url = NSURL(fileURLWithPath: filePath)
        if let fileData = NSData(contentsOfFile: filePath) {
            LogFile.addToLog("Attaching log.txt at location \(url) with feedback mail", forFunction: "\(#function)", withUserID: UserDetails.sharedInstance.User_ID)
            mcVC.addAttachmentData(fileData as Data, mimeType: "txt", fileName: "Log")
        }
        
        self.present(mcVC, animated: true, completion: nil)
    }
    else{
        
    }
}
*/





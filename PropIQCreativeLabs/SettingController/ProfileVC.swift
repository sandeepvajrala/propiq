//
//  ProfileVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 27/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Alamofire

class ProfileVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var profilePictureBtn: UIButton!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contactNumberLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    
    var imagePicker = UIImagePickerController()
    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        addKeyBoardNotificationMethods()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setupUI()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    func setupUI() {
        profilePictureBtn.circleWithBorderColor(color: UIColor.white, width: 5)

        emailLabel.text = "   " + UserDetails.sharedInstance.user_email
        contactNumberLabel.text = "   " + UserDetails.sharedInstance.mobile_num
        firstNameTextField.placeholder = UserDetails.sharedInstance.user_firstName
        lastNameTextField.placeholder = UserDetails.sharedInstance.user_lastName
        firstNameTextField.font = UIFont(name: "Helvetica Neue", size: 18.0)
        lastNameTextField.font = UIFont(name: "Helvetica Neue", size: 18.0)
        firstNameTextField.setLeftPaddingPoints(15)
        lastNameTextField.setLeftPaddingPoints(15)
        guard let imageData = UserDefaults.standard.value(forKey: "user_image") as? Data else { return }
        profilePictureBtn.setImage(UIImage(data: imageData), for: .normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//MARK: - Methods
    // MARK :- keyboard notification value
    
    
    func addKeyBoardNotificationMethods(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        self.view.frame.origin.y = -80
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
        
    }

    fileprivate func pickImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    fileprivate func pickFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showAlert(title: "Alert", message: "Something wrong")
        }
    }

    func uploadDetails() {
        
        self.view.showLoader()
        let userId = UserDetails.sharedInstance.User_ID
        var firstName = String()
        var lastName = String()
        if firstNameTextField.text != "" && firstNameTextField.text != nil {
            firstName = firstNameTextField.text!
        } else {
            firstName = UserDefaults.standard.value(forKey: "user_firstName") as! String
        }
        if lastNameTextField.text != "" && lastNameTextField.text != nil {
            lastName = lastNameTextField.text!
        } else {
            lastName = UserDefaults.standard.value(forKey: "user_lastName") as! String
        }
        let params = ["user_id": userId,"first_name": firstName,"last_name": lastName]
            
        var imageData = Data()
        if let image = image {
            imageData = UIImageJPEGRepresentation(image, 0.2)!
        }
    
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(imageData, withName: "profile picture", fileName: "\(Date().timeIntervalSince1970).jpg", mimeType: "image/jpeg")
            
            for (key, value) in params {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
            
        }, to: ApiFile.updateProfile) { (encodingResult) in
            
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    self.view.hideLoader()
                    UserDefaults.standard.set(firstName, forKey: "user_firstName")
                    UserDefaults.standard.set(lastName, forKey: "user_lastName")
                    if let image = self.image {
                        UserDefaults.standard.set(UIImageJPEGRepresentation(image, 100), forKey: "user_image")
                    }
                    self.dismiss(animated: true, completion: nil)
                    self.showAlert(title: "Success", message: "profile picture Uploaded successfully")
                    
                    
                }
            case .failure(let error):
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                self.view.hideLoader()
            }
        }
        
    }
    

//MARK: - Actions
    
    @IBAction func submitBtnAction(_ sender: Any) {
        uploadDetails()
    }
    
    @IBAction func backBtnTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func profilePicBnTapped(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePhotoOrVideo = UIAlertAction(title: "Camera", style: .default) { (action) in
            print("Camera")
            self.pickFromCamera()
        }
        
        let sharePhoto = UIAlertAction(title: "Photo Library", style: .default) { (action) in
            print("Photo Library")
            self.pickImage()
        }
       
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { (action) in
        }
        
        takePhotoOrVideo.setValue(#imageLiteral(resourceName: "camera"), forKey: "image")
        sharePhoto.setValue(#imageLiteral(resourceName: "picture"), forKey: "image")
        
        optionMenu.addAction(takePhotoOrVideo)
        optionMenu.addAction(sharePhoto)
        optionMenu.addAction(cancelAction)
        
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
}
extension ProfileVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstNameTextField {
            lastNameTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}
extension ProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true) {
            
        }
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            image = pickedImage
            profilePictureBtn.setImage(pickedImage, for: .normal)
            
        }
        
    }
}




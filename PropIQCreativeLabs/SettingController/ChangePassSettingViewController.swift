//
//  ChangePassSettingViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 25/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ChangePassSettingViewController: UIViewController {
    
    @IBOutlet weak var txtCurrentPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnSignUPButton: UIButton!
    
    @IBOutlet weak var bottomLayoutConstraints : NSLayoutConstraint!
    @IBOutlet weak var scrScrollView : UIScrollView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        addKeyBoardNotificationMethods()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtCurrentPassword.text = ""
        txtPassword.text = ""
        txtConfirmPassword.text = ""
        setUpInitialView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpInitialView()
    {
        
        txtPassword.textFieldPadding()
        txtConfirmPassword.textFieldPadding()
        txtPassword.LatentBorderColor()
        txtConfirmPassword.LatentBorderColor()
        txtCurrentPassword.textFieldPadding()
        txtCurrentPassword.LatentBorderColor()
        self.buttonActive(txtConfirmPassword, txtCurrentPassword, string: txtPassword.text!)
       
    }
    
    func addKeyBoardNotificationMethods()
    {
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePassSettingViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ChangePassSettingViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        //  let info = notification.userInfo!
        
        // let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
            
            if (self.txtConfirmPassword.isFirstResponder)
            {
                self.scrScrollView.setContentOffset(CGPoint(x:0, y: 130), animated: true)
            }
            // self.bottomLayoutConstraints.constant = -100
            //-keyboardFrame.size.height
            self.scrScrollView.isScrollEnabled = true
            
            self.scrScrollView.contentSize = CGSize(width:self.scrScrollView.frame.width, height:500)
            
        })
    }
    
    
    @objc func keyboardWillHide(_ notification: NSNotification)
    {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            if (self.txtConfirmPassword.isFirstResponder)
            {
                //self.bottomLayoutConstraints.constant = 0
                self.scrScrollView.setContentOffset(CGPoint(x:0, y: 0), animated: true)
                self.scrScrollView.isScrollEnabled = false
            }
        })
    }
    
    // func to check if both fields are filled
    func buttonActive(_ textField : UITextField,_ textField2 : UITextField, string: String){
        
        if (!(textField.text?.trimmedString().isEmpty)! && !(textField2.text?.trimmedString().isEmpty)! && !(string.isEmpty))
        {
            btnSignUPButton.setButtonEnableForPrimaryButton(true)
        }
        else
        {
            btnSignUPButton.setButtonEnableForPrimaryButton(false)
        }
    }
    
    func checkValidEmail()
    {
        
        self.view.endEditing(true)
        if ((txtPassword.text?.isValidPassword())! && (txtCurrentPassword.text?.isValidPassword())!)
        {
            if (txtPassword.text != txtConfirmPassword.text)
            {
                let alert = UIAlertController(title: "Confirm Password", message: "Password and confirm password donot match.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
            } else {
                ApiCallForChangePassword()
            }
        }
        else
        {
            var strText = "\u{2022} Password must contain one capital letter."
            strText = strText + "\n\u{2022} Must be atleast 8 digit long."
            strText = strText + "\n\u{2022} Can not contain Special Characters."
            let alert = UIAlertController(title: "Confirm Password", message: strText, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
    }
    
    // MARK: - Api call confirm password
    
    func ApiCallForChangePassword()
    {
        self.view.endEditing(true)
        var requestParams = [String : AnyObject]()
        requestParams["user_id"] = UserDetails.sharedInstance.User_ID as AnyObject
        requestParams["user_pass"] = txtCurrentPassword.text as AnyObject
        requestParams["user_newPass"] = txtPassword.text as AnyObject
        
        print("\(requestParams)")
        txtCurrentPassword.LatentBorderColor()
        txtConfirmPassword.LatentBorderColor()
        txtPassword.LatentBorderColor()
        self.scrScrollView.setContentOffset(CGPoint(x:0, y: 0), animated: true)
        // let loder = CustomLoader(title: "", image:#imageLiteral(resourceName: "ic_launcher") )
        
        
        
        _ = ConnectivityHelper.executePOSTRequest(URLString: ApiFile.ChangePassword, parameters: requestParams, headers: nil, success: { (response) in
            
            
            let json =  self.convertStringToDictionary(text: response as! String)
              //print("response: \(json)")
            
            if json == nil
            {
                self.view.endEditing(true)
                
            }
            if let responseDict = json
            {
                if let responseCode = responseDict["responseCode"] as? Int
                {
                    
                    
                    if responseCode == 100
                    {
                        
                        self.txtPassword.text = ""
                        self.txtConfirmPassword.text = ""
                        self.txtCurrentPassword.text = ""
                        
                    }
                    else if responseCode == 120
                    {
                        
                    }
                    else if responseCode == 125
                    {
                        
                    }
                    else if responseCode == 404
                    {
                        
                    }
                }
            }
            
            
        },  failure: { (error) in
            
            
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    // MARK: - IBaction
    @IBAction func SignUpButtonClicked()
    {
        // LogFile.addToLog("ConfirmPasswordViewController.swift Password button clicked", forFunction: "\(#function)", withUserID: UserDetails.sharedInstance.user_id)
        checkValidEmail()
    }
    
    @IBAction func backButtonClicked()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension ChangePassSettingViewController : UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nsString = textField.text as NSString?
        var newString = nsString?.replacingCharacters(in: range, with: string)
        newString = newString?.trimmedString()
        
        if textField == txtPassword
        {
            
            textField.ActiveBorderColor()
            txtCurrentPassword.LatentBorderColor()
            txtConfirmPassword.LatentBorderColor()
            self.buttonActive(txtConfirmPassword,txtCurrentPassword, string: newString!)
        }
        else if textField == txtConfirmPassword
        {
            textField.ActiveBorderColor()
            txtCurrentPassword.LatentBorderColor()
            txtPassword.LatentBorderColor()
            self.buttonActive(txtPassword,txtCurrentPassword, string: newString!)
        }
        else if textField == txtCurrentPassword
        {
            textField.ActiveBorderColor()
            txtPassword.LatentBorderColor()
            txtConfirmPassword.LatentBorderColor()
            self.buttonActive(txtPassword,txtConfirmPassword, string: newString!)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtCurrentPassword
        {
            textField.LatentBorderColor()
            txtPassword.ActiveBorderColor()
            txtPassword.becomeFirstResponder()
        }
        else if textField == txtPassword
        {
            textField.LatentBorderColor()
            txtConfirmPassword.ActiveBorderColor()
            txtConfirmPassword.becomeFirstResponder()
        }
        else if textField == txtConfirmPassword
        {
            textField.LatentBorderColor()
            txtPassword.LatentBorderColor()
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        btnSignUPButton.setButtonEnableForPrimaryButton(false)
        return true
    }
    
}





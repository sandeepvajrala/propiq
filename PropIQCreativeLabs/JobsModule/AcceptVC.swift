//
//  AcceptVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 13/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Alamofire

class AcceptVC: UIViewController {

    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var centerPopupConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    var imagesArray =  [UIImage]()
    var jobId: Int?
    var jobDetails : [Detail]?
    var jobIds = [Int]()
    var selectedIndexPaths = [Int]()
    var selectedIds = [Int]()
    var status: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tabBarController?.tabBar.isHidden = true
        popupView.round(enable: true, withRadius: 10)
        addImageBtn.border(enable: true, withWidth: 1.5, andColor: UIColor.white)
        setupForStaus()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        centerPopupConstraint.constant = 500
        backgroundButton.alpha = 0
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupForStaus () {
        if let status = status {
            if status == 1 {
                //Before status
                tableView.isHidden = true
                self.statusLabel.text = "Take Before picture"
                
            } else {
                //After Status
                self.statusLabel.text = "Take After Picture"
                tableView.isHidden = false
                if let details = jobDetails {
                    self.selectedIndexPaths = [0].repeated(count: details.count)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func addImagesBtnTapped(_ sender: Any) {
        showPopup()
    }
    @IBAction func backgroundBtnPressed(_ sender: Any) {
        closePopup()
    }
    
    @IBAction func uploadImagesBtnTapped(_ sender: Any) {
        uploadImages()
    }
    @IBAction func takePictureBtnTapped(_ sender: Any) {
        pickFromCamera()
    }
    @IBAction func chooseFromLibraryTapped(_ sender: Any) {
        pickImage()
    }
    @IBAction func cancelTapped(_ sender: Any) {
        closePopup()
    }
    @IBAction func backBtnPressed(_ sender: Any) {
//        tabBarController?.tabBar.isHidden = false
//        self.remove()
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func pickImage() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    fileprivate func pickFromCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker.sourceType = .camera
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            self.showAlert(title: "Alert", message: "Something wrong")
        }
    }
    
    fileprivate func showPopup() {
        
        centerPopupConstraint.constant = 0
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            self.backgroundButton.alpha = 0.5
        }
    }
    
    fileprivate func closePopup() {
        
        centerPopupConstraint.constant = 500
        UIView.animate(withDuration: 1.0) {
            self.view.layoutIfNeeded()
            self.backgroundButton.alpha = 0
        }
    }
    
    func uploadImages() {
        _ = addActivityIndicator()
        if imagesArray.count > 0 {
            var arrayOfDictionaries = [[String : Any]]()
            if let status = status {
                if status == 2 {
                    for i in 0..<selectedIndexPaths.count {
                        guard let details = jobDetails else { return }
                        if selectedIndexPaths[i] == 1 {
                            let dict = ["id": details[i].jobDetailID,"status":"1"] as [String : Any]
                            arrayOfDictionaries.append(dict)
                        } else {
                            let dict = ["id": details[i].jobDetailID,"status":"2"] as [String : Any]
                            arrayOfDictionaries.append(dict)
                        }
                    }
                }
            }
            
            let json = arrayOfDictionaries.toJSONString()
            var params = [String : Any]()
            if let id = jobId {
                if status == 1 {
                    params = ["image_status" : "0","job_status": json, "job_id" : "\(id)"]
                }
                else {
                    params = ["image_status" : "1","job_status": json, "job_id" : "\(id)"]
                }
            }
            var imagesData = [Data]()
            var imageParamName = [String]()
            for image in imagesArray {
                let imgData = UIImageJPEGRepresentation(image, 0.2)!
                imagesData.append(imgData)
            }
            for index in 1...imagesArray.count {
                imageParamName.append("image\(index)")
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                for imageData in imagesData {
                    multipartFormData.append(imageData, withName: "\(imageParamName)[]", fileName: "\(Date().timeIntervalSince1970).jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
            }, to: ApiFile.jobImages) { (encodingResult) in
               
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        removeIndicator()
                        if self.status == 1 {
                            self.moveToAfterImages()
                        } else {
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                    removeIndicator()
                }
            }
        } else {
            removeIndicator()
            showAlert(title: "Error", message: "There are no images selected")
        }
    }
    func moveToAfterImages() {
        self.status = 2
        self.imagesArray = []
        self.collectionView.reloadData()
        self.setupForStaus()
    }
    
}

extension AcceptVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if imagesArray.count > 0 {
            return imagesArray.count > 4 ? 4 : imagesArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        if imagesArray.count > 0 {
            cell.imageView.image = imagesArray[indexPath.row]
        }
        return cell
    }
    
    
}

extension AcceptVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {

            imagesArray.append(pickedImage)
            collectionView.reloadData()
            
        }
        
    }
}

extension AcceptVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let details = jobDetails else { return 0 }
        return details.count > 0 ? details.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsInnerCell") as? DetailsInnerCell else { return UITableViewCell()}
        
        guard let details = jobDetails else { return cell }
        cell.configureCell(details: details[indexPath.row])
        if selectedIndexPaths.count > 0 {
            if selectedIndexPaths[indexPath.row] == 0 {
                cell.selectionImage.image = #imageLiteral(resourceName: "unSelection")
            } else {
                
                cell.selectionImage.image = #imageLiteral(resourceName: "selection")
            }
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndexPaths[indexPath.row] == 0 {
            selectedIndexPaths[indexPath.row] = 1
        } else {
            selectedIndexPaths[indexPath.row] = 0
        }
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 25
    }
    func fetchData() {
        
        for i in 0..<selectedIndexPaths.count {
            if selectedIndexPaths[i] == 1 {
                guard let details = jobDetails else { return }
                self.selectedIds.append(details[i].jobDetailID)
            }
        }
        
    }

}

extension Collection where Iterator.Element == [String:Any] {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
    
}





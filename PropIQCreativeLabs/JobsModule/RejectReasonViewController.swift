//
//  RejectReasonViewController.swift
//  PropIQ
//
//  Created by Shashank K. Atray on 15/03/18.
//  Copyright © 2018 Tangerine Creative Labs. All rights reserved.
//

import UIKit

class RejectReasonViewController: UIViewController {
    
    @IBOutlet weak var tblRejectReasonScreen: UITableView!
    @IBOutlet weak var btnConitnueButton: UIButton!
    @IBOutlet weak var txtReasonTextView: UITextView!
    @IBOutlet weak var NSlayoutTextViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nslayoutButtomContraint: NSLayoutConstraint!
    
     let toolBar = UIToolbar()
    
   
    var arrReasons = [AnyObject]()
    var strSelectedvalue =  ""
    var SelectedOther = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyBoardNotificationMethods()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.btnConitnueButton.setButtonEnableForPrimaryButton(false)
        
        self.txtReasonTextView.layer.borderColor = UIColor(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0).cgColor
        
        apiCallToGetCancelReason()
        
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.backgroundColor = UIColor.white
        toolBar.sizeToFit()
        let flaxyBar = UIBarButtonItem (barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem (title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(RejectReasonViewController.DoneButtonClicked))
        
        toolBar.setItems([flaxyBar, doneButton], animated: false)
        self.txtReasonTextView.inputAccessoryView = toolBar
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Keyboard height methods
    
    func addKeyBoardNotificationMethods()
    {
        
        NotificationCenter.default.addObserver(self, selector: #selector(RejectReasonViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RejectReasonViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        let info = notification.userInfo!
        
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
           
            self.nslayoutButtomContraint.constant = keyboardFrame.height
            
        })
    }
    
    
    @objc func keyboardWillHide(_ notification: NSNotification)
    {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            
           self.nslayoutButtomContraint.constant =  10
        })
    }
    
    
    @objc func DoneButtonClicked()
    {
        self.txtReasonTextView.resignFirstResponder()
    }
    // MARK: - API call
    func apiCallToGetCancelReason()
    {
        self.view.endEditing(true)
        let requestParams = [String : AnyObject]()
    
        //  print(requestParams)
        
        // let loder = CustomLoader(title: "", image:#imageLiteral(resourceName: "ic_launcher") )
       
        
       
        _ = ConnectivityHelper.executePOSTRequest(URLString:"", parameters:requestParams , headers: nil, success: { (response) in
            
            print(response)
        
            let json =  self.convertStringToDictionary(text: response as! String)
            
            if json == nil
            {
                self.view.endEditing(true)
             
            }
            
            print("json \(String(describing: json))")
            if let responseDict = json
            {
                
                if let responseCode = responseDict["responseCode"] as? Int
                {
                    //  LogFile.addToLog("ForgotPasswordViewController.swift api call to \(ApiFile.sendPasswordLink) with respondcode \(responseCode)", forFunction: "\(#function)", withUserID: UserDetails.sharedInstance.user_id)
                    
                    if responseCode == 100
                    {
                        if let DataValue = responseDict["data"] as? Array<AnyObject>
                        {
                            self.arrReasons = DataValue 
                        }
                        
                        self.tblRejectReasonScreen.reloadData()
                    }
                    
                  
                }
            }
        },  failure: { (error) in
           
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }
    
    
    @IBAction func backButtonClicked()
    {
        /*
        let MyAddressVC = UIStoryboard(name: "\(self.isIpad() ? "IpadSettingView" : "SettingView")", bundle: nil).instantiateViewController(withIdentifier: "AboutViewController") as! AboutViewController
       
        
        self.navigationController?.pushViewController(MyAddressVC, animated: true)*/
        
        self.navigationController?.popViewController(animated: true)
      
    }
    
    @IBAction func ContinueButtonClicked()
    {
        
        self.view.endEditing(true)
        var requestParams = [String : AnyObject]()
        requestParams["user_id"] = UserDetails.sharedInstance.User_ID as AnyObject
        
        if  self.SelectedOther
        {
              requestParams["reason_comment"] = self.txtReasonTextView.text as AnyObject
        }else
        {
             requestParams["reason_comment"] = strSelectedvalue as AnyObject
        }
      
        requestParams["toggle_status"] = "0" as AnyObject
        
        
          print(requestParams)
        
        // let loder = CustomLoader(title: "", image:#imageLiteral(resourceName: "ic_launcher") )
        
       
        _ = ConnectivityHelper.executePOSTRequest(URLString: "", parameters:requestParams , headers: nil, success: { (response) in
            
            print(response)
          
            let json =  self.convertStringToDictionary(text: response as! String)
            
            if json == nil
            {
                self.view.endEditing(true)
               
            }
            
            print("json \(String(describing: json))")
            if let responseDict = json
            {
                
                if let responseCode = responseDict["responseCode"] as? Int
                {
                    //  LogFile.addToLog("ForgotPasswordViewController.swift api call to \(ApiFile.sendPasswordLink) with respondcode \(responseCode)", forFunction: "\(#function)", withUserID: UserDetails.sharedInstance.user_id)
                    
                    if responseCode == 100
                    {
                        self.backButtonClicked()  
                    }
                    
                  
                    
                }
            }
        },  failure: { (error) in
         
            print("Failed")
            
            
        }, notRechable: {
            
            
        }, sessionExpired: {
            
            
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RejectReasonViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.strSelectedvalue = (arrReasons[indexPath.row] as? String)!
      
        
        if indexPath.row == self.arrReasons.count - 1
        {
           self.txtReasonTextView.isHidden = false
            self.txtReasonTextView.text = ""
            self.NSlayoutTextViewHeight.constant = 100
            self.SelectedOther = true
              self.btnConitnueButton.setButtonEnableForPrimaryButton(false)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                
                let scrollPoint = CGPoint(x: 0, y: self.tblRejectReasonScreen.contentSize.height - self.tblRejectReasonScreen.frame.size.height)
                self.tblRejectReasonScreen.setContentOffset(scrollPoint, animated: true)
            }
        }else
        {
            self.SelectedOther = false
            self.txtReasonTextView.isHidden = true
            self.NSlayoutTextViewHeight.constant = 0
            self.btnConitnueButton.setButtonEnableForPrimaryButton(true)
        }
        tableView.reloadData()
    }
}

extension RejectReasonViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrReasons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let CancelCell:CancelSubscriptionReasonTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CancelSubscriptionReasonTableViewCell", for: indexPath) as! CancelSubscriptionReasonTableViewCell
        
        CancelCell.lblTxtMessage.text = arrReasons[indexPath.row] as? String
        
        if strSelectedvalue == CancelCell.lblTxtMessage.text
        
            {
                // AddressData.sharedInstance.fillingAddressData(AddressData: arrAddress[indexPath.row] as! [String : AnyObject?])
               
                CancelCell.ivRadioButton.image = #imageLiteral(resourceName: "radio_button_Red")
                
            }
            else
            {
                CancelCell.ivRadioButton.image = #imageLiteral(resourceName: "radio_button_Grey")
            }
        
        
        return CancelCell
    }
    
    
}

extension RejectReasonViewController : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let nsString = textView.text as NSString?
        var newString = nsString?.replacingCharacters(in: range, with: text)
        newString = newString?.trimmedString()
        if textView == txtReasonTextView
        {
            if newString?.count != 0
            {
                btnConitnueButton.setButtonEnableForPrimaryButton(true)
            }else
            {
                btnConitnueButton.setButtonEnableForPrimaryButton(false)
            }
        }
        return true
    }
}

//
//  RejectionCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 18/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class RejectionCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var backgroundRejectionView: UIView!
    @IBOutlet weak var rejctionReasonLabel: UILabel!
    @IBOutlet weak var rejectionSelectionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundRejectionView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 4)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

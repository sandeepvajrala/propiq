//
//  JobTableCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import SDWebImage

class JobTableCell: UITableViewCell {

    @IBOutlet weak var mainOuterView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var expandedView: UIView!
    @IBOutlet weak var detailLblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var dateTimeViewWidthconstraint: NSLayoutConstraint!
    @IBOutlet weak var leadingLineLblConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingLineLblConstraint: NSLayoutConstraint!
    @IBOutlet weak var topLineLblConstraint: NSLayoutConstraint!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var roomNumber: UILabel!
    @IBOutlet weak var expandedRoomNoLbl: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var workerView: UIView!
    @IBOutlet weak var dateAndTimeView: UIView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusImageForExpanded: UIImageView!
    
    @IBOutlet weak var workerNameLabel: UILabel!
    @IBOutlet weak var workerDepartmentLabel: UILabel!
    @IBOutlet weak var workerImage: UIImageView!
    @IBOutlet weak var viewImageBtn: UIButton!
    
    
    var jobDetailsArray: [Detail]? {
        didSet {
            tableView.reloadData()
        }
    }
    var jobDetails: Job?

    var isSelection: Bool?
    
    var fromPM: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        
        tableView.delegate = self
        tableView.dataSource = self

        acceptBtn.round(enable: true, withRadius: 17.0)
        rejectBtn.round(enable: true, withRadius: 17.0)
        
        isSelection = false

        mainOuterView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5)
        
        workerImage.circleWithBorderColor(enable: true, color: #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1), width: 2)
        workerView.round(enable: true, withRadius: 20)
        workerView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5)
        
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        UIView.animate(withDuration: 0.1) {
            self.layoutIfNeeded()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
        selected ? cellSelected() : unSelected()
        isSelection = selected
  
    }
    
    func configureCell(jobsArr: Job) {
        roomNumber.text = jobsArr.roomNo
        dateLabel.text = jobsArr.jobDate
        timeLabel.text = jobsArr.jobTime
        jobDetailsArray = jobsArr.detail
        let workerDetails = jobsArr.workerDetails
        workerNameLabel.text = workerDetails.workerFirstName + " " + workerDetails.workerLastName
        workerDepartmentLabel.text = workerDetails.department
        expandedRoomNoLbl.text = jobsArr.roomNo
        
        let urlString = workerDetails.workerImage
        workerImage.sd_setImage(with: URL(string: urlString), placeholderImage: #imageLiteral(resourceName: "avatarPlaceholder"))
        if UserDetails.sharedInstance.user_workStatus == "2"{
            acceptBtn.setTitle("ReAssign", for: UIControlState.normal)
            rejectBtn.setTitle("Decline", for: .normal)
        } else {
            acceptBtn.setTitle("Accept", for: UIControlState.normal)
            rejectBtn.setTitle("Reject", for: .normal)
        }
        let status = jobsArr.status
        switch status-1 {
        case 0:
            statusImage.image = #imageLiteral(resourceName: "yellowIcon")
            statusImageForExpanded.image = #imageLiteral(resourceName: "yellowIcon")
            viewImageBtn.isHidden = true
            
        case 1:
            statusImage.image = #imageLiteral(resourceName: "greenIcon")
            statusImageForExpanded.image = #imageLiteral(resourceName: "greenIcon")
            viewImageBtn.isHidden = false
        case 2:
            statusImage.image = #imageLiteral(resourceName: "whiteIcon")
            statusImageForExpanded.image = #imageLiteral(resourceName: "whiteIcon")
            viewImageBtn.isHidden = false
        case 3: statusImage.image = #imageLiteral(resourceName: "redIcon")
            statusImageForExpanded.image = #imageLiteral(resourceName: "redIcon")
            viewImageBtn.isHidden = true
        case 4:
            statusImage.image = #imageLiteral(resourceName: "greenIcon")
            statusImageForExpanded.image = #imageLiteral(resourceName: "greenIcon")
            viewImageBtn.isHidden = false
            acceptBtn.setTitle("Approve", for: UIControlState.normal)
            rejectBtn.setTitle("Re-Open", for: .normal)
        default:
            break
        }
    }
    
    func cellSelected() {
        expandedView.isHidden = false
        dateTimeViewWidthconstraint.constant = 0
        detailLblTopConstraint.constant = 125.0
        mainOuterView.border(enable: true, withWidth: 1.5, andColor: #colorLiteral(red: 0.5695265532, green: 0.07436098903, blue: 0.1497644782, alpha: 1))
        acceptBtn.isHidden = fromPM ? true : false
        rejectBtn.isHidden = fromPM ? true : false
        dateAndTimeView.isHidden = true
        leadingLineLblConstraint.constant = 30.0
        trailingLineLblConstraint.constant = 30.0
        topLineLblConstraint.constant = 8.0
        mainOuterView.shadow(enable: false, colored: UIColor.black.cgColor)
    }
    
    func unSelected() {
        expandedView.isHidden = true
        dateTimeViewWidthconstraint.constant = 120.0
        detailLblTopConstraint.constant = 10.0
        mainOuterView.border(enable: false)
        acceptBtn.isHidden = true
        rejectBtn.isHidden = true
        dateAndTimeView.isHidden = false
        leadingLineLblConstraint.constant = 15.0
        trailingLineLblConstraint.constant = 15.0
        topLineLblConstraint.constant = 3.0
        mainOuterView.shadow(enable: true, colored: UIColor.black.cgColor, withRadius: 5)
    }

}

//MARK: - Table view Delagate and Datasource methods

extension JobTableCell: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let jobDetails = jobDetailsArray else { return 0 }
        if let selection = isSelection {
            let count = jobDetails.count
            return selection ? count : (count > 3 ? 3 : count)
        }
        return 0
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobsInnerCell") as! JobsInnerCell
        guard let array = jobDetailsArray else { return cell }
        if let selection = isSelection {
            if selection {
                cell.jobDetailLabel.font = cell.jobDetailLabel.font.withSize(18)
            } else {
                cell.jobDetailLabel.font = cell.jobDetailLabel.font.withSize(16)
            }
        }
        
        cell.jobDetailLabel.text = array[indexPath.row].jobDetail
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let selection = isSelection {
            return selection ? 30 : 20
        }
        return 20
    }

}

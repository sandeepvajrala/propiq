//
//  DetailsInnerCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 17/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class DetailsInnerCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        selectionImage.border(enable: true, withWidth: 1, andColor: UIColor.white)
    }
    func configureCell(details: Detail) {
        detailLabel.text = details.jobDetail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

//        if selected{
//            selectionImage.backgroundColor = UIColor.green
//        } else {
//            selectionImage.backgroundColor = imageView?.backgroundColor
//        }
    }

}

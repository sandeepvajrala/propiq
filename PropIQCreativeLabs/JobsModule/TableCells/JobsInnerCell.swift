//
//  JobsInnerCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class JobsInnerCell: UITableViewCell {

    @IBOutlet weak var circleLabel: UILabel!
    @IBOutlet weak var jobDetailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        circleLabel.layer.cornerRadius = 0.5 * circleLabel.layer.bounds.width
        circleLabel.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

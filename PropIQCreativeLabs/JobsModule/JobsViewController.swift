//
//  JobsViewController.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 25/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
import Alamofire

let appDelegate = UIApplication.shared.delegate as! AppDelegate

class JobsViewController: UIViewController {
    
    @IBOutlet weak var tblJobListing: UITableView!
    @IBOutlet weak var noJobsFoundLAbel: UILabel!
    
    // Variables
    var isIndexPathSelected = 0
    var selectedCellIndexPath: IndexPath?
    var selectedCellHeight: CGFloat = 240.0
    let unselectedCellHeight: CGFloat = 140.0
    var jobsArray:[Job]? {
        didSet {
            tblJobListing.reloadData()
        }
    }
    
//MARK: - Life Cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        apiCallForAllJobs()
        
        if let jobsArr = jobsArray {
            selectedCellHeight = CGFloat(jobsArr.count * 30 + 70 + 120 + 30)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        selectedCellIndexPath = nil
        apiCallForAllJobs()
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// MARK : - Methods
   
    func apiCallForAllJobs() {
    
        _ = addActivityIndicator()
        let params = ["job_status":"0","job_priority":"0","user_id":UserDetails.sharedInstance.User_ID,"department_id":"0"]

        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.JobListing, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
            print(response)
            do {
                let jobDetails = try JSONDecoder().decode(JobsData.self, from: response as! Data)
                self.jobsArray = jobDetails.jobs
                if let job = self.jobsArray {
                    if job.count == 0 {
                        self.noJobsFoundLAbel.isHidden = false
                    } else {
                        self.noJobsFoundLAbel.isHidden = true
                    }
                }
               
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
                print(error.localizedDescription)
            }
            
        }, failure: { (error) in
           removeIndicator()
            self.showAlert(title: "Server Error", message: "Failure")
            print(error ?? "error")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Server Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Server Error", message: "Session Expired")

        })

    }
    
//MARK: - Actions
    
    @IBAction func acceptBtnTapped(_ sender: Any) {
        
        let acceptVC = self.storyboard!.instantiateViewController(withIdentifier: "AcceptVC") as! AcceptVC
        
        if let jobsArr = jobsArray {
            guard let index = selectedCellIndexPath?.row else { return }
            acceptVC.jobId = jobsArr[index].jobID
            acceptVC.jobDetails = jobsArr[index].detail
            acceptVC.status = jobsArr[index].status
        }
        self.present(acceptVC, animated:true, completion: nil)
    }
    
    @IBAction func rejectBtnPressed(_ sender: Any) {
        
        let rejectionVC = self.storyboard!.instantiateViewController(withIdentifier: "RejectionVC") as! RejectionVC
        
        if let jobsArr = jobsArray {
            guard let index = selectedCellIndexPath?.row else { return }
            rejectionVC.jobId = jobsArr[index].jobID
        }
        self.present(rejectionVC, animated:true, completion: nil)
    }
    
}

//MARK: - Tableview Methods

extension JobsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard  let jobsArray = jobsArray else { return 0 }
        return jobsArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = indexPath.row
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTableCell") as? JobTableCell else { return UITableViewCell() }

        if let jobsArr = jobsArray {
            
            let jobDetails = jobsArr[index]
            cell.configureCell(jobsArr: jobDetails)
        }
        if selectedCellIndexPath == nil {
            cell.setSelected(false, animated: true)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let jobsArr = jobsArray {
            let count = jobsArr[indexPath.row].detail.count
            selectedCellHeight = CGFloat(count * 30 + 70 + 120 + 20)
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! JobTableCell
        if selectedCellIndexPath != nil && selectedCellIndexPath == indexPath {
            selectedCellIndexPath = nil
            cell.setSelected(false, animated: true)
//            cell.isSelection = false
            
        } else {
            selectedCellIndexPath = indexPath
            cell.setSelected(true, animated: true)
//            cell.isSelection = true
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if selectedCellIndexPath == indexPath {
            return selectedCellHeight
        }
        return unselectedCellHeight

    }
}








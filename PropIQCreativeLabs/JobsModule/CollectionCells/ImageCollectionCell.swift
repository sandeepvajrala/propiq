//
//  ImageCollectionCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 13/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}

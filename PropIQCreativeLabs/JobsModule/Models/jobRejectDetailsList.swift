//
//  jobRejectDetailsList.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 18/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation

struct JobRejectDetailsList: Codable {
    let jobRejectDetailsList: [JobRejectDetailsListElement]
    let message: String
    let status: Bool
    let responseCode: Int
}

struct JobRejectDetailsListElement: Codable {
    let departmentID: Int
    let detail: String
    let detailID: Int
    
    enum CodingKeys: String, CodingKey {
        case departmentID = "department_id"
        case detail
        case detailID = "detail_id"
    }
}

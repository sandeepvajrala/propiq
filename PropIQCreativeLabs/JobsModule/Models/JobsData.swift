//
//  JobsData.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 11/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import Foundation


struct JobsData: Codable {
    let jobs: [Job]
    let priority2, priority3, priority1: Int?
    let message: String
    let status: Bool
    let responseCode: Int
}

struct Job: Codable {
    let jobTime: String
    let jobPriority: Int
    let jobDate: String
    let jobID: Int
    let workerDetails: WorkerDetails
    let roomNo, time: String
    let detail: [Detail]
    let room: String
    let status: Int
    
    enum CodingKeys: String, CodingKey {
        case jobTime = "job_time"
        case jobPriority = "job_priority"
        case jobDate = "job_date"
        case jobID = "job_id"
        case workerDetails = "worker_details"
        case roomNo = "room_no"
        case time, detail, room, status
    }
}

struct Detail: Codable {
    let jobDetailID: Int
    let jobDetail: String
    let jobDetailStatus: Int
    
    enum CodingKeys: String, CodingKey {
        case jobDetailID = "jobDetail_id"
        case jobDetail
        case jobDetailStatus = "jobDetail_status"
    }
}

struct WorkerDetails: Codable {
    let workerLastName: String
    let departmentID: Int
    let workerFirstName, workerImage, department: String
    let workerID: Int
    
    enum CodingKeys: String, CodingKey {
        case workerLastName = "worker_lastName"
        case departmentID = "department_id"
        case workerFirstName = "worker_firstName"
        case workerImage = "worker_image"
        case department
        case workerID = "worker_id"
    }
}

//
//  RejectionVC.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 18/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit
protocol RejectionDelegate: class {
    func updateJobsVC()
}

class RejectionVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    var selectedIndexPaths = [Int]()
    var jobId: Int?
    var rejectDetailsArray: [JobRejectDetailsListElement]? {
        didSet{
            self.tableView.reloadData()
        }
    }
    weak var delegate: RejectionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.selectedIndexPaths = [0].repeated(count: 3)
        callAPIForRejectDetailsList()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {

        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtnPressed(_ sender: Any) {
        callApiForSubmitRejectionList()
    }
    
    func callAPIForRejectDetailsList() {
        _ = addActivityIndicator()
        _ = ConnectivityHelper.executeGETRequest(URLString: ApiFile.jobRejectDetailsList, parameters: nil, headers: nil, success: { (response) in
             removeIndicator()
            do {
                let jobRejectDetailsList = try JSONDecoder().decode(JobRejectDetailsList.self, from: response as! Data)
                self.rejectDetailsArray = jobRejectDetailsList.jobRejectDetailsList
            } catch let error {
                self.showAlert(title: "Error", message: "\(error.localizedDescription)")
            }
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Error", message: "Failure")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "error", message: "Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Error", message: "session expired")
        })
    
    }
    
    func callApiForSubmitRejectionList() {
        _ = addActivityIndicator()
        var arrayOfRejectedDetails = [String]()
        for i in 0..<selectedIndexPaths.count {
            guard let details = rejectDetailsArray else { return }
            if selectedIndexPaths[i] == 1 {
                arrayOfRejectedDetails.append(details[i].detail)
            }
        }
        guard let id = jobId else { return }
        
        let params = ["job_id": "\(id)", "reject_list": "\(arrayOfRejectedDetails)"]
        
        _ = ConnectivityHelper.callPOSTRequest(URLString: ApiFile.rejectJobs, parameters: params as [String : AnyObject], headers: nil, success: { (response) in
            removeIndicator()
//            self.tabBarController?.tabBar.isHidden = false
//            self.remove()
            self.delegate?.updateJobsVC()
            self.dismiss(animated: true, completion: nil)
        }, failure: { (error) in
            removeIndicator()
            self.showAlert(title: "Error", message: "Failure")
        }, notRechable: {
            removeIndicator()
            self.showAlert(title: "error", message: "Not Reachable")
        }, sessionExpired: {
            removeIndicator()
            self.showAlert(title: "Error", message: "session expired")
        })
    }
    
}

//MARK: - Tableview Methods

extension RejectionVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let rejectionDetails = rejectDetailsArray{
            return rejectionDetails.count > 0 ? rejectionDetails.count : 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RejectionCell") as? RejectionCell else { return UITableViewCell() }
        
        if let rejectionDetails = rejectDetailsArray {
            cell.rejctionReasonLabel.text = rejectionDetails[indexPath.row].detail
        }
    
        if selectedIndexPaths.count > 0 {
            if selectedIndexPaths[indexPath.row] == 0 {
                cell.rejectionSelectionImage.image = #imageLiteral(resourceName: "unSelection")
            } else {
                
                cell.rejectionSelectionImage.image = #imageLiteral(resourceName: "selection")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedIndexPaths[indexPath.row] == 0 {
            selectedIndexPaths[indexPath.row] = 1
        } else {
            selectedIndexPaths[indexPath.row] = 0
        }
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

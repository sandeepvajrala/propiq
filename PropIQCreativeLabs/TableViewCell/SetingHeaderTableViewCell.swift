//
//  SetingHeaderTableViewCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 22/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class SetingHeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblDesignation: UILabel!
    @IBOutlet weak var lblname: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  SetingMainTableViewCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 22/05/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class SetingMainTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblMenuType: UILabel!
    @IBOutlet weak var imgMenuImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

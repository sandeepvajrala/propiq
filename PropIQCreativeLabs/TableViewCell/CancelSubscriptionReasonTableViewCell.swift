//
//  CancelSubscriptionReasonTableViewCell.swift
//  Younique
//
//  Created by AcmeMinds on 15/03/18.
//  Copyright © 2018 AcmeMinds. All rights reserved.
//

import UIKit

class CancelSubscriptionReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTxtMessage: UILabel!
    @IBOutlet weak var ivRadioButton: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

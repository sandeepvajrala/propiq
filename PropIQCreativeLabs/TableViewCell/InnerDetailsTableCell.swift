//
//  InnerDetailsTableCell.swift
//  PropIQCreativeLabs
//
//  Created by Tangerine Creative on 17/07/18.
//  Copyright © 2018 Tangerine Creative. All rights reserved.
//

import UIKit

class InnerDetailsTableCell: UITableViewCell {

    @IBOutlet weak var detailNameLabel: UILabel!
    @IBOutlet weak var selectionImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionImage.border(enable: true, withWidth: 1, andColor: UIColor.black)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            selectionImage.backgroundColor = UIColor.green
        }
    }
    
}
